import { Operations, Orders } from './enum'

export interface IStaticFilter {
  name: string
  operation: Operations
  value: number | string | number[]
}
export interface ISort {
  name: string
  order: Orders
}
