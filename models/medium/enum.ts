export enum MediumType {
  Image = 'image',
}

export enum MediumFor {
  BannerImage = 'banner-image',

  ProductImage = 'product-image',
  ProductImages = 'product-images',

  ContentImage = 'content-image',
  ServiceIcon = 'category-icon',
}
