import { MediumFor, MediumType } from './enum'

export interface IMediumCreate {
  file: File
  for: MediumFor
  type: MediumType
}
