import { MediumType } from './enum'
// List Model
export interface IMedium {
  id: number
  url: string
  type: MediumType
}