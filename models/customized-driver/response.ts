import { IDefaultTrip } from '../default-trip/response'
import { IDriver } from '../driver/response'
import IBaseModel from '../base/model/index'
import { CustomizedDriverType } from './enum'

export interface ICustomizedDriver extends IBaseModel {
  id: number
  date: string
  type?: CustomizedDriverType
  companyId: number
  defaultTrip: IDefaultTrip
  driver: IDriver
}

export interface ICustomizedDriverDetails extends ICustomizedDriver {}
