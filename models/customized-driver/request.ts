import { CustomizedDriverType } from './enum'

export interface ICustomizedDriverCreate {
  date: string
  type: CustomizedDriverType
  default_trip_id: number
  driver_id: number
}

export interface ICustomizedDriverEdit {
  date?: string
  type?: CustomizedDriverType
  default_trip_id?: number
  driver_id?: number
}
