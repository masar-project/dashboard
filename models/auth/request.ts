export interface ILoginRequest {
  username: string
  password: string
}

export interface IRegisterRequest {
  fullname: string
  username: string
  password: string
}

export interface IChangePassword {
  oldPassword: string
  password: string
}
