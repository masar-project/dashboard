import { IUserDetails } from '../user/resopnse'
import { IEmployee } from '../employee/response'

export interface IEmployeeLoginResponse {
  employee: IEmployee
  accessToken: string
  tokenType: string
}

export interface IRegisterResponse {
  user: IUserDetails
  token: string
}
