export interface IEmployeeCreate {
  profile_img_id: number;
  first_name: string
  last_name: string
  gender: string
  mobile_number: string
  birth_date: string
  email: string
  password: string
  password_confirmation: string
  role: string
}

export interface IEmployeeEdit {
  profile_img_id: number
  first_name?: string
  last_name?: string
  gender?: string
  mobile_number?: string
  birth_date?: string
}
