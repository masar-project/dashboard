import { ICompany } from '../company/response'
import { IUser } from '../user/resopnse'

export interface IEmployee {
  id: number
  role: string
  disable: boolean
  createdAt: string
  updatedAt: string
  user: IUser
  companyId: number
  company: ICompany
}

export interface IEmployeeDetails extends IEmployee {}

interface RootObject {}

interface User {
  id: number
  firstName: string
  lastName: string
  gender: string
  birthDate: string
  mobileNumber: string
  email: string
  createdAt: string
  updatedAt: string
  profileImg: ProfileImg
}

interface ProfileImg {
  id: number
  type: string
  url: string
}
