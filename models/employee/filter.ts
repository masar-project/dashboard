import IBaseFilter from '../base/filter/index'
import { UserGender, UserRole } from '../user/enums';

export interface IEmployeeListFilter extends IBaseFilter {
	gender?: UserGender
	role?: UserRole
}
