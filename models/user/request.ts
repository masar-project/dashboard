export interface IUserCreate {
  fullName: string
  username: string
  password: string

  roles: string[]
}

export interface IUserEdit {
  fullName: string
  username: string

  roles: string[]
}
