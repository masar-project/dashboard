export enum UserStatus {
  NOT_VERIFIED = 1,
  VERIFIED,
  BLOCKED,
}

export enum UserGender {
  male = 'male',
  female = 'female',
}

export enum UserRole {
  owner = 'owner',
  manager = 'manager',
  driver = 'driver',
  normal = 'normal',
}

// Status Tags Colors
export const getUserStatusColor = (status: UserStatus) => {
  switch (status) {
    case UserStatus.VERIFIED:
      return 'green'
    case UserStatus.NOT_VERIFIED:
      return 'magenta'
    case UserStatus.BLOCKED:
      return 'red'
    default:
      return 'blue'
  }
}
