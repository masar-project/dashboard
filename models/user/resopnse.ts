import { IMedium } from '../medium/resopnse'
import { IRole } from '../role/response'
import IBaseModel from '../base/model/index'

// List Model
export interface IUser extends IBaseModel {
  id: number
  email: string

  firstName: string
  lastName: string

  gender: string
  birthDate: string

  mobileNumber: string
  profileImg: IMedium

  roles: IRole[]
}

// Details Model
export interface IUserDetails extends IUser {}
