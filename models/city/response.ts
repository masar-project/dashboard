import { ITranslationResponse } from '../base/translation'
import { IRegion } from '../region/response'

export interface ICity {
  id: number
  name: string
  nameTranslations: ITranslationResponse
  regions: IRegion[]
}
