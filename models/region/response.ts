import { ITranslationResponse } from '../base/translation'

export interface IRegion {
  id: number
  name: string
  cityId: number
  nameTranslations: ITranslationResponse
}
