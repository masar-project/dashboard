export const EVENT_CONNECTION = 'connection'
export const EVENT_DISCONNECT = 'disconnect'

export const EVENT_POST_LOCATION = 'post-location'
export const EVENT_GET_LOCATION = 'get-location'

export const EVENT_POST_FAILURE_POS = 'post-failure-pos'
export const EVENT_GET_FAILURE_POS = 'get-failure-pos'

export const EVENT_POST_SUCCESS_POS = 'post-success-pos'
export const EVENT_GET_SUCCESS_POS = 'get-success-pos'

export const EVENT_POST_IN_TRANSIT_POS = 'post-in-transit-pos'
export const EVENT_GET_IN_TRANSIT_POS = 'get-in-transit-pos'

export const EVENT_POST_START_TRIP = 'post-start-trip'
export const EVENT_GET_START_TRIP = 'get-start-trip'

//
export const EVENT_POST_PAUSE_TRIP = 'post-pause-trip'
export const EVENT_GET_PAUSE_TRIP = 'get-pause-trip'

export const EVENT_POST_CANCEL_TRIP = 'post-cancel-trip'
export const EVENT_GET_CANCEL_TRIP = 'get-cancel-trip'

export const EVENT_POST_COMPLETE_TRIP = 'post-complete-trip'
export const EVENT_GET_COMPLETE_TRIP = 'get-complete-trip'

export const EVENT_POST_REFRESH = 'post-refresh'
export const EVENT_GET_REFRESH_DASHBOARD = 'get-refresh-dashboard'
