export interface IProductCreate {
  name: string
  categoryId: number
  imageId: number
}

export interface IProductEdit {
  name?: string
  categoryId?: number
  imageId?: number
}
