import { ICategory } from '../category/response'
import { IMedium } from '../medium/resopnse'

export interface IProduct {
  id: number
  name: string
  category: ICategory
  image: IMedium
  createdAt: string
  updatedAt: string
}

export interface IProductDetails extends IProduct {}
