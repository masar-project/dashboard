export enum TripStatus {
  Idle = 'idle',
  Active = 'active',
  Paused = 'paused',
  Done = 'done',
  Canceled = 'canceled',
}

export enum SortedPOSStatus {
  Idle = 'idle',
  Intransit = 'intransit',
  Success = 'success',
  Failure = 'failure',
  Dropped = 'dropped',
}
