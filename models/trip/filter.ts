import IBaseFilter from '../base/filter/index'

export interface ITripListFilter extends IBaseFilter {
  date?: string
}
