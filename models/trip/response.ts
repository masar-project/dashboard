import { ICity } from '../city/response'
import { IWorkTimes } from '../pos/response'
import { IRegion } from '../region/response'
import { SortedPOSStatus, TripStatus } from './enum'

export interface ITrip {
  id: number
  date: string
  status: TripStatus
  routeDistance?: number // Todo: m => km
  routeDuration?: number // Todo: min => h
  pointsOfSaleSorted: ISortedPOS[]
  defaultTripId: number
  //
  name: string
  pointsOfSaleCount: number
}

export interface ITripDetails extends ITrip {}

export interface ISortedPOS {
  id: number
  name: string
  note: string
  address: string
  lat: number
  long: number
  workTimes: IWorkTimes
  city: ICity
  region: IRegion

  status: SortedPOSStatus
  failureNote?: string

  arrivalTime?: string
  actualOrder?: number
  defaultOrder?: number
  pointOfSaleFailureReason?: string
}
