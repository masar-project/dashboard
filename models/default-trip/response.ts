import { TripDays } from './enum'
import IBaseModel from '../base/model/index'
import { IDriver } from '../driver/response'
import { IPOS } from '../pos/response'

export interface IDefaultTrip extends IBaseModel {
  id: number
  name: string
  day: TripDays
  pointsOfSaleCount: number
  driver: IDriver
  pointsOfSale: IPOS[]
}

export interface IDefaultTripDetails extends IDefaultTrip {
  pointsOfSale: IPOS[]
}

export interface IPreviewTrip {
  name: string
  day: TripDays
  driver: IDriver
  pointsOfSale: IPOS[]
}
