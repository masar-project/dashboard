import { TripDays } from './enum'

export interface IDefaultTripCreate {
  name: string
  day: TripDays

  driver_id: number
  point_of_sale_ids: number[]
}

export interface IDefaultTripEdit {
  name?: string
  day?: TripDays

  driver_id?: number
  point_of_sale_ids?: number[]
}
