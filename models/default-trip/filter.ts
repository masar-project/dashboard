import IBaseFilter from '../base/filter/index'

export interface IDefaultTripListFilter extends IBaseFilter {
  date?: string
}

export interface IPreviewTripListFilter extends IBaseFilter {
  date: string
}
