export interface ITranslationResponse {
  en: string
  ar: string
}

export interface ITranslationRequest {
  [key: string]: string
}
