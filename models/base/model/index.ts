import { IUser } from '../../user/resopnse'

export default interface IBaseModel {
  createdAt: Date
  createdBy: IUser

  updatedAt: Date
  updatedBy: IUser

  deletedAt: Date
  deletedBy: IUser
}
