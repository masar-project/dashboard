export default interface IBaseFilter {
  key_search?: string
  city_id?: number
  region_id?: number
}
