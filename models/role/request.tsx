export interface IRoleCreate {
  name: string
  permissions: string[]
}

export interface IRoleEdit {
  name: string
  permissions: string[]
}
