import { IPermission } from '../permission/response'

export interface IRole {
  id: number
  name: string
  permissions: IPermission[]
  editable: boolean
}

export interface IRoleDetails extends IRole {}
