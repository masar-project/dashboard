import IBaseModel from '../base/model/index'
import { IDefaultTrip } from '../default-trip/response'
import { IPOS } from '../pos/response'

export interface ICustomizedPOS extends IBaseModel {
  id: number
  date: string
  type: string
  companyId: number
  defaultTrip: IDefaultTrip
  pointsOfSale: IPOS
}

export interface ICustomizedPOSDetails extends ICustomizedPOS {}
