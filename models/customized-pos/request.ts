import { CustomizedPOSType } from './enum'

export interface ICustomizedPOSCreate {
  date: string
  type: CustomizedPOSType
  default_trip_id: number
  point_of_sale_id: number
}

export interface ICustomizedPOSEdit {
  date?: string
  type?: CustomizedPOSType
  default_trip_id?: number
  point_of_sale_id?: number
}
