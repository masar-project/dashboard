import IBaseFilter from '../base/filter/index'
import { UserGender, UserRole } from '../user/enums';

export interface IDriverListFilter extends IBaseFilter {
	gender?: UserGender
	role?: UserRole
}
