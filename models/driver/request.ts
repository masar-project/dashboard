import { IUser } from "../user/resopnse";

export interface IDriverCreate {
  profile_img_id: number;
  first_name: string
  last_name: string
  gender: string
  mobile_number: string
  birth_date: string
  email: string
  password: string
  password_confirmation: string
  role: string
}

export interface IDriverEdit {
  profile_img_id: number
  first_name?: string
  last_name?: string
  gender?: string
  mobile_number?: string
  birth_date?: string
}