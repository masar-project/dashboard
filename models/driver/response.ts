import { IUser } from '../user/resopnse'

export interface IDriver {
  id: number
  role: string
  disable: boolean
  createdAt: string
  updatedAt: string
  user: IUser
}

export interface IDriverDetails extends IDriver {}
