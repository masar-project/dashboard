export enum PointStatus {
  Primary,
  Success,
  Failure,
  InTransit,
  Disable,
}
