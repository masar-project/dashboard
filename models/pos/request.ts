export interface IPOSCreate {
  name: string

  address: string

  lat: string
  long: string

  region_id: number
  city_id: number

  work_times: IWorktimesCreate
}

export interface IPOSEdit {
  name?: string

  address?: string

  lat?: string
  long?: string

  region_id?: number
  city_id?: number

  work_times?: IWorktimesCreate
}

interface IWorktimesCreate {
  Friday: IWorktimeCreate
  Monday: IWorktimeCreate
  Saturday: IWorktimeCreate
  Sunday: IWorktimeCreate
  Thursday: IWorktimeCreate
  Tuesday: IWorktimeCreate
  Wednesday: IWorktimeCreate
}

interface IWorktimeCreate {
  close: string
  open: string
}
