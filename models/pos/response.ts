import IBaseModel from '../base/model/index'
import { ICity } from '../city/response'
import { IRegion } from '../region/response'
import { SortedPOSStatus } from '../trip/enum'

export interface IPOS extends IBaseModel {
  id: number
  name: string
  note: string
  address: string
  lat: number
  long: number
  workTimes: IWorkTimes
  city: ICity
  region: IRegion

  defaultOrder?: number
  status?: SortedPOSStatus
}

export interface IPOSDetails extends IPOS {}

export interface IWorkTimes {
  Saturday: IWorkTime
  Sunday: IWorkTime
  Monday: IWorkTime
  Tuesday: IWorkTime
  Wednesday: IWorkTime
  Thursday: IWorkTime
  Friday: IWorkTime
}

export interface IWorkTime {
  open: string
  close: string
  vacation: boolean
}

export const days = {
  Saturday: '',
  Sunday: '',
  Monday: '',
  Tuesday: '',
  Wednesday: '',
  Thursday: '',
  Friday: '',
}
