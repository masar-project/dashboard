import { IMedium } from '../medium/resopnse'
export interface ICompany {
  id: number
  name: string
  address: string
  lat: string
  long: string
  img: IMedium
}
