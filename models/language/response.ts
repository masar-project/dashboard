import IBaseModel from '../base/model/index'
export interface ILanguage extends IBaseModel {
  code: string
  name: string
  isMain: boolean
}

export interface ILanguageDetails extends ILanguage {}
