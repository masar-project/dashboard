import { ITranslationRequest } from '../base/translation/index'

export interface ILanguageCreate {
  code: string
  name: string
}

export interface ILanguageEdit {
  code?: string
  name?: string
}
