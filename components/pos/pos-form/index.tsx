import React, { useContext, useEffect, useState } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Input, Select, Upload, Row, Col, DatePicker, Checkbox } from 'antd'
import { ErrorMessage } from '@hookform/error-message'
import { days, IPOSDetails } from '@/models/pos/response'
import AppContext from '@/contexts/app/context'
import FormItem from '@/components/general/form-item'
import LocationPicker, { ILatLng } from '@/components/general/location-picker'
import moment from 'moment'
import styles from './style.module.scss'

const { TextArea } = Input
const { RangePicker } = DatePicker

export interface IPOSFrom {
  name?: string
  address?: string
  location: ILatLng

  region_id?: number
  city_id?: number

  workTimes?: {
    Saturday: {
      date: [moment.Moment, moment.Moment]
      vacation: boolean
    }
    Sunday: {
      date: [moment.Moment, moment.Moment]
      vacation: boolean
    }
    Monday: {
      date: [moment.Moment, moment.Moment]
      vacation: boolean
    }
    Tuesday: {
      date: [moment.Moment, moment.Moment]
      vacation: boolean
    }
    Wednesday: {
      date: [moment.Moment, moment.Moment]
      vacation: boolean
    }
    Thursday: {
      date: [moment.Moment, moment.Moment]
      vacation: boolean
    }
    Friday: {
      date: [moment.Moment, moment.Moment]
      vacation: boolean
    }
  }
}

interface IPOSFormProps {
  pos?: IPOSDetails
}

const POSForm: React.FC<IPOSFormProps> = ({ pos }) => {
  const { control, errors, getValues } = useFormContext()
  const { t } = useTranslation()
  const { loading, languages, cities } = useContext(AppContext)

  // City Id
  const [cityId, setCityId] = useState<number>()

  // Regions
  const [regions, setRegions] = useState([])

  useEffect(() => {
    cityId &&
      setRegions(
        cities?.data?.filter((city) => city?.id === cityId)[0]?.regions
      )
  }, [cityId])

  return (
    <>
      <Row gutter={12}>
        4{/* Name */}
        <Col span={24}>
          <FormItem label={t('name')}>
            <Controller
              as={Input}
              name='name'
              control={control}
              rules={{ required: `${t('field_is_required_message')}` }}
            />

            <ErrorMessage
              errors={errors}
              name='name'
              render={({ message }) => (
                <p className={styles.alert}>{message}</p>
              )}
            />
          </FormItem>
        </Col>
        {/* City */}
        <Col span={12}>
          <FormItem label={t('city')}>
            <Controller
              rules={{ required: `${t('field_is_required_message')}` }}
              render={({ onChange, onBlur, value }) => {
                return (
                  <Select
                    style={{ width: '100%' }}
                    placeholder={t('please_select')}
                    onChange={(value) => {
                      console.log('value: ', value)
                      setCityId(value)
                      onChange(value)
                    }}
                    onBlur={onBlur}
                    value={value}
                    optionLabelProp='label'
                    options={
                      cities?.data?.map((city) => {
                        return {
                          value: city.id,
                          label: city.nameTranslations.ar,
                        }
                      }) ?? []
                    }
                  />
                )
              }}
              name='city_id'
              control={control}
            />

            <ErrorMessage
              errors={errors}
              name='city_id'
              render={({ message }) => (
                <p className={styles.alert}>{message}</p>
              )}
            />
          </FormItem>
        </Col>
        {/* Region */}
        <Col span={12}>
          <FormItem label={t('region')}>
            <Controller
              rules={{ required: `${t('field_is_required_message')}` }}
              render={({ onChange, onBlur, value }) => {
                return (
                  <Select
                    style={{ width: '100%' }}
                    placeholder={t('please_select')}
                    onChange={onChange}
                    onBlur={onBlur}
                    value={value}
                    optionLabelProp='label'
                    options={
                      regions?.map((region) => {
                        return {
                          value: region.id,
                          label: region.nameTranslations.ar,
                        }
                      }) ?? []
                    }
                  />
                )
              }}
              name='region_id'
              control={control}
            />

            <ErrorMessage
              errors={errors}
              name='region_id'
              render={({ message }) => (
                <p className={styles.alert}>{message}</p>
              )}
            />
          </FormItem>
        </Col>
        {/* Address */}
        <Col span={24}>
          <FormItem label={t('address')}>
            <Controller
              as={Input}
              name='address'
              control={control}
              rules={{ required: `${t('field_is_required_message')}` }}
            />

            <ErrorMessage
              errors={errors}
              name='address'
              render={({ message }) => (
                <p className={styles.alert}>{message}</p>
              )}
            />
          </FormItem>
        </Col>
        {/* Work Times */}
        <Col span={24}>
          <FormItem label={t('work_times')}>
            {Object?.keys(days ?? {})?.map((key) => {
              return (
                <Row>
                  <Controller
                    name={`workTimes.${key}.date`}
                    control={control}
                    render={({ onChange, onBlur, value, name }) => {
                      return (
                        <Row key={key} className={styles.timeContainer}>
                          <Col span={4}>
                            <span>{t(key)}:</span>
                          </Col>
                          <Col span={16}>
                            <RangePicker
                              name={name}
                              onChange={(value) => {
                                onChange(value)
                              }}
                              onBlur={onBlur}
                              value={
                                value ?? [
                                  moment('09:00', 'hh:mm'),
                                  moment('18:00', 'hh:mm'),
                                ]
                              }
                              picker='time'
                              bordered
                              format='hh:mm'
                              key={key}
                            />
                          </Col>

                          <Col span={4}>
                            <div>
                              <Controller
                                name={`workTimes.${key}.vacation`}
                                control={control}
                                render={({ onChange, onBlur, value, name }) => {
                                  console.log('check: ', value)
                                  return (
                                    <Checkbox
                                      name={name}
                                      key={key}
                                      onChange={(e) => {
                                        onChange(e?.target?.checked)
                                      }}
                                      checked={value ?? false}
                                    >
                                      {t('vacation')}
                                    </Checkbox>
                                  )
                                }}
                              />
                            </div>
                          </Col>
                        </Row>
                      )
                    }}
                  />

                  <ErrorMessage
                    errors={errors}
                    name={`workTimes.${key}`}
                    render={({ message }) => (
                      <p className={styles.alert}>{message}</p>
                    )}
                  />
                </Row>
              )
            })}
          </FormItem>
        </Col>
        <Col span={24}>
          <FormItem label={t('location')}>
            <Controller
              name='location'
              control={control}
              rules={{ required: `${t('field_is_required_message')}` }}
              render={({ onChange, value }) => {
                return <LocationPicker value={value} onChange={onChange} />
              }}
            />

            <ErrorMessage
              errors={errors}
              name='location'
              render={({ message }) => (
                <p className={styles.alert}>{message}</p>
              )}
            />
          </FormItem>
        </Col>
      </Row>
    </>
  )
}

export default POSForm
