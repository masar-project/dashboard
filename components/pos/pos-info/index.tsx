import React from 'react'
import { Descriptions } from 'antd'
import { useTranslation } from 'react-i18next'
import { IPOSDetails, IWorkTime } from '../../../models/pos/response'
import LocationMap from '@/components/general/location-map'
import { MAP_DEFAULT_LOCATION } from '@/utils/constants'

interface IPOSInfoProps {
  pos?: IPOSDetails
}

const POSInfo: React.FC<IPOSInfoProps> = ({ pos }) => {
  const {
    t,
    i18n: { language },
  } = useTranslation()

  return (
    <>
      <Descriptions
        bordered
        column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
        size='default'
      >
        <Descriptions.Item label={t('id')}>{`#${pos?.id}`}</Descriptions.Item>
        <Descriptions.Item label={t('name')}>{pos?.name}</Descriptions.Item>
        <Descriptions.Item label={t('lat')}>{pos?.lat}</Descriptions.Item>
        <Descriptions.Item label={t('long')}>{pos?.long}</Descriptions.Item>
        <Descriptions.Item label={t('note')}>{pos?.note}</Descriptions.Item>

        <Descriptions.Item label={t('region')}>
          {pos?.region?.name}
        </Descriptions.Item>
        <Descriptions.Item label={t('city')}>
          {pos?.city?.name}
        </Descriptions.Item>
        <Descriptions.Item label={t('address')}>
          {pos?.address}
        </Descriptions.Item>
        <Descriptions.Item label={t('location')}>
          <LocationMap
            value={{
              lat: Number(pos?.lat ?? MAP_DEFAULT_LOCATION.lat),
              lng: Number(pos?.long ?? MAP_DEFAULT_LOCATION.lng),
            }}
            height='400px'
          />
        </Descriptions.Item>
        <Descriptions.Item label={t('work_times')}>
          {pos?.workTimes &&
            Object.keys(pos?.workTimes).map((key) => (
              <WorkTime key={key} day={key} workTime={pos?.workTimes[key]} />
            ))}
        </Descriptions.Item>
      </Descriptions>
    </>
  )
}

export default POSInfo

const WorkTime: React.FC<{ day: string; workTime: IWorkTime }> = ({
  day,
  workTime,
}) => {
  const { t } = useTranslation()
  return (
    <div>
      <div>{`${t(`${day}`)}: ${workTime?.open} -> ${workTime?.close}`}</div>
    </div>
  )
}
