import React, { useContext, useEffect, useState } from 'react'
import { Avatar, Badge, Divider, List, Typography } from 'antd'
import { useTranslation } from 'react-i18next'
import IMapFilter from '@/models/base/filter-items'
import { ITrip } from '@/models/trip/response'
import TripContext from '@/contexts/trip/context'
import styles from './style.module.scss'
import cn from 'classnames'
import { ISortedPOS } from '../../../models/trip/response'
import { getPng } from '@/components/mapbox/map-pin'
import { getTodayPOSsStatus } from '../../../pages/dashboard/manage/index'
import { sortBy } from 'lodash'
import SocketContext from '../../../contexts/socket/context'
import { TripStatus } from '@/models/trip/enum'
import TodayPOSsList from '../today-trip-points'
import CompleteTripModal from '../complete-trip-modal'

const { Text } = Typography

interface IPorps {
  filters: IMapFilter

  selectedTrip?: ITrip
  onTripSelected?: (trip: ITrip) => void

  todaySelectedPOS: ISortedPOS
  setTodaySelectedPOS: (sortedPos: ISortedPOS) => void
}

const TodayTripsList: React.FC<IPorps> = ({
  filters,
  selectedTrip,
  onTripSelected,
  todaySelectedPOS,
  setTodaySelectedPOS,
}) => {
  const { t } = useTranslation()
  const { activeTripId } = useContext(SocketContext)
  // Trips
  const { data: trips, loading: tripsLoading } = useContext(TripContext)
  const [todayTrips, setTodayTrips] = useState(trips?.data)

  useEffect(() => {
    trips && setTodayTrips(trips?.data)
  }, [trips])

  useEffect(() => {
    console.log('activeTripId: ', activeTripId)

    if (activeTripId) {
      console.log('activeTripId: ', activeTripId)
      let changedTrip = todayTrips?.find((t) => t?.id == activeTripId)
      changedTrip.status = TripStatus.Active
      // Change status of trip
      setTodayTrips([
        ...todayTrips?.filter((t) => t.id != activeTripId),
        changedTrip,
      ])
    }
  }, [activeTripId])

  return (
    <>
      <List
        header={
          <Text strong>
            {t('today_trips')} ( {t(`${filters?.date?.format('dddd')}`)} )
          </Text>
        }
        className='tripsList'
        dataSource={todayTrips ?? []}
        loading={tripsLoading}
        renderItem={(trip) => (
          <List.Item
            key={trip?.id}
            className={cn(styles.trip, {
              [styles.active]: selectedTrip?.id == trip?.id,
            })}
            onClick={() => {
              onTripSelected(trip)
            }}
          >
            <List.Item.Meta
              style={{ padding: '0 1rem' }}
              avatar={<Badge color={getBadgeColor(trip?.status)} />}
              description={
                <>
                  <Text>
                    {trip?.name} ({trip?.pointsOfSaleCount})
                  </Text>
                </>
              }
            />
          </List.Item>
        )}
        split={false}
      />

      <Divider />

      <TodayPOSsList
        selectedTrip={selectedTrip}
        onTripSelected={onTripSelected}
        filters={filters}
        todaySelectedPOS={todaySelectedPOS}
        setTodaySelectedPOS={setTodaySelectedPOS}
      />

      <CompleteTripModal />
    </>
  )
}

export default TodayTripsList

const getBadgeColor: (tripStats: TripStatus) => string = (tripStats) => {
  let color = '#ccc'
  switch (tripStats) {
    case TripStatus.Idle:
      color = 'purple'
      break
    case TripStatus.Active:
      color = 'green'
      break
    case TripStatus.Paused:
      color = 'red'
      break
    case TripStatus.Done:
      color = 'blue'
      break
    case TripStatus.Canceled:
      color = 'red'
      break
    default:
      color = '#ccc'
      break
  }
  return color
}
