import { Avatar, Badge, Button, Col, Modal, Row } from 'antd'
import React, { useContext, useEffect, useState } from 'react'
import SocketContext from '../../../contexts/socket/context'
import { useTranslation } from 'react-i18next'
import { SortedPOSStatus } from '@/models/trip/enum'
import { getPng } from '@/components/mapbox/map-pin'
import { PointStatus } from '@/models/pos/enum'
import DefaultTripContext from '@/contexts/default-trip/context'

interface IProps {}

const CompleteTripModal: React.FC<IProps> = () => {
  const { completeTrip } = useContext(SocketContext)
  const { previewTrip } = useContext(DefaultTripContext)
  const { t } = useTranslation()

  useEffect(() => {}, [])
  useEffect(() => {
    console.log('completeTrip: ', completeTrip)
    completeTrip && setVisible(true)
  }, [completeTrip])

  const [visible, setVisible] = useState(false)

  return (
    <Modal
      title={`${previewTrip?.name} ${t('completed')}`}
      visible={visible}
      closable
      destroyOnClose
      onCancel={() => setVisible(false)}
      afterClose={() => setVisible(false)}
      width='50vw'
      footer={
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          <Button
            onClick={() => setVisible(false)}
            style={{ marginInlineEnd: 8 }}
          >
            {t('ok')}
          </Button>
        </div>
      }
    >
      <div
        style={{ textAlign: 'center', margin: 'auto', fontSize: '2rem' }}
      >{`${t('trip')} "${previewTrip?.name}" ${t('completed')}`}</div>

      <div
        style={{
          textAlign: 'center',
          margin: 'auto',
          fontSize: '1rem',
          fontWeight: 'bold',
        }}
      >
        {completeTrip?.date}
      </div>

      <Row style={{ width: '100%', marginTop: '1rem', fontSize: '1rem' }}>
        <Col span={12}>
          <div style={{ textAlign: 'start', margin: 'auto' }}>
            <span>
              <Badge status='success' />
            </span>{' '}
            {t('success_points')}
          </div>
          <div>
            {previewTrip?.pointsOfSale
              ?.filter((p) => p?.status == SortedPOSStatus?.Success)
              ?.map((point, index) => {
                return (
                  <div key={index} style={{ margin: '0.5rem 1rem' }}>
                    <span style={{ marginInlineEnd: '0.5rem' }}>
                      <Avatar
                        src={getPng(PointStatus.Success)}
                        style={{ height: '20px', width: '20px' }}
                      />
                    </span>
                    {point?.name}
                  </div>
                )
              })}
          </div>
        </Col>

        <Col span={12}>
          <div style={{ textAlign: 'start', margin: 'auto', fontSize: '1rem' }}>
            <span>
              <Badge status='error' />
            </span>{' '}
            {t('failure_points')}
          </div>
          <div>
            {previewTrip?.pointsOfSale
              ?.filter((p) => p?.status == SortedPOSStatus?.Failure)
              ?.map((point, index) => {
                return (
                  <div key={index} style={{ margin: '0.5rem 1rem' }}>
                    <span style={{ marginInlineEnd: '0.5rem' }}>
                      <Avatar
                        src={getPng(PointStatus.Failure)}
                        style={{ height: '20px', width: '20px' }}
                      />
                    </span>
                    {point?.name}
                  </div>
                )
              })}
          </div>
        </Col>
      </Row>
    </Modal>
  )
}

export default CompleteTripModal
