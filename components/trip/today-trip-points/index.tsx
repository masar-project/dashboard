import React, { useContext, useEffect, useState } from 'react'
import { Avatar, Badge, Button, List, Switch, Typography } from 'antd'
import styles from './style.module.scss'
import { sortBy } from 'lodash'
import { getPng } from '@/components/mapbox/map-pin'
import { getTodayPOSsStatus } from '@/pages/dashboard/manage'
import cn from 'classnames'
import TripContext from '@/contexts/trip/context'
import { useTranslation } from 'react-i18next'
import { ISortedPOS, ITrip } from '@/models/trip/response'
import DefaultTripContext from '@/contexts/default-trip/context'
import { PlusOutlined } from '@ant-design/icons'
import CustomizedPOSModal from '@/components/customized-pos/customized-pos-modal'
import { FILTER_DATE_FORMATE } from '@/utils/constants'
import IMapFilter from '@/models/base/filter-items'
import CustomizedPOSContext from '@/contexts/customized-pos/context'
import { CustomizedPOSType } from '@/models/customized-pos/enum'
import { SortedPOSStatus } from '@/models/trip/enum'
import SocketContext from '@/contexts/socket/context'

const { Text } = Typography

interface IProps {
  filters: IMapFilter

  selectedTrip?: ITrip
  onTripSelected?: (trip: ITrip) => void

  todaySelectedPOS: ISortedPOS
  setTodaySelectedPOS: (sortedPos: ISortedPOS) => void
}

const TodayPOSsList: React.FC<IProps> = ({
  filters,
  selectedTrip,
  onTripSelected,
  todaySelectedPOS,
  setTodaySelectedPOS,
}) => {
  // Preview
  const {
    previewTrip,
    previewTripLoading,
    actions: defaultTripActions,
  } = useContext(DefaultTripContext)

  // Socket
  const { actions: socketActions, refreshTrip } = useContext(SocketContext)

  useEffect(() => {
    if (selectedTrip) {
      defaultTripActions.previewTrip(selectedTrip?.defaultTripId, {
        date: filters?.date?.format(FILTER_DATE_FORMATE),
      })
    }
  }, [refreshTrip])

  const { t } = useTranslation()
  const { data: trips, loading: tripsLoading } = useContext(TripContext)

  useEffect(() => {
    console.log('selectedTrip: ', selectedTrip)
    if (selectedTrip) {
      defaultTripActions.previewTrip(selectedTrip?.defaultTripId, {
        date: filters?.date?.format(FILTER_DATE_FORMATE),
      })
    }
  }, [selectedTrip])
  // Customized (Add POS)
  const [createVisible, setCreateVisible] = useState(false)

  const {
    actions: customizedPOSActions,
    actionLoading: customizedPOSActionLoading,
  } = useContext(CustomizedPOSContext)

  const isActive: (defaultTrip: ITrip, pointOfSaleId: number) => boolean = (
    defaultTrip,
    pointOfSaleId
  ) => {
    return (
      previewTrip?.pointsOfSale?.map((p) => p.id)?.includes(pointOfSaleId) ||
      defaultTrip?.pointsOfSaleSorted
        ?.map((p) => p?.id)
        ?.includes(pointOfSaleId)
    )
  }

  return (
    <>
      {previewTrip && (
        <>
          <List
            header={
              <div className={styles.listHeader}>
                <Text strong>{t('poss')}</Text>
                <Button
                  className={styles.addBtn}
                  type='text'
                  onClick={() => {
                    setCreateVisible(true)
                  }}
                >
                  <PlusOutlined />
                  {t('add')}
                </Button>
              </div>
            }
            className='possList'
            dataSource={previewTrip?.pointsOfSale ?? []}
            loading={previewTripLoading}
            renderItem={(pointOfSale) => (
              <List.Item
                key={pointOfSale?.id}
                className={cn(styles.pos, {
                  [styles.active]: todaySelectedPOS?.id == pointOfSale?.id,
                })}
                onClick={() => {
                  setTodaySelectedPOS({
                    ...pointOfSale,
                    status: pointOfSale?.status ?? SortedPOSStatus.Idle,
                    defaultOrder:
                      pointOfSale?.defaultOrder ??
                      selectedTrip?.pointsOfSaleSorted?.find(
                        (p) => p?.id == pointOfSale?.id
                      )?.defaultOrder ??
                      null,
                  })
                }}
                actions={[
                  <Switch
                    defaultChecked={isActive(selectedTrip, pointOfSale?.id)}
                    loading={customizedPOSActionLoading}
                    size='small'
                    onChange={async () => {
                      let res: Boolean
                      res = isActive(selectedTrip, pointOfSale?.id)
                        ? await customizedPOSActions?.createCustomizedPOS({
                            date: filters?.date?.format(FILTER_DATE_FORMATE),
                            default_trip_id: selectedTrip?.defaultTripId,
                            point_of_sale_id: pointOfSale?.id,
                            type: CustomizedPOSType.Deletion,
                          })
                        : await customizedPOSActions?.deleteCustomizedPOS(
                            pointOfSale?.id
                          )

                      if (res) {
                        defaultTripActions?.previewTrip(
                          selectedTrip?.defaultTripId,
                          {
                            date: filters?.date?.format(FILTER_DATE_FORMATE),
                          }
                        )
                        socketActions.refreshTrip()
                      }
                    }}
                  />,
                ]}
              >
                <List.Item.Meta
                  style={{ padding: '0 1rem' }}
                  avatar={
                    <Avatar
                      src={getPng(
                        getTodayPOSsStatus(
                          {
                            ...pointOfSale,
                            defaultOrder: pointOfSale?.defaultOrder,
                            status: pointOfSale?.status ?? SortedPOSStatus.Idle,
                          }
                          // selectedTrip?.pointsOfSaleSorted?.find(
                          //   (p) => p.id === pointOfSale?.id
                          // ) ?? {
                          //   ...pointOfSale,
                          //   defaultOrder: pointOfSale?.defaultOrder,
                          //   status: pointOfSale?.status ?? SortedPOSStatus.Idle,
                          // }
                        )
                      )}
                      style={{ height: '20px', width: '20px' }}
                    />
                  }
                  description={
                    <>
                      <Text>{pointOfSale?.name}</Text>
                    </>
                  }
                />
              </List.Item>
            )}
            split={false}
          />
          <TodayDriver />

          <CustomizedPOSModal
            visible={createVisible}
            setVisible={setCreateVisible}
            date={filters?.date?.format(FILTER_DATE_FORMATE)}
            defaultTripId={selectedTrip?.defaultTripId}
          />
        </>
      )}
    </>
  )
}

export default TodayPOSsList

const TodayDriver: React.FC = () => {
  const { t } = useTranslation()
  const { previewTrip, previewTripLoading } = useContext(DefaultTripContext)

  return (
    <div className='driversList'>
      <List split={false} loading={previewTripLoading}>
        <>
          <List.Item key='default-driver' className={styles.driver}>
            <List.Item.Meta
              style={{ padding: '0 1rem' }}
              title={`${t('default_driver')}:`}
              description={`${previewTrip?.driver?.user?.firstName} ${previewTrip?.driver?.user?.lastName}`}
            />
          </List.Item>
        </>
      </List>
    </div>
  )
}
