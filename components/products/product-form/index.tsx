import React, { useContext, useEffect, useState } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Input, Select, Row, Col, Spin, Upload } from 'antd'
import { ErrorMessage } from '@hookform/error-message'
import FormItem from '@/components/general/form-item'
import styles from './style.module.scss'
import { IProductDetails } from '@/models/product/response'
import AppContext from '@/contexts/app/context'
import { isImageTypeSupport } from '@/utils/helpers/support-images'
import { errorNotification } from '@/utils/helpers/error-notification'
import { compressImage } from '@/utils/helpers/image-compression'
import { ACCESS_TOKEN } from '@/utils/constants'
import { MediumFor, MediumType } from '@/models/medium/enum'
import { InboxOutlined } from '@ant-design/icons'
import Img from '@/components/general/img'
import cn from 'classnames'

const { Dragger } = Upload

interface IProductFormProps {
  product?: IProductDetails
}

const ProductForm: React.FC<IProductFormProps> = ({ product }) => {
  const { control, errors } = useFormContext()
  const { t } = useTranslation()
  const { categories } = useContext(AppContext)

  // Image
  const [fileList, setFileList] = useState([])
  const [image, setImage] = useState(product?.image?.url)
  const [uploading, setUploading] = useState(false)

  useEffect(() => {
    product?.image?.url && setImage(product?.image?.url)
  }, [product])

  return (
    <Row gutter={12}>
      <Col span={12}>
        <Row gutter={12}>
          {/* name */}
          <Col span={24}>
            <FormItem label={t('name')}>
              <Controller
                as={Input}
                name='name'
                control={control}
                rules={{ required: `${t('field_is_required_message')}` }}
              />

              <ErrorMessage
                errors={errors}
                name='name'
                render={({ message }) => (
                  <p className={styles.alert}>{message}</p>
                )}
              />
            </FormItem>
          </Col>

          <Col span={24}>
            <FormItem label={t('category')}>
              <Controller
                rules={{ required: `${t('field_is_required_message')}` }}
                render={({ onChange, onBlur, value }) => {
                  return (
                    <Select
                      style={{ width: '100%' }}
                      placeholder={t('please_select')}
                      onChange={onChange}
                      onBlur={onBlur}
                      value={value}
                      optionLabelProp='label'
                      options={categories?.data?.map((category) => {
                        return {
                          value: category?.id,
                          label: category?.name,
                        }
                      })}
                    />
                  )
                }}
                name='categoryId'
                control={control}
              />

              <ErrorMessage
                errors={errors}
                name='categoryId'
                render={({ message }) => (
                  <p className={styles.alert}>{message}</p>
                )}
              />
            </FormItem>
          </Col>
        </Row>
      </Col>
      <Col span={12}>
        {/* Image */}
        {/* <div className={styles.imageTitle}>{t('image')}</div> */}
        <FormItem label={t('category')}>
          <Controller
            name='imageId'
            control={control}
            rules={{
              required: {
                value: true,
                message: `${t('field_is_required_message')}`,
              },
            }}
            render={({ onChange }) => (
              <Spin spinning={uploading}>
                <Dragger
                  accept='image/*'
                  name='medium'
                  listType='text'
                  multiple={false}
                  onRemove={() => {
                    setFileList([])
                    onChange(null)
                  }}
                  beforeUpload={(file) => {
                    setUploading(true)
                    onChange(null)

                    // Check File Type.
                    const isImageSupport = isImageTypeSupport(file.type)

                    if (!isImageSupport) {
                      errorNotification(t('you_can_only_upload_images'))
                      setUploading(false)
                      return false
                    }

                    // Compress Image
                    setFileList([file])
                    setImage(null)
                    return compressImage(file)
                  }}
                  onChange={({ file }) => {
                    if (file.status === 'done') {
                      onChange(file?.response?.id)
                      setUploading(false)
                    }
                  }}
                  fileList={[...fileList]}
                  action={process.env.NEXT_PUBLIC_UPLOAD_FILE_URL}
                  headers={{
                    Authorization: `Bearer ${localStorage.getItem(
                      ACCESS_TOKEN
                    )}`,
                  }}
                  data={() => {
                    return {
                      type: MediumType.Image,
                      for: MediumFor.ProductImage,
                    }
                  }}
                >
                  <p className='ant-upload-drag-icon'>
                    <InboxOutlined />
                  </p>
                  <p className='ant-upload-text'>{t('select_or_drag_file')}</p>
                </Dragger>
              </Spin>
            )}
          />

          <ErrorMessage
            errors={errors}
            name='imageId'
            render={({ message }) => <p className={styles.alert}>{message}</p>}
          />
        </FormItem>

        {/* Old  Image */}
        {image && (
          <div className={styles.oldImageContainer}>
            <span className={cn(styles.p0, styles.mx1)}>
              <Img
                className={styles.imageIcon}
                src={image}
                placeholder={true}
              />
            </span>
          </div>
        )}
      </Col>
    </Row>
  )
}

export default ProductForm
