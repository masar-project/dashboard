import React from 'react'
import { Col, Descriptions, Row } from 'antd'
import { useTranslation } from 'react-i18next'
import { IProductDetails } from '@/models/product/response'
import Img from '@/components/general/img'

import styles from './style.module.scss'

interface IProductInfoProps {
  product: IProductDetails
}

const ProductInfo: React.FC<IProductInfoProps> = ({ product }) => {
  const { t } = useTranslation()

  return (
    <Row>
      <Col span={16}>
        <Descriptions
          bordered
          column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          size='small'
        >
          {/* id */}
          <Descriptions.Item label={t('id')} span={3}>
            {`#${product?.id}`}
          </Descriptions.Item>

          {/* name */}
          <Descriptions.Item label={t('name')}>
            {product?.name}
          </Descriptions.Item>

          {/* category */}
          <Descriptions.Item label={t('category')}>
            {product?.category?.name}
          </Descriptions.Item>

          {/* created_at */}
          <Descriptions.Item label={t('created_at')}>
            {product?.createdAt}
          </Descriptions.Item>
        </Descriptions>
      </Col>

      <Col span={8}>
        {/* image */}
        <Descriptions.Item label={t('image')} span={3}>
          <Img className={styles.imageIcon} src={product?.image?.url} />
        </Descriptions.Item>
      </Col>
    </Row>
  )
}

export default ProductInfo
