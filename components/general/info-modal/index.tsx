import React from 'react'

import { Button, Modal, Spin } from 'antd'
import { useTranslation } from 'react-i18next'

interface IInfoProps {
  id: string
  title: string
  visible: boolean
  toggleVisible: (visible: boolean) => void
  confirmButtonText: string
  loading: boolean

  children?: React.ReactNode
  style?: React.CSSProperties
  width?: string | number
}
const InfoModal: React.FC<IInfoProps> = ({
  id,
  title,
  visible,
  toggleVisible,
  confirmButtonText,
  loading,
  children,
  style,
  width,
}) => {
  const { t } = useTranslation()
  return (
    <Modal
      key={id}
      title={title}
      visible={visible}
      closable
      destroyOnClose
      centered
      width={width}
      onCancel={() => toggleVisible(false)}
      afterClose={() => toggleVisible(false)}
      footer={
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <Button type='primary' onClick={async () => toggleVisible(false)}>
            {t(confirmButtonText ?? 'ok')}
          </Button>
        </div>
      }
    >
      <Spin spinning={loading}>{children}</Spin>
    </Modal>
  )
}

export default InfoModal
