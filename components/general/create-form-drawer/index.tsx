import { Button, Drawer } from 'antd'

import React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

interface IFormDrawer<T> {
  id?: string
  title: string
  visible: boolean
  toggleVisible: (visible: boolean) => void
  children?: React.ReactNode
  onSubmit?: (data: any) => Promise<Boolean>
  submitLoading: boolean
}

function CreateFormDrawer<T>(props: IFormDrawer<T>) {
  const { id, title, toggleVisible, visible, children, submitLoading } = props

  const {
    t,
    i18n: { language },
  } = useTranslation()

  const methods = useForm()
  const { handleSubmit } = methods

  const onSubmit = async (data) => {
    return (await props.onSubmit(data)) && toggleVisible(false)
  }

  return (
    <FormProvider {...methods}>
      <form id={id ?? 'create-form-drawer'} onSubmit={handleSubmit(onSubmit)}>
        <Drawer
          title={title}
          visible={visible}
          maskClosable={false}
          closable
          destroyOnClose
          onClose={() => toggleVisible(false)}
          width='50vw'
          placement={language === 'ar' ? 'left' : 'right'}
          footer={
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Button
                onClick={() => toggleVisible(false)}
                style={{ marginInlineEnd: 8 }}
              >
                {t('cancel')}
              </Button>
              <Button
                htmlType='submit'
                form={id ?? 'create-form-drawer'}
                type='primary'
                loading={submitLoading}
              >
                {t('submit')}
              </Button>
            </div>
          }
        >
          {children}
        </Drawer>
      </form>
    </FormProvider>
  )
}

export default CreateFormDrawer
