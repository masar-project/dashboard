import { Spin } from 'antd'
import React from 'react'

import styles from './style.module.scss'
import { LoadingOutlined } from '@ant-design/icons'

const Spinner: React.FC = () => {
  return (
    <div className={styles.loading}>
      <Spin spinning indicator={<LoadingOutlined />} />
    </div>
  )
}

export default Spinner
