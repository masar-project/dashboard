import React from 'react'
import { Pagination, Card } from 'antd'

import styles from './style.module.scss'
import { useTranslation } from 'react-i18next'
import { PaginationProps } from 'antd/lib/pagination'
import { DEFAULT_PAGE_SIZE } from '../../../contexts/constants'

const PaginationCard: React.FC<PaginationProps> = (props) => {
  const { t } = useTranslation()
  return (
    <Card className={styles.paginationCard}>
      <Pagination
        {...props}
        showTotal={(total, range) =>
          `${range[0]}-${range[1]} ${t('of')} ${total} ${t('items')}`
        }
        defaultPageSize={DEFAULT_PAGE_SIZE}
      />
    </Card>
  )
}

export default PaginationCard
