import React from 'react'
import { PageHeader as AntdPageHeader } from 'antd'
import { TagType } from 'antd/lib/tag'

import styles from './style.module.scss'
interface ISettingsHeaderProps {
  title: string
  subTitle?: string
  extra?: React.ReactNode
  // tags?: React.ReactElement<TagType> | React.ReactElement<TagType>[]
  onBack?: () => void
}

const SettingsHeader: React.FC<ISettingsHeaderProps> = (props) => {
  return <AntdPageHeader ghost={false} {...props} />
}

export default SettingsHeader
