import { Layout } from 'antd'
import React from 'react'
import styles from './style.module.scss'

const { Content } = Layout

const SettingsLayout: React.FC = ({ children }) => {
	return <Content className={styles.settingLayout}>{children}</Content>
}

export default SettingsLayout
