import React, { useContext } from 'react'
import { Layout, Menu } from 'antd'
import cn from 'classnames'
import AppContext from '@/contexts/app/context'

const { Sider } = Layout
const { Item } = Menu
import styles from './style.module.scss'
import { useRouter } from 'next/router'
import { settingsRoutes } from '../../../../routes'
import { useTranslation } from 'react-i18next'

interface IProps {}

const SettingsSideBar: React.FC<IProps> = () => {
  const { direction, screenSize } = useContext(AppContext)
  const { pathname, push } = useRouter()
  const { t } = useTranslation()

  return (
    <Sider
      theme='dark'
      width='200'
      className={cn(styles.sider, {
        [styles.siderLeft]: direction === 'ltr',
        [styles.siderRight]: direction === 'rtl',
      })}
      collapsed={screenSize === 'mobileOrTablet'}
    >
      <Menu
        theme='dark'
        mode='inline'
        defaultSelectedKeys={[pathname]}
        style={{ height: '100%' }}
      >
        {settingsRoutes.map((route, index) => {
          return (
            <Item
              key={route?.path}
              icon={route?.icon}
              style={{ marginTop: '0' }}
              onClick={() => push(route?.path)}
            >
              {t(route?.labelKey)}
            </Item>
          )
        })}
      </Menu>
    </Sider>
  )
}

export default SettingsSideBar
