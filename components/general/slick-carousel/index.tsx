import React from 'react'

import { Carousel } from 'antd'
import classes from './style.module.scss'

const ImagesCarousel: React.FC = ({ children }) => {
  return (
    <div className={classes.carouselContainer}>
      <Carousel
        swipeToSlide={true}
        speed={2000}
        autoplaySpeed={3000}
        slidesToShow={React.Children.count(children) >= 2 ? 2 : 1}
        slidesToScroll={1}
        cssEase='linear'
        autoplay
        infinite={true}
      >
        {children}
      </Carousel>
    </div>
  )
}

export default ImagesCarousel
