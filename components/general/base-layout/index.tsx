import React, { useContext } from 'react'
import { Layout } from 'antd'
import AppContext from '../../../contexts/app/context'
import NavBar from '../nav-bar'
import LayoutContent from '../layout-content'
import AuthContext from '../../../contexts/auth/context'
import LoadingPage from '../loading-page'
import styles from './style.module.scss'

interface IBaseLayoutProps {
  pageHeader?: JSX.Element
  footer?: React.ReactNode
  sideBar?: React.ReactNode
}

const BaseLayout: React.FC<IBaseLayoutProps> = ({
  children,
  pageHeader,
  footer,
  sideBar,
}) => {
  const { direction } = useContext(AppContext)
  const { isAuthenticated } = useContext(AuthContext)
  return isAuthenticated ? (
    <Layout dir={direction} className='h-100vh'>
      <NavBar />
      <Layout dir={direction}>
        {sideBar}
        <Layout>
          <LayoutContent pageHeader={pageHeader}>{children}</LayoutContent>
        </Layout>
      </Layout>
      {footer}
    </Layout>
  ) : (
    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh'}}>
      <LoadingPage />
    </div>
  )
}

export default BaseLayout
