import React from 'react'
import { Layout, Menu, Row, Col, Popover, DatePicker, Card } from 'antd'
import { useTranslation } from 'react-i18next'
import { SlidersOutlined } from '@ant-design/icons'

import styles from './style.module.scss'

const { Footer: AntdFooter } = Layout

const menu = (
	<Card><Row>
  <Col span={12}>
    <DatePicker/>
  </Col>
  <Col span={12}></Col>
</Row></Card>
)

const Footer: React.FC = () => {
	const { t } = useTranslation()

	return (
		<AntdFooter className={styles.footer}>
			<Row>
				<Col span={24} lg={8}></Col>
				<Col span={24} lg={8}>
					<span>{`جميع الحقوق محفوظة © 2021 `}</span>
					<img src='/images/masar-logo.png' height='40' />
				</Col>
				<Col span={24} lg={8} className='d-flex-end'>
					<Popover content={menu} trigger={['click']}>
						<Menu theme='dark' mode='horizontal'>
							<Menu.Item onClick={() => console.log('ss')}>
								<SlidersOutlined />
								{t('filter')}
							</Menu.Item>
						</Menu>
					</Popover>
				</Col>
			</Row>
		</AntdFooter>
	)
}

export default Footer
