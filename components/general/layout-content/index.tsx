import React from 'react'
import { Layout } from 'antd'
import Footer from '../footer'

import styles from './style.module.scss'

const { Content } = Layout

interface ILayoutContentProps {
  pageHeader?: JSX.Element
}

const LayoutContent: React.FC<ILayoutContentProps> = ({
  children,
  pageHeader,
}) => {
  return (
    <Content className={styles.overflowAuto}>
      {pageHeader}
      <div className={styles.layoutInnerContent}>{children}</div>
    </Content>
  )
}

export default LayoutContent
