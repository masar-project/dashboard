import { Button, Drawer, Spin } from 'antd'

import React, { useEffect } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

interface IFormDrawer<T> {
  title: string
  visible: boolean
  toggleVisible: (visible: boolean) => void
  defaultValues?: T
  children?: React.ReactNode
  onSubmit?: (data: any) => Promise<Boolean>
  loading?: boolean
  submitLoading: boolean
}

function EditFormDrawer<T>(props: IFormDrawer<T>) {
  const {
    defaultValues,
    title,
    toggleVisible,
    visible,
    children,
    loading,
    submitLoading,
  } = props

  const {
    t,
    i18n: { language },
  } = useTranslation()

  const methods = useForm<any>({
    defaultValues: { ...defaultValues },
  })

  const { handleSubmit, reset } = methods

  useEffect(() => {
    if (defaultValues) {
      reset(defaultValues)
    }
  }, [defaultValues])

  const onSubmit = async (data) => {
    return (await props.onSubmit(data)) && toggleVisible(false)
  }

  return (
    <FormProvider {...methods}>
      <form id='edit-form-drawer' onSubmit={handleSubmit(onSubmit)}>
        <Drawer
          title={title}
          visible={visible}
          maskClosable={false}
          closable
          destroyOnClose
          onClose={() => toggleVisible(false)}
          width='50vw'
          placement={language === 'ar' ? 'left' : 'right'}
          footer={
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Button
                onClick={() => toggleVisible(false)}
                style={{ marginInlineEnd: 8 }}
              >
                {t('cancel')}
              </Button>
              <Button
                htmlType='submit'
                form='edit-form-drawer'
                type='primary'
                loading={submitLoading}
              >
                {t('submit')}
              </Button>
            </div>
          }
        >
          <Spin spinning={loading}>{children}</Spin>
        </Drawer>
      </form>
    </FormProvider>
  )
}

export default EditFormDrawer
