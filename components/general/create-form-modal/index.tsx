import { Button, Modal } from 'antd'

import React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

interface IFormModal<T> {
  id?: string
  title: string
  visible: boolean
  toggleVisible: (visible: boolean) => void
  children?: React.ReactNode
  onSubmit?: (data: any) => Promise<Boolean>
  submitLoading: boolean
  submitText?: string
  width?: string | number
}

function CreateFormModal<T>(props: IFormModal<T>) {
  const {
    id,
    title,
    toggleVisible,
    visible,
    children,
    submitLoading,
    submitText,
    width,
  } = props

  const {
    t,
    i18n: { language },
  } = useTranslation()

  const methods = useForm()
  const { handleSubmit } = methods

  const onSubmit = async (data) => {
    return (await props.onSubmit(data)) && toggleVisible(false)
  }

  return (
    <FormProvider {...methods}>
      <form id={id ?? 'create-form-modal'} onSubmit={handleSubmit(onSubmit)}>
        <Modal
          title={title}
          visible={visible}
          maskClosable={true}
          closable
          destroyOnClose
          centered
          width={width}
          onCancel={() => toggleVisible(false)}
          footer={
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Button
                onClick={() => toggleVisible(false)}
                style={{ marginInlineEnd: 8 }}
              >
                {t('cancel')}
              </Button>
              <Button
                htmlType='submit'
                form={id ?? 'create-form-modal'}
                type='primary'
                loading={submitLoading}
              >
                {submitText ?? t('submit')}
              </Button>
            </div>
          }
        >
          {children}
        </Modal>
      </form>
    </FormProvider>
  )
}

export default CreateFormModal
