import GoogleMapReact from 'google-map-react'
import { useEffect, useState } from 'react'
import {
  MAP_DEFAULT_LOCATION,
  MAP_DEFAULT_ZOOM,
} from '../../../utils/constants'
import { ILatLng } from '../location-picker'

const Marker = (_: ILatLng) => {
  return (
    <div style={{ position: 'absolute', bottom: 0, left: '-25px' }}>
      <img src='/images/marker.png' width='48' />
    </div>
  )
}

interface ILocationPickerProps {
  value?: ILatLng
  height?: string
  width?: string
}

const LocationMap: React.FC<ILocationPickerProps> = ({
  value,
  height,
  width,
}) => {
  const [state, setState] = useState(MAP_DEFAULT_LOCATION)

  useEffect(() => {
    value && setState(value)
  }, [value])

  const defaultProps = {
    center: MAP_DEFAULT_LOCATION,
    zoom: MAP_DEFAULT_ZOOM,
  }

  return (
    <div style={{ height: height ?? '300px', width: width ?? '100%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{
          key: process.env.NEXT_PUBLIC_GOOGLE_MAP_KEY,
        }}
        defaultCenter={state ?? defaultProps.center}
        center={state ?? defaultProps.center}
        defaultZoom={defaultProps.zoom}
        yesIWantToUseGoogleMapApiInternals
        options={{ fullscreenControl: false }}
      >
        <Marker lat={state.lat} lng={state.lng} />
      </GoogleMapReact>
    </div>
  )
}

export default LocationMap
