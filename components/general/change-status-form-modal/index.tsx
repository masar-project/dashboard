import { Button, Modal, Spin } from 'antd'

import React, { useEffect } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

interface IFormDrawer<T> {
  title: string
  visible: boolean
  toggleVisible: (visible: boolean) => void
  defaultValues?: T
  children?: React.ReactNode
  onSubmit?: (data) => void
  confirmButtonContent: string
  loading: boolean
}

function ChangeStatusFormModal<T>(props: IFormDrawer<T>) {
  const {
    title,
    toggleVisible,
    visible,
    defaultValues,
    children,
    confirmButtonContent,
    loading,
  } = props

  const {
    t,
    i18n: { language },
  } = useTranslation()

  const methods = useForm<any>({
    defaultValues: { ...defaultValues },
  })

  const { handleSubmit, reset } = methods

  useEffect(() => {
    if (defaultValues) {
      reset(defaultValues)
    }
  }, [defaultValues])

  const onSubmit = (data) => {
    toggleVisible(false)
    props.onSubmit(data)
  }

  return (
    <FormProvider {...methods}>
      <form id='unsuspend-form-drawer' onSubmit={handleSubmit(onSubmit)}>
        <Modal
          title={title}
          visible={visible}
          closable
          destroyOnClose
          onCancel={() => toggleVisible(false)}
          afterClose={() => toggleVisible(false)}
          width='50vw'
          footer={
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Button
                onClick={() => toggleVisible(false)}
                style={{ marginInlineEnd: 8 }}
              >
                {t('cancel')}
              </Button>
              <Button
                htmlType='submit'
                form='unsuspend-form-drawer'
                type='primary'
                // danger
              >
                {confirmButtonContent}
              </Button>
            </div>
          }
        >
          <Spin spinning={loading}>{children}</Spin>
        </Modal>
      </form>
    </FormProvider>
  )
}

export default ChangeStatusFormModal
