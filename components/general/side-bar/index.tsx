import React, { useContext, useState } from 'react'
import { Layout, Menu } from 'antd'
import AppContext from '../../../contexts/app/context'
import cn from 'classnames'
import styles from './style.module.scss'
import { routes } from '../../../routes'
import { useRouter } from 'next/router'
import { useTranslation } from 'react-i18next'
import AuthContext from '../../../contexts/auth/context'
import { isArray } from 'lodash'

const { Sider } = Layout
const { SubMenu } = Menu

const SideBar: React.FC = () => {
  const { pathname, push } = useRouter()
  const { t } = useTranslation()
  const { direction, screenSize } = useContext(AppContext)
  const { grantedPermissions } = useContext(AuthContext)

  // Get Keys To Open SubMenu
  const paths = pathname.split('/')
  const keys = paths.splice(1, paths.length)

  return (
    <Sider
      theme='dark'
      width='300'
      className={cn(styles.sider, {
        [styles.siderLeft]: direction === 'ltr',
        [styles.siderRight]: direction === 'rtl',
      })}
      collapsed={screenSize === 'mobileOrTablet'}
    >
      <Menu
        theme='dark'
        mode='inline'
        defaultSelectedKeys={[pathname]}
        defaultOpenKeys={keys?.map((key) => `/${key}`)}
        style={{ padding: '0 0 3rem 0' }}
      >
        {routes.map(
          ({ icon, labelKey, hasSubMenus, subMenus, path, permissions }) => {
            let subMenusPermission = []
            if (hasSubMenus) {
              subMenusPermission = subMenus?.flatMap((sub) => sub?.permissions)
            }

            return (
              grantedPermissions.find((p) =>
                hasSubMenus
                  ? subMenusPermission.includes(p)
                  : permissions.includes(p)
              ) &&
              (!hasSubMenus ? (
                <Menu.Item key={path} icon={icon} onClick={() => push(path)}>
                  {t(labelKey)}
                </Menu.Item>
              ) : (
                <SubMenu key={path} icon={icon} title={t(labelKey)}>
                  {subMenus?.map(({ icon, labelKey, path, permissions }) => {
                    return (
                      grantedPermissions.find(
                        isArray(permissions)
                          ? (p) => permissions.includes(p)
                          : (p) => p === permissions
                      ) && (
                        <Menu.Item
                          icon={icon}
                          key={path}
                          onClick={() => push(path)}
                        >
                          {t(labelKey)}
                        </Menu.Item>
                      )
                    )
                  })}
                </SubMenu>
              ))
            )
          }
        )}
      </Menu>
    </Sider>
  )
}

export default SideBar
