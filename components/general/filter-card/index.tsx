import React from 'react'
import { Divider, Input, Row, Switch, Tooltip } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import { useTranslation } from 'react-i18next'
import Can from '../../../utils/rbac/can'
const { Search } = Input

import styles from './style.module.scss'

interface ISearchFilterCard {
  onSearchChange: (event: any) => void
  onSearch: (searchKey: string) => void
  withTrashed?: {
    perform: string | string[]
    checked: boolean
    loading: boolean
    onChange: (checked: boolean) => void
  }
}

const SearchFilterCard: React.FC<ISearchFilterCard> = ({
  children,
  onSearchChange,
  onSearch,
  withTrashed,
}) => {
  const { t } = useTranslation()

  return (
    <div className={styles.filterCard}>
      <div
        style={{ display: 'flex' }}
        className={React.Children.count(children) > 0 && styles.cardHeader}
      >
        <Search
          placeholder={t('search_hint')}
          onChange={onSearchChange}
          onSearch={onSearch}
          enterButton
          allowClear
        />
        {withTrashed && (
          <Can
            perform={withTrashed?.perform}
            yes={() => {
              return (
                <>
                  <Divider type='vertical' style={{ height: 'auto' }} />
                  <Tooltip title={t('deleted_data')}>
                    <Switch
                      style={{ margin: '0.3rem 1rem 0 1rem' }}
                      checkedChildren={<CheckOutlined />}
                      unCheckedChildren={<CloseOutlined />}
                      defaultChecked={false}
                      checked={withTrashed?.checked}
                      loading={withTrashed?.loading}
                      onChange={withTrashed?.onChange}
                    />
                  </Tooltip>
                </>
              )
            }}
          />
        )}
      </div>

      <Row gutter={16}>{children}</Row>
    </div>
  )
}

export default SearchFilterCard
