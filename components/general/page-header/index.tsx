import React from 'react'
import { PageHeader as AntdPageHeader } from 'antd'
import { TagType } from 'antd/lib/tag'

import styles from './style.module.scss'
interface IPageHeaderProps {
  title: string
  subTitle?: string
  extra?: React.ReactNode
  // tags?: React.ReactElement<TagType> | React.ReactElement<TagType>[]
  onBack?: () => void
}

const PageHeader: React.FC<IPageHeaderProps> = (props) => {
  return <AntdPageHeader ghost={false} className={styles.header} {...props} />
}

export default PageHeader
