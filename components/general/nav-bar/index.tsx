import Link from 'next/link'
import React, { useContext, useEffect, useState } from 'react'
import { Avatar, Button, List, Menu, Popover } from 'antd'
import {
  GlobalOutlined,
  SettingOutlined,
  UserOutlined,
} from '@ant-design/icons'
import AppContext from '../../../contexts/app/context'
import { useTranslation } from 'react-i18next'
import AuthContext from '../../../contexts/auth/context'
import ProfileCard from '../../auth/profile-card'
import { horizontalRoutes } from '../../../routes'

import styles from './style.module.scss'
import { Header } from 'antd/lib/layout/layout'
import { useRouter } from 'next/router'

interface IProps {}
const NavBar: React.FC<IProps> = () => {
  const { direction, languages } = useContext(AppContext)
  const { t, i18n } = useTranslation()
  const { push, pathname } = useRouter()

  // Languages
  const [languagePopoverVisible, setLanguagePopoverVisible] = useState(false)

  const { employeeDetails } = useContext(AuthContext)

  const [employee, setEmployee] = useState(employeeDetails)

  useEffect(() => {
    employeeDetails && setEmployee(employeeDetails)
  }, [employeeDetails])

  // User Popover
  const [userPopoverVisible, setUserPopoverVisible] = useState(false)

  return (
    <Header className={styles.header}>
      <div className={styles.brand}>
        <Link href='/'>
          <img className={styles.logo} src='/images/masar-logo.png' />
        </Link>
      </div>
      <div className={styles.tabs}>
        <Menu
          theme='dark'
          mode='horizontal'
          defaultSelectedKeys={[pathname]}
          className={styles.menu}
        >
          {horizontalRoutes?.map((route, _) => {
            return (
              <Menu.Item key={route?.path} onClick={() => push(route?.path)}>
                {route?.icon}
                {t(`${route?.labelKey}`)}
              </Menu.Item>
            )
          })}
        </Menu>
      </div>
      <div className={styles.actionsContainer}>
        {/* Languages */}
        {languages?.data?.length > 1 && (
          <Popover
            visible={languagePopoverVisible}
            onVisibleChange={setLanguagePopoverVisible}
            placement={direction === 'rtl' ? 'bottom' : 'bottom'}
            content={
              <>
                <List
                  size='small'
                  dataSource={languages?.data}
                  renderItem={(language) => (
                    <List.Item
                      className={styles.cursorPointer}
                      onClick={() => {
                        i18n?.changeLanguage(language.code)

                        setLanguagePopoverVisible(false)
                      }}
                    >
                      <span className={styles.mStart05}>{language.name}</span>
                    </List.Item>
                  )}
                />
              </>
            }
            trigger='click'
          >
            <div className={styles.languageContainer}>
              <span className={styles.mx1}>
                <Button shape='circle' icon={<GlobalOutlined />} />
              </span>
            </div>
          </Popover>
        )}

        <Menu
          theme='dark'
          mode='horizontal'
          defaultSelectedKeys={[pathname]}
          // className={styles.menu}
        >
          <Menu.Item onClick={() => push('/dashboard/settings/employees')}>
            <SettingOutlined />
            {/* {t('settings')} */}
          </Menu.Item>
        </Menu>

        {/* User */}
        <Popover
          placement={direction === 'rtl' ? 'bottomLeft' : 'bottomRight'}
          content={
            <ProfileCard toogleUserPopoverVisible={setUserPopoverVisible} />
          }
          overlayClassName='avatar-popover'
          trigger='click'
          visible={userPopoverVisible}
          onVisibleChange={setUserPopoverVisible}
        >
          <div className={styles.avatarContainer}>
            <label className={styles.userName}>
              {employee?.user?.firstName}
            </label>
            <UserOutlined />
          </div>
        </Popover>
      </div>
    </Header>
  )
}

export default NavBar
