import { Spin } from 'antd'
import React from 'react'

import styles from './style.module.scss'

interface IProps {
  height?: number
}


const LoadingPage:React.FC<IProps> = ({height = 150}) => {
  return (
    <div className={styles.container}>
      <img src="/images/masar-logo.png" height={height} />
    </div>
  )
}

export default LoadingPage
