import { Button, Modal, Spin } from 'antd'

import React, { useEffect } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

interface IFormModal<T> {
  id: string
  title: string
  visible: boolean
  toggleVisible: (visible: boolean) => void
  defaultValues?: T
  children?: React.ReactNode
  onSubmit?: (data) => Promise<Boolean>
  confirmButtonText: string
  loading: boolean
  danger?: boolean
  maskClosable?: boolean
  style?: React.CSSProperties
  width?: string | number
}

function FormModal<T>(props: IFormModal<T>) {
  const {
    id,
    title,
    toggleVisible,
    visible,
    defaultValues,
    children,
    confirmButtonText,
    loading,
    danger,
    maskClosable = true,
    style,
    width,
  } = props

  const {
    t,
    i18n: { language },
  } = useTranslation()

  const methods = useForm<any>({
    defaultValues: { ...defaultValues },
  })

  const { handleSubmit, reset } = methods

  useEffect(() => {
    if (defaultValues) {
      reset(defaultValues)
    }
  }, [defaultValues])

  const onSubmit = async (data) => {
    return (await props.onSubmit(data)) && toggleVisible(false)
  }

  return (
    <FormProvider {...methods}>
      <form id={id} onSubmit={handleSubmit(onSubmit)}>
        <Modal
          key={id}
          title={title}
          visible={visible}
          closable
          maskClosable={maskClosable}
          destroyOnClose
          onCancel={() => toggleVisible(false)}
          afterClose={() => toggleVisible(false)}
          width={width ?? '50vw'}
          style={style}
          footer={
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Button
                onClick={() => toggleVisible(false)}
                style={{ marginInlineEnd: 8 }}
              >
                {t('cancel')}
              </Button>
              <Button
                htmlType='submit'
                form={id}
                type='primary'
                loading={loading}
                danger={danger}
              >
                {confirmButtonText}
              </Button>
            </div>
          }
        >
          <Spin spinning={loading}>{children}</Spin>
        </Modal>
      </form>
    </FormProvider>
  )
}

export default FormModal
