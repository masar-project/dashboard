import { Button, Modal, Spin } from 'antd'

import React, { useEffect } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

interface IFormModal<T> {
  title: string
  visible: boolean
  toggleVisible: (visible: boolean) => void
  defaultValues?: T
  children?: React.ReactNode
  onSubmit?: (data: any) => Promise<Boolean>
  loading?: boolean
  submitLoading: boolean
  width?: string | number
}

function EditFormModal<T>(props: IFormModal<T>) {
  const {
    defaultValues,
    title,
    toggleVisible,
    visible,
    children,
    loading,
    submitLoading,
    width,
  } = props

  const {
    t,
    i18n: { language },
  } = useTranslation()

  const methods = useForm<any>({
    defaultValues: { ...defaultValues },
  })

  const { handleSubmit, reset } = methods

  useEffect(() => {
    if (defaultValues) {
      reset(defaultValues)
    }
  }, [defaultValues])

  const onSubmit = async (data) => {
    return (await props.onSubmit(data)) && toggleVisible(false)
  }

  return (
    <FormProvider {...methods}>
      <form id='edit-form-modal' onSubmit={handleSubmit(onSubmit)}>
        <Modal
          title={title}
          visible={visible}
          maskClosable={true}
          closable
          destroyOnClose
          centered
          width={width}
          onCancel={() => toggleVisible(false)}
          footer={
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <Button
                onClick={() => toggleVisible(false)}
                style={{ marginInlineEnd: 8 }}
              >
                {t('cancel')}
              </Button>
              <Button
                htmlType='submit'
                form='edit-form-modal'
                type='primary'
                loading={submitLoading}
              >
                {t('submit')}
              </Button>
            </div>
          }
        >
          <Spin spinning={loading}>{children}</Spin>
        </Modal>
      </form>
    </FormProvider>
  )
}

export default EditFormModal
