import React from 'react'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import styles from './style.module.scss'

const Loader: React.FC<{ spinning: boolean; pathName: string }> = ({
  spinning,
  pathName,
  children,
}) => {
  return (
    <Spin
      spinning={
        pathName == '/login' || pathName == '/register'
          ? false
          : spinning ?? true
      }
      indicator={
        <div className={styles.spinnerContainer}>
          <LoadingOutlined />
        </div>
      }
      style={{ maxHeight: '100vw' }}
    >
      {children}
    </Spin>
  )
}

export default Loader
