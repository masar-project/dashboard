import { useTranslation } from 'react-i18next'
import Img from '../img'

interface IImagePlaceholderProps {
  className?: string
}

const ImagePlaceholder: React.FC<IImagePlaceholderProps> = ({ className }) => {
  const { t } = useTranslation()

  return (
    <div className={className}>
      <Img src='/images/placeholder1.png' />
    </div>
  )
}

export default ImagePlaceholder
