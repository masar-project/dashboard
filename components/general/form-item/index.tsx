import React from 'react'
import { useTranslation } from 'react-i18next'
import styles from './style.module.scss'

interface IFormItemProps {
  label: string
  isOptional?: boolean
}

const FormItem: React.FC<IFormItemProps> = ({
  label,
  isOptional,
  children,
}) => {
  const { t } = useTranslation()

  return (
    <div className={styles.formItem}>
      <label className={styles.formItemLabel}>
        {label} {isOptional && <span>({t('optional')})</span>}
      </label>
      {children}
    </div>
  )
}

export default FormItem
