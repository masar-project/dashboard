import React, { useState } from 'react'
import {
  Layout,
  Menu,
  Row,
  Col,
  Popover,
  Calendar,
  Card,
  Tabs,
  Tag,
  Button,
} from 'antd'
import { useTranslation } from 'react-i18next'
import { SlidersOutlined } from '@ant-design/icons'
import { FaCalendarAlt } from 'react-icons/fa'

const { TabPane } = Tabs

import styles from './style.module.scss'
import moment from 'moment'
import {
  DATE_FORMATE,
  DATE_FORMATE_FOR_MESSAGE,
  DAY_FORMATE,
} from '@/utils/constants'
import { formatDate } from '../../../utils/helpers/format'

const { Footer: AntdFooter } = Layout

interface IMenuProps {
  onCalendarChange?: (date: moment.Moment) => void
}

const FilterMenu: React.FC<IMenuProps> = ({ onCalendarChange }) => {
  const { t } = useTranslation()
  return (
    <Tabs defaultActiveKey='1' tabPosition='right' type='card'>
      <TabPane tab={t('date')} key='1'>
        <div className={styles.tabContent}>
          <Calendar
            fullscreen={false}
            defaultValue={moment()}
            onSelect={onCalendarChange}
          />
        </div>
      </TabPane>
      <TabPane tab={t('trip_status')} key='2'>
        <Card className={styles.tabContent}></Card>
      </TabPane>
      <TabPane tab={t('point_status')} key='3'>
        <Card className={styles.tabContent}></Card>
      </TabPane>
    </Tabs>
  )
}

interface IPorps {
  onDateChange?: (date: moment.Moment) => void
}

const ManageFooter: React.FC<IPorps> = ({ onDateChange }) => {
  const { t } = useTranslation()

  const [date, setDate] = useState<moment.Moment>(moment())
  return (
    <AntdFooter className={styles.footer}>
      <Row>
        <Col span={24} lg={8}></Col>
        <Col span={24} lg={8}>
          <span>{`جميع الحقوق محفوظة © 2021 `}</span>
          <img src='/images/masar-logo.png' height='40' />
        </Col>
        <Col span={24} lg={8} className='d-flex-end'>
          <Button
            className='mx-2'
            type='ghost'
            shape='round'
            icon={<FaCalendarAlt />}
            size='small'
          >
            {date.format(DAY_FORMATE)}
          </Button>
          <Popover
            overlayClassName='filterPopover'
            content={
              <FilterMenu
                onCalendarChange={(value) => {
                  setDate(value)
                  onDateChange(value)
                }}
              />
            }
            trigger={['click']}
            placement='topLeft'
          >
            <Menu theme='dark' mode='horizontal'>
              <Menu.Item>
                <SlidersOutlined />
                {t('filter')}
              </Menu.Item>
            </Menu>
          </Popover>
        </Col>
      </Row>
    </AntdFooter>
  )
}

export default ManageFooter
