import React, { useContext } from 'react'
import { Button, Divider, Dropdown, Input, Layout, Menu } from 'antd'
import AppContext from '@/contexts/app/context'
import { useTranslation } from 'react-i18next'
import cn from 'classnames'
import { IDefaultTrip } from '@/models/default-trip/response'
import { IPOS } from '@/models/pos/response'
import IMapFilter from '@/models/base/filter-items'
import DefaultTripsList from '@/components/default-trip/default-trip-list'
import PreviewTrip from '@/components/default-trip/preview-trip'

import styles from './style.module.scss'
import moment from 'moment'
import CustomizedPOSContextProvider from '@/contexts/customized-pos/provider'
import TodayTripsList from '@/components/trip/today-trips-list'
import { ITrip } from '@/models/trip/response'
import { ISortedPOS } from '../../../models/trip/response'

const { Sider } = Layout

interface IProps {
  selectedDefaultTrip?: IDefaultTrip
  onDefaultTripSelected?: (defaultTrip: IDefaultTrip) => void

  defaultSelectedPOS: IPOS
  setDefaultSelectedPOS: (pos: IPOS) => void

  selectedTrip?: ITrip
  onTripSelected?: (defaultTrip: ITrip) => void

  todaySelectedPOS: ISortedPOS
  setTodaySelectedPOS: (pos: ISortedPOS) => void

  filters: IMapFilter
  showCustomizedActions: boolean
}

const SideBar: React.FC<IProps> = ({
  onDefaultTripSelected,
  selectedDefaultTrip,

  selectedTrip,
  onTripSelected,
  todaySelectedPOS,
  setTodaySelectedPOS,

  setDefaultSelectedPOS,
  defaultSelectedPOS,
  filters,

  showCustomizedActions,
}) => {
  const { t } = useTranslation()
  const { direction, screenSize } = useContext(AppContext)

  const renderList = () => {
    // Old Trips
    if (moment().isAfter(filters.date, 'day')) {
    }
    // Today Trips
    else if (moment().isSame(filters.date, 'day')) {
      return (
        <>
          <TodayTripsList
            filters={filters}
            selectedTrip={selectedTrip}
            onTripSelected={onTripSelected}
            todaySelectedPOS={todaySelectedPOS}
            setTodaySelectedPOS={setTodaySelectedPOS}
          />
        </>
      )
    }
    // New Trips
    else {
      return (
        <>
          {/* Default Trips List */}
          <DefaultTripsList
            filters={filters}
            selectedDefaultTrip={selectedDefaultTrip}
            onDefaultTripSelected={onDefaultTripSelected}
          />
          <Divider />

          {/* Preview Trip */}
          <PreviewTrip
            selectedPOS={defaultSelectedPOS}
            setSelectedPOS={setDefaultSelectedPOS}
            filters={filters}
            defaultTrip={selectedDefaultTrip}
          />
        </>
      )
    }
  }

  return (
    <Sider
      theme='dark'
      width='300'
      className={
        cn(styles.sider, {
          [styles.siderLeft]: direction === 'ltr',
          [styles.siderRight]: direction === 'rtl',
        }) + ' manageSidebar'
      }
      collapsed={screenSize === 'mobileOrTablet'}
    >
      {renderList()}
    </Sider>
  )
}

const ManageSideBar: React.FC<IProps> = (props) => {
  return (
    <CustomizedPOSContextProvider>
      <SideBar {...props} />
    </CustomizedPOSContextProvider>
  )
}

export default ManageSideBar
