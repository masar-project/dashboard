import React, { useContext, useState } from 'react'
import { Avatar, Badge, List, Typography } from 'antd'
import { useTranslation } from 'react-i18next'
import DefaultTripContext from '@/contexts/default-trip/context'
import cn from 'classnames'
import { IDefaultTrip } from '@/models/default-trip/response'
import IMapFilter from '@/models/base/filter-items'

import styles from './style.module.scss'
import CustomizedPOSModal from '@/components/customized-pos/customized-pos-modal'
import { FILTER_DATE_FORMATE } from '../../../utils/constants'

const { Text } = Typography

interface IProps {
  selectedDefaultTrip?: IDefaultTrip
  onDefaultTripSelected?: (defaultTrip: IDefaultTrip) => void
  filters: IMapFilter
}

const DefaultTripsList: React.FC<IProps> = ({
  onDefaultTripSelected,
  selectedDefaultTrip,
  filters,
}) => {
  const { t } = useTranslation()

  const { data: defaultTrips, loading: defaultTripsLoading } =
    useContext(DefaultTripContext)

  return (
    <>
      <List
        header={
          <Text strong>
            {t('trips_on_day')} {t(`${filters?.date?.format('dddd')}`)}
          </Text>
        }
        className='tripsList'
        dataSource={defaultTrips?.data ?? []}
        loading={defaultTripsLoading}
        renderItem={(defaultTrip) => (
          <List.Item
            key={defaultTrip?.id}
            className={cn(styles.trip, {
              [styles.active]: selectedDefaultTrip?.id == defaultTrip?.id,
            })}
            onClick={() => {
              onDefaultTripSelected(defaultTrip)
            }}
          >
            <List.Item.Meta
              style={{ padding: '0 1rem' }}
              avatar={<Badge color='#ccc' />}
              description={
                <>
                  <Text>
                    {defaultTrip?.name} ({defaultTrip?.pointsOfSaleCount})
                  </Text>
                </>
              }
            />
          </List.Item>
        )}
        split={false}
      />
    </>
  )
}

export default DefaultTripsList
