import React, { useContext, useEffect, useState } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Input, Select, Row, Col, Typography, Divider, List } from 'antd'
import { ErrorMessage } from '@hookform/error-message'
import { IDefaultTripDetails } from '@/models/default-trip/response'
import AppContext from '@/contexts/app/context'
import FormItem from '@/components/general/form-item'
import dynamic from 'next/dynamic'
import { TripDays } from '../../../models/default-trip/enum'
import styles from './style.module.scss'
import POSContext from '@/contexts/pos/context'
import { IPOS } from '../../../models/pos/response'
import { DEFAULT_MAXIMUM_PAGE_SIZE } from '../../../contexts/constants'
import { PointStatus } from '@/models/pos/enum'
import { isArray, isEmpty } from 'lodash'
import LoadingPage from '@/components/general/loading-page'
const { Title } = Typography

const OurMap = dynamic(() => import('@/components/mapbox/map/index'), {
  ssr: false,
  loading: () => <LoadingPage height={100} />,
})

interface IDefaultTripFormProps {
  defaultTrip?: IDefaultTripDetails
}

const DefaultTripForm: React.FC<IDefaultTripFormProps> = ({ defaultTrip }) => {
  const { control, errors, getValues, setValue } = useFormContext()
  const { t } = useTranslation()
  const {
    loading: appLoading,
    languages,
    cities,
    drivers,
  } = useContext(AppContext)

  const [points, setPoints] = useState<IPOS[]>([])
  const {
    data: poss,
    loading: posLoading,
    actions: posActions,
  } = useContext(POSContext)

  useEffect(() => {
    posActions.setPageSize(DEFAULT_MAXIMUM_PAGE_SIZE)
  }, [])

  useEffect(() => {
    poss && setPoints(poss?.data)
  }, [poss])

  const [selectedPoints, setSelectedPoints] = useState<number[]>(
    defaultTrip?.pointsOfSale?.map((p) => p.id)
  )

  useEffect(() => {
    defaultTrip &&
      setSelectedPoints(defaultTrip?.pointsOfSale?.map((p) => p.id))
  }, [defaultTrip])

  const addPoint = (id: number) => {
    if (selectedPoints && isArray(selectedPoints) && !isEmpty(selectedPoints)) {
      if (selectedPoints?.includes(id))
        setSelectedPoints(selectedPoints?.filter((p) => p !== id))
      else {
        let s = []
        selectedPoints?.forEach((p) => s.push(p))
        s.push(id)
        setSelectedPoints(s)
      }
    } else {
      setSelectedPoints([id])
    }
  }

  // City Id
  const [cityId, setCityId] = useState<number>()
  const [regionId, setRegionId] = useState<number>()

  // Regions
  const [regions, setRegions] = useState([])

  useEffect(() => {
    if (cityId) {
      setRegions(
        cities?.data?.filter((city) => city?.id === cityId)[0]?.regions
      )

      posActions.setPageSize(DEFAULT_MAXIMUM_PAGE_SIZE)
      posActions.setFilter({
        region_id: regionId,
        city_id: cityId,
      })
    }
  }, [cityId])

  useEffect(() => {
    if (regionId) {
      posActions.setPageSize(DEFAULT_MAXIMUM_PAGE_SIZE)
      posActions.setFilter({
        region_id: regionId,
        city_id: cityId,
      })
    }
  }, [regionId])

  return (
    <Row gutter={[12, 12]}>
      <Col span={8}>
        <Row gutter={12}>
          {/* Name */}
          <Col span={24}>
            <FormItem label={t('name')}>
              <Controller
                as={Input}
                name='name'
                control={control}
                rules={{ required: `${t('field_is_required_message')}` }}
              />

              <ErrorMessage
                errors={errors}
                name='name'
                render={({ message }) => (
                  <p className={styles.alert}>{message}</p>
                )}
              />
            </FormItem>
          </Col>

          {/* Day */}
          <Col span={12}>
            <FormItem label={t('day')}>
              <Controller
                rules={{ required: `${t('field_is_required_message')}` }}
                render={({ onChange, onBlur, value }) => {
                  return (
                    <Select
                      style={{ width: '100%' }}
                      placeholder={t('please_select')}
                      onChange={onChange}
                      onBlur={onBlur}
                      value={value}
                      optionLabelProp='label'
                      options={
                        [
                          {
                            value: TripDays.Saturday,
                            label: t(`${TripDays[TripDays.Saturday]}`),
                          },
                          {
                            value: TripDays.Sunday,
                            label: t(`${TripDays[TripDays.Sunday]}`),
                          },
                          {
                            value: TripDays.Monday,
                            label: t(`${TripDays[TripDays.Monday]}`),
                          },
                          {
                            value: TripDays.Tuesday,
                            label: t(`${TripDays[TripDays.Tuesday]}`),
                          },
                          {
                            value: TripDays.Wednesday,
                            label: t(`${TripDays[TripDays.Wednesday]}`),
                          },
                          {
                            value: TripDays.Thursday,
                            label: t(`${TripDays[TripDays.Thursday]}`),
                          },
                          {
                            value: TripDays.Friday,
                            label: t(`${TripDays[TripDays.Friday]}`),
                          },
                        ] ?? []
                      }
                    />
                  )
                }}
                name='day'
                control={control}
              />

              <ErrorMessage
                errors={errors}
                name='day'
                render={({ message }) => (
                  <p className={styles.alert}>{message}</p>
                )}
              />
            </FormItem>
          </Col>

          {/* Driver */}
          <Col span={12}>
            <FormItem label={t('driver')}>
              <Controller
                rules={{ required: `${t('field_is_required_message')}` }}
                render={({ onChange, onBlur, value }) => {
                  return (
                    <Select
                      style={{ width: '100%' }}
                      placeholder={t('please_select')}
                      onChange={onChange}
                      onBlur={onBlur}
                      value={value}
                      optionLabelProp='label'
                      options={
                        drivers?.data?.map((driver) => {
                          return {
                            value: driver.id,
                            label: driver.user?.firstName,
                          }
                        }) ?? []
                      }
                    />
                  )
                }}
                name='driver_id'
                control={control}
              />

              <ErrorMessage
                errors={errors}
                name='driver_id'
                render={({ message }) => (
                  <p className={styles.alert}>{message}</p>
                )}
              />
            </FormItem>
          </Col>

          {/* Points of sale */}
          <Col span={24}>
            <Divider />
            <Title level={4}>{t('poss')}</Title>
            <Row gutter={12}>
              {/* City */}
              <Col span={12}>
                <FormItem label={t('city')}>
                  <Controller
                    render={({ onChange, onBlur, value }) => {
                      return (
                        <Select
                          allowClear
                          style={{ width: '100%' }}
                          placeholder={t('please_select')}
                          onChange={(value) => {
                            setCityId(value)
                            onChange(value)
                          }}
                          onBlur={onBlur}
                          value={value}
                          optionLabelProp='label'
                          options={
                            cities?.data?.map((city) => {
                              return {
                                value: city.id,
                                label: city.nameTranslations.ar,
                              }
                            }) ?? []
                          }
                        />
                      )
                    }}
                    name='city_id'
                    control={control}
                  />

                  <ErrorMessage
                    errors={errors}
                    name='city_id'
                    render={({ message }) => (
                      <p className={styles.alert}>{message}</p>
                    )}
                  />
                </FormItem>
              </Col>

              {/* Region */}
              <Col span={12}>
                <FormItem label={t('region')}>
                  <Controller
                    render={({ onChange, onBlur, value }) => {
                      return (
                        <Select
                          allowClear
                          style={{ width: '100%' }}
                          placeholder={t('please_select')}
                          onChange={(value) => {
                            setRegionId(value)
                            onChange(value)
                          }}
                          onBlur={onBlur}
                          value={value}
                          optionLabelProp='label'
                          options={
                            regions?.map((region) => {
                              return { value: region.id, label: region.name }
                            }) ?? []
                          }
                        />
                      )
                    }}
                    name='region_id'
                    control={control}
                  />

                  <ErrorMessage
                    errors={errors}
                    name='region_id'
                    render={({ message }) => (
                      <p className={styles.alert}>{message}</p>
                    )}
                  />
                </FormItem>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <FormItem label={t('selected_poss')}>
                  <Controller
                    rules={{ required: `${t('field_is_required_message')}` }}
                    render={({ onChange, onBlur, value }) => {
                      return (
                        <Select
                          mode='multiple'
                          allowClear
                          style={{ width: '100%' }}
                          placeholder={t('please_select')}
                          onChange={(value) => {
                            setSelectedPoints(isArray(value) ? value : [value])
                            onChange(value)
                          }}
                          onBlur={onBlur}
                          value={selectedPoints}
                          optionLabelProp='label'
                          options={
                            points?.map((point) => {
                              return { value: point.id, label: point.name }
                            }) ?? []
                          }
                        />
                      )
                    }}
                    name='point_of_sale_ids'
                    control={control}
                  />

                  <ErrorMessage
                    errors={errors}
                    name='point_of_sale_ids'
                    render={({ message }) => (
                      <p className={styles.alert}>{message}</p>
                    )}
                  />
                </FormItem>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>

      {/* Map */}
      <Col span={16}>
        <OurMap
          coordinates={points?.map((point, _) => {
            return {
              id: point?.id,
              long: point?.long,
              lat: point?.lat,
              status: selectedPoints?.includes(point?.id)
                ? PointStatus.Primary
                : PointStatus.Disable,
              onClick: () => {
                addPoint(point?.id)
              },
            }
          })}
        />
      </Col>
    </Row>
  )
}

export default DefaultTripForm
