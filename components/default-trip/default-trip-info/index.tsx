import React, { useEffect, useState } from 'react'
import { IDefaultTripDetails } from '@/models/default-trip/response'
import { Descriptions, Input, List, Tabs } from 'antd'
import { useTranslation } from 'react-i18next'
import { SearchOutlined } from '@ant-design/icons'

const { TabPane } = Tabs

interface IProps {
  defaultTrip: IDefaultTripDetails
}

const DefaultTripInfo: React.FC<IProps> = ({ defaultTrip }) => {
  const { t } = useTranslation()

  const [pointsOfSale, setPointsOfSale] = useState(defaultTrip?.pointsOfSale)

  useEffect(() => {
    defaultTrip?.pointsOfSale && setPointsOfSale(defaultTrip?.pointsOfSale)
  }, [defaultTrip?.pointsOfSale])

  return (
    <>
      <Descriptions
        bordered
        size='small'
        column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
      >
        {/* Id */}
        <Descriptions.Item label={t('id')}>
          {`#${defaultTrip?.id}`}
        </Descriptions.Item>

        {/* Name */}
        <Descriptions.Item label={t('name')}>
          {defaultTrip?.name}
        </Descriptions.Item>

        {/* Driver */}
        <Descriptions.Item label={t('driver')}>
          {`${defaultTrip?.driver?.user?.firstName} ${defaultTrip?.driver?.user?.lastName}`}
        </Descriptions.Item>

        {/* Points of sale count */}
        <Descriptions.Item label={t('points_of_sale_count')}>
          {defaultTrip?.pointsOfSaleCount}
        </Descriptions.Item>

        {/* created_at */}
        <Descriptions.Item label={t('created_at')}>
          {defaultTrip?.createdAt}
        </Descriptions.Item>
      </Descriptions>

      {/* Tabs */}
      <Tabs defaultActiveKey='1' animated>
        <TabPane tab={t('poss')} key='1'>
          <List
            header={
              <Input
                placeholder={t('search_hint')}
                suffix={<SearchOutlined />}
                allowClear
                onChange={(event) => {
                  let searchKey = event.target.value

                  let filteredPermissions = defaultTrip?.pointsOfSale?.filter(
                    (pointsOfSale) => pointsOfSale?.name?.includes(searchKey)
                  )

                  setPointsOfSale(filteredPermissions)

                  if (searchKey == '') {
                    setPointsOfSale(defaultTrip?.pointsOfSale)
                  }
                }}
              />
            }
            bordered
            dataSource={pointsOfSale ?? []}
            renderItem={(item) => <List.Item>{item.name}</List.Item>}
          />
        </TabPane>
      </Tabs>
    </>
  )
}

export default DefaultTripInfo
