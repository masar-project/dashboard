import React, { useContext, useState } from 'react'
import { Avatar, Button, List, Switch, Typography } from 'antd'
import cn from 'classnames'
import { IPOS } from '@/models/pos/response'
import DefaultTripContext from '@/contexts/default-trip/context'
import { useTranslation } from 'react-i18next'
import { EditOutlined, PlusOutlined, DeleteOutlined } from '@ant-design/icons'

import styles from './style.module.scss'
import CustomizedPOSModal from '@/components/customized-pos/customized-pos-modal'
import IMapFilter from '../../../models/base/filter-items/index'
import { FILTER_DATE_FORMATE } from '@/utils/constants'
import { CustomizedPOSType } from '@/models/customized-pos/enum'
import CustomizedPOSContext from '@/contexts/customized-pos/context'
import { IDefaultTrip } from '../../../models/default-trip/response'

const { Text } = Typography

interface IProps {
  setSelectedPOS: (pos: IPOS) => void
  selectedPOS: IPOS
  filters: IMapFilter
  defaultTrip: IDefaultTrip
}
const PreviewTrip: React.FC<IProps> = ({
  setSelectedPOS,
  selectedPOS,
  defaultTrip,
  filters,
}) => {
  // Preview
  const {
    previewTrip,
    previewTripLoading,
    actions: defaultTripActions,
  } = useContext(DefaultTripContext)
  const { t } = useTranslation()

  // Customized (Add POS)
  const [createVisible, setCreateVisible] = useState(false)

  const {
    actions: customizedPOSActions,
    actionLoading: customizedPOSActionLoading,
  } = useContext(CustomizedPOSContext)

  const isActive: (
    defaultTrip: IDefaultTrip,
    pointOfSaleId: number
  ) => boolean = (defaultTrip, pointOfSaleId) => {
    return (
      previewTrip?.pointsOfSale?.map((p) => p.id)?.includes(pointOfSaleId) ||
      defaultTrip?.pointsOfSale?.map((p) => p?.id)?.includes(pointOfSaleId)
    )
  }
  return (
    <>
      {previewTrip && (
        <>
          <List
            header={
              <div className={styles.listHeader}>
                <Text strong>{t('poss')}</Text>
                <Button
                  className={styles.addBtn}
                  type='text'
                  onClick={() => {
                    setCreateVisible(true)
                  }}
                >
                  <PlusOutlined />
                  {t('add')}
                </Button>
              </div>
            }
            className='possList'
            dataSource={previewTrip?.pointsOfSale ?? []}
            loading={previewTripLoading}
            renderItem={(pointOfSale) => (
              <List.Item
                key={pointOfSale?.id}
                className={cn(styles.pos, {
                  [styles.active]: selectedPOS?.id == pointOfSale?.id,
                })}
                onClick={() => {
                  setSelectedPOS(pointOfSale)
                }}
                actions={[
                  <Switch
                    defaultChecked={isActive(defaultTrip, pointOfSale?.id)}
                    loading={customizedPOSActionLoading}
                    size="small"
                    onChange={async () => {
                      let res: Boolean
                      res = isActive(defaultTrip, pointOfSale?.id)
                        ? await customizedPOSActions?.createCustomizedPOS({
                            date: filters?.date?.format(FILTER_DATE_FORMATE),
                            default_trip_id: defaultTrip?.id,
                            point_of_sale_id: pointOfSale?.id,
                            type: CustomizedPOSType.Deletion,
                          })
                        : await customizedPOSActions?.deleteCustomizedPOS(
                            pointOfSale?.id
                          )

                      res &&
                        defaultTripActions?.previewTrip(defaultTrip?.id, {
                          date: filters?.date?.format(FILTER_DATE_FORMATE),
                        })
                    }}
                  />,
                  // <Button
                  //   type='text'
                  //   danger
                  //   onClick={async () => {
                  //     console.log('Delete: ', pointOfSale)
                  //     let res = await customizedPOSActions?.createCustomizedPOS(
                  //       {
                  //         date: filters?.date?.format(FILTER_DATE_FORMATE),
                  //         default_trip_id: defaultTripId,
                  //         point_of_sale_id: pointOfSale?.id,
                  //         type: CustomizedPOSType.Deletion,
                  //       }
                  //     )
                  //   }}
                  // >
                  //   <DeleteOutlined />
                  // </Button>,
                ]}
              >
                <List.Item.Meta
                  style={{ padding: '0 1rem' }}
                  avatar={
                    <Avatar
                      src={`/images/pin/pin-${
                        selectedPOS?.id == pointOfSale?.id
                          ? 'primary'
                          : 'disable'
                      }.png`}
                      style={{ height: '20px', width: '20px' }}
                    />
                  }
                  description={
                    <>
                      <Text>{pointOfSale?.name}</Text>
                    </>
                  }
                />
              </List.Item>
            )}
            split={false}
          />
          <Drivers />

          <CustomizedPOSModal
            visible={createVisible}
            setVisible={setCreateVisible}
            date={filters?.date?.format(FILTER_DATE_FORMATE)}
            defaultTripId={defaultTrip?.id}
          />
        </>
      )}
    </>
  )
}

export default PreviewTrip

const Drivers: React.FC = () => {
  const { t } = useTranslation()
  const { previewTrip, previewTripLoading } = useContext(DefaultTripContext)

  return (
    <div className='driversList'>
      <List split={false} loading={previewTripLoading}>
        <>
          <List.Item key='default-driver' className={styles.driver}>
            <List.Item.Meta
              style={{ padding: '0 1rem' }}
              title={`${t('default_driver')}:`}
              description={`${previewTrip?.driver?.user?.firstName} ${previewTrip?.driver?.user?.lastName}`}
            />
          </List.Item>
          <List.Item
            key='current_driver'
            className={styles.driver}
            actions={[
              <Button type='text'>
                <EditOutlined />
              </Button>,
            ]}
          >
            <List.Item.Meta
              style={{ padding: '0 1rem' }}
              title={`${t('current_driver')}:`}
              description={`${previewTrip?.driver?.user?.firstName} ${previewTrip?.driver?.user?.lastName}`}
            />
          </List.Item>
        </>
      </List>
    </div>
  )
}
