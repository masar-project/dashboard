import React, { useContext, useState } from 'react'
import { Avatar, Button, Card, Divider, Row } from 'antd'
import { PoweroffOutlined, LockOutlined, UserOutlined } from '@ant-design/icons'
import AuthContext from '../../../contexts/auth/context'
import { useTranslation } from 'react-i18next'
import ChangePasswordModal from '../change-password-modal'

import styles from './style.module.scss'

interface IProfileProps {
  toogleUserPopoverVisible: (visible: boolean) => void
}

const ProfileCard: React.FC<IProfileProps> = ({ toogleUserPopoverVisible }) => {
  const { t } = useTranslation()
  const {
    actions,
    logoutLoading,
    employeeDetails: employeeDetails,
  } = useContext(AuthContext)

  return (
    <>
      <Card
        bordered={false}
        bodyStyle={{ padding: 0 }}
        title={
          <div className={styles.textCenter}>
            {/* <Avatar size={64} src={employeeDetails?.user?.profileImg?.url} /> */}
            <UserOutlined />

            <Row className={styles.contentContainer} align='middle'>
              <div style={{ width: '100%' }}>
                <h5>
                  <b>{`${employeeDetails?.user?.firstName}`}</b>
                </h5>
                <small>{`${employeeDetails?.user?.email}`}</small>
              </div>
            </Row>
          </div>
        }
      >
        <span>
          <Button
            className={styles.button}
            type='link'
            onClick={() => {
              actions.logout()
            }}
            size='small'
            icon={<PoweroffOutlined />}
            loading={logoutLoading}
          >
            {t('signout')}
          </Button>
        </span>
      </Card>
    </>
  )
}

export default ProfileCard
