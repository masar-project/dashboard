import React, { useContext } from 'react'
import FormModal from '../../general/form-modal'
import ChangePasswordForm from '../change-password-form'
import { useTranslation } from 'react-i18next'
import AuthContext from '../../../contexts/auth/context'

interface IProps {
  visible: boolean
  toggleVisible: (visible: boolean) => void
}

const ChangePasswordModal: React.FC<IProps> = ({ visible, toggleVisible }) => {
  const { t } = useTranslation()
  const { actions, changePasswordLoading } = useContext(AuthContext)
  return (
    <FormModal
      id='change-password'
      key='change-password'
      title={t('change_password')}
      confirmButtonText={t('change_password_and_signout')}
      visible={visible}
      toggleVisible={toggleVisible}
      loading={changePasswordLoading}
      width='40vw'
      onSubmit={async (data) => {
        const result = await actions.changePassword(data)
        return result
      }}
    >
      <ChangePasswordForm />
    </FormModal>
  )
}

export default ChangePasswordModal
