import React from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import { Input } from 'antd'
import { ErrorMessage } from '@hookform/error-message'
import FormItem from '../../general/form-item'

import styles from './style.module.scss'

const ChangePasswordForm: React.FC = () => {
  const { control, errors, getValues } = useFormContext()
  const { t } = useTranslation()

  return (
    <>
      {/* Old Password */}
      <FormItem label={t('old_password')}>
        <Controller
          as={Input}
          name='oldPassword'
          control={control}
          type='password'
          rules={{ required: `${t('field_is_required_message')}` }}
        />

        <ErrorMessage
          errors={errors}
          name='oldPassword'
          render={({ message }) => <p className={styles.alert}>{message}</p>}
        />
      </FormItem>

      {/* New Password */}
      <FormItem label={t('new_password')}>
        <Controller
          as={Input}
          name='password'
          control={control}
          type='password'
          rules={{
            required: `${t('field_is_required_message')}`,
            minLength: {
              value: 8,
              message: t('invalid_password'),
            },
            maxLength: {
              value: 32,
              message: t('invalid_password'),
            },
          }}
        />

        <ErrorMessage
          errors={errors}
          name='password'
          render={({ message }) => <p className={styles.alert}>{message}</p>}
        />
      </FormItem>

      {/* Confirm New Password */}
      <FormItem label={t('confirm_new_password')}>
        <Controller
          as={Input}
          name='confirmPassword'
          control={control}
          type='password'
          rules={{
            required: `${t('field_is_required_message')}`,
            minLength: {
              value: 8,
              message: t('invalid_password'),
            },
            maxLength: {
              value: 32,
              message: t('invalid_password'),
            },
            validate: (value) => {
              return (
                value === getValues('password') ||
                `${t('passwords_mismatched')}`
              )
            },
          }}
        />

        <ErrorMessage
          errors={errors}
          name='confirmPassword'
          render={({ message }) => <p className={styles.alert}>{message}</p>}
        />
      </FormItem>
    </>
  )
}

export default ChangePasswordForm
