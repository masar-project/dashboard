import React, { useContext, useEffect, useState } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Col, Row, Select } from 'antd'
import AppContext from '@/contexts/app/context'
import { IPOS } from '@/models/pos/response'
import POSContext from '@/contexts/pos/context'
import styles from './style.module.scss'
import FormItem from '@/components/general/form-item'
import { ErrorMessage } from '@hookform/error-message'
import LoadingPage from '@/components/general/loading-page'
import dynamic from 'next/dynamic'
import { PointStatus } from '@/models/pos/enum'
import { DEFAULT_MAXIMUM_PAGE_SIZE } from '@/contexts/constants'
import CustomizedPOSContextProvider from '@/contexts/customized-pos/provider'
const OurMap = dynamic(() => import('@/components/mapbox/map/index'), {
  ssr: false,
  loading: () => <LoadingPage height={100} />,
})

interface IProps {}

const CustomizedPOSForm: React.FC<IProps> = () => {
  const { control, errors, setValue } = useFormContext()
  const { t } = useTranslation()

  const [points, setPoints] = useState<IPOS[]>([])
  const {
    data: poss,
    loading: posLoading,
    actions: posActions,
  } = useContext(POSContext)

  useEffect(() => {
    posActions.setPageSize(DEFAULT_MAXIMUM_PAGE_SIZE)
  }, [])

  useEffect(() => {
    poss && setPoints(poss?.data)
  }, [poss])

  const [selectedPoint, setSelectedPoint] = useState<number>()

  const addPoint = (id: number) => {
    setSelectedPoint(id)
  }

  useEffect(() => {
    selectedPoint && setValue('point_of_sale_id', selectedPoint)
  }, [selectedPoint])

  return (
    <>
      <Row gutter={12} style={{ height: '50vh' }}>
        <Col span={8}>
          <Row>
            <Col span={24}>
              <FormItem label={t('selected_poss')}>
                <Controller
                  rules={{ required: `${t('field_is_required_message')}` }}
                  render={({ onChange, onBlur, value }) => {
                    return (
                      <Select
                        allowClear
                        style={{ width: '100%' }}
                        placeholder={t('please_select')}
                        onChange={(value) => {
                          setSelectedPoint(value)
                          onChange(value)
                        }}
                        onBlur={onBlur}
                        value={value}
                        optionLabelProp='label'
                        options={
                          points?.map((point) => {
                            return { value: point.id, label: point.name }
                          }) ?? []
                        }
                      />
                    )
                  }}
                  name='point_of_sale_id'
                  control={control}
                />

                <ErrorMessage
                  errors={errors}
                  name='point_of_sale_id'
                  render={({ message }) => (
                    <p className={styles.alert}>{message}</p>
                  )}
                />
              </FormItem>
            </Col>
          </Row>
        </Col>
        {/* Map */}
        <Col span={16}>
          <OurMap
            coordinates={points?.map((point, _) => {
              return {
                id: point?.id,
                long: point?.long,
                lat: point?.lat,
                status:
                  selectedPoint == point?.id
                    ? PointStatus.Primary
                    : PointStatus.Disable,
                onClick: () => {
                  addPoint(point?.id)
                },
              }
            })}
          />
        </Col>
      </Row>
    </>
  )
}

export default CustomizedPOSForm
