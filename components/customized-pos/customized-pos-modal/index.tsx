import CreateFormModal from '@/components/general/create-form-modal'
import React, { useContext } from 'react'
import CustomizedPOSForm from '../customized-pos-form/index'
import { useTranslation } from 'react-i18next'
import CustomizedPOSContextProvider from '@/contexts/customized-pos/provider'
import POSContextProvider from '@/contexts/pos/provider'
import CustomizedPOSContext from '@/contexts/customized-pos/context'
import { CustomizedPOSType } from '@/models/customized-pos/enum'
import DefaultTripContext from '@/contexts/default-trip/context'
import SocketContext from '@/contexts/socket/context'

interface IProps {
  visible: boolean
  setVisible: (visible: boolean) => void
  date: string
  defaultTripId: number
}

const Modal: React.FC<IProps> = ({
  visible,
  setVisible,
  date,
  defaultTripId,
}) => {
  const { t } = useTranslation()
  const { actions, actionLoading } = useContext(CustomizedPOSContext)

  // Preview
  const { actions: previewActions } = useContext(DefaultTripContext)

  // Socket
  const { actions: socketActions } = useContext(SocketContext)

  return (
    <div>
      <CreateFormModal
        key='create'
        title={t('add_pos')}
        visible={visible}
        toggleVisible={setVisible}
        submitLoading={actionLoading}
        width='70vw'
        onSubmit={async (data) => {
          console.log('Submit CustomizedPOS: ', data)
          let res = await actions?.createCustomizedPOS({
            date: date,
            default_trip_id: defaultTripId,
            point_of_sale_id: data.point_of_sale_id,
            type: CustomizedPOSType.Addition,
          })
          if (res) {
            previewActions.previewTrip(defaultTripId, { date: date })
            socketActions.refreshTrip()
          }

          return res
        }}
      >
        <CustomizedPOSForm />
      </CreateFormModal>
    </div>
  )
}

const CustomizedPOSModal: React.FC<IProps> = (props) => {
  return (
    <POSContextProvider>
      <Modal {...props} />
    </POSContextProvider>
  )
}

export default CustomizedPOSModal
