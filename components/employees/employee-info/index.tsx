import React from 'react'
import { Descriptions } from 'antd'
import { useTranslation } from 'react-i18next'
import { IEmployeeDetails } from '@/models/employee/response'

import styles from './style.module.scss'
import Img from '@/components/general/img'
import { UserGender } from '@/models/user/enums'

interface IEmployeeInfoProps {
  employee: IEmployeeDetails
}

const EmployeeInfo: React.FC<IEmployeeInfoProps> = ({ employee }) => {
  const { t } = useTranslation()

  return (
    <>
      <Descriptions
        bordered
        column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
        size='small'
      >
        {/* id */}
        <Descriptions.Item label={t('id')} span={3}>
          {`#${employee?.id}`}
        </Descriptions.Item>

        {/* role */}
        <Descriptions.Item label={t('role')}>
          {employee?.role}
        </Descriptions.Item>

        {/* email */}
        <Descriptions.Item label={t('email')}>
          {employee?.user?.email}
        </Descriptions.Item>

        {/* first_name */}
        <Descriptions.Item label={t('first_name')}>
          {employee?.user?.firstName}
        </Descriptions.Item>

        {/* last_name */}
        <Descriptions.Item label={t('last_name')}>
          {employee?.user?.lastName}
        </Descriptions.Item>

        {/* gender */}
        <Descriptions.Item label={t('gender')}>
          {UserGender[employee?.user?.gender]}
        </Descriptions.Item>

        {/* birthdate */}
        <Descriptions.Item label={t('birth_date')}>
          {employee?.user?.birthDate}
        </Descriptions.Item>

        {/* mobileNumber */}
        <Descriptions.Item label={t('mobile_number')}>
          {employee?.user?.mobileNumber}
        </Descriptions.Item>

        {/* image */}
        <Descriptions.Item label={t('image')} span={3}>
          <Img
            className={styles.imageIcon}
            src={employee?.user?.profileImg?.url}
          />
        </Descriptions.Item>

        {/* created_at */}
        <Descriptions.Item label={t('created_at')}>
          {employee?.createdAt}
        </Descriptions.Item>
      </Descriptions>
    </>
  )
}

export default EmployeeInfo
