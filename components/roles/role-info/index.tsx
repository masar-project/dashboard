import { Avatar, Button, Descriptions, Input, List, Skeleton, Tabs } from 'antd'
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { SearchOutlined, EditOutlined } from '@ant-design/icons'
import { IRoleDetails } from '../../../models/role/response'

const { TabPane } = Tabs

interface IRoleInfoProps {
  role?: IRoleDetails
}

const RoleInfo: React.FC<IRoleInfoProps> = ({ role }) => {
  const {
    t,
    i18n: { language },
  } = useTranslation()

  const [permissions, setPermissions] = useState(role?.permissions)

  useEffect(() => {
    role?.permissions && setPermissions(role?.permissions)
  }, [role?.permissions])

  return (
    <>
      <Descriptions
        bordered
        column={{ xxl: 3, xl: 2, lg: 2, md: 1, sm: 1, xs: 1 }}
        size='small'
      >
        <Descriptions.Item label={t('name')}>{role?.name}</Descriptions.Item>
      </Descriptions>

      <Tabs defaultActiveKey='1' animated>
        <TabPane tab={t('permissions')} key='1'>
          <List
            header={
              <Input
                placeholder={t('search_hint')}
                suffix={<SearchOutlined />}
                allowClear
                onChange={(event) => {
                  let searchKey = event.target.value

                  let filteredPermissions = role?.permissions?.filter(
                    (permission) => permission?.name?.includes(searchKey)
                  )

                  setPermissions(filteredPermissions)

                  if (searchKey == '') {
                    setPermissions(role?.permissions)
                  }
                }}
              />
            }
            bordered
            dataSource={permissions ?? []}
            renderItem={(item) => <List.Item>{item.name}</List.Item>}
          />
        </TabPane>
      </Tabs>
    </>
  )
}

export default RoleInfo
