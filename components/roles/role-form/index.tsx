import React, { useContext, useEffect } from 'react'
import { Input as AntdInput, Select } from 'antd'

import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { ErrorMessage } from '@hookform/error-message'
import styles from './style.module.scss'
import FormItem from '../../general/form-item'
import PermissionSelectOptionsContext from '../../../contexts/select-options/permission/context'
import PermissionSelectOptionsContextProvider from '../../../contexts/select-options/permission/provider'

export interface IRoleForm {
  name?: string
  permissions?: string[]
}

const Form: React.FC = (props) => {
  const { control, errors } = useFormContext()

  const { data: permissionData, actions: permissionActions } = useContext(
    PermissionSelectOptionsContext
  )

  const { t } = useTranslation()

  return (
    <>
      {/* Name */}
      <FormItem label={t('name')}>
        <Controller
          as={AntdInput}
          name='name'
          control={control}
          rules={{ required: `${t('field_is_required_message')}` }}
        />

        <ErrorMessage
          errors={errors}
          name='name'
          render={({ message }) => <p className={styles.alert}>{message}</p>}
        />
      </FormItem>

      {/* Permissions */}
      <FormItem label={t('permissions')}>
        <Controller
          rules={{
            required: `${t('field_is_required_message')}`,
          }}
          render={({ onChange, onBlur, value }) => {
            return (
              <Select
                mode='multiple'
                allowClear
                autoClearSearchValue={false}
                style={{ width: '100%' }}
                placeholder={t('please_select')}
                onChange={onChange}
                onBlur={onBlur}
                value={value}
                optionLabelProp='label'
                options={
                  permissionData?.data
                    .filter((permission) => !permission.name.includes('role'))
                    .filter(
                      (permission) => !permission.name.includes('permission')
                    )
                    .map((permission) => {
                      return { value: permission.name, label: permission.name }
                    }) ?? []
                }
              ></Select>
            )
          }}
          name='permissions'
          control={control}
        ></Controller>
        <ErrorMessage
          errors={errors}
          name='permissions'
          render={({ message }) => <p className={styles.alert}>{message}</p>}
        />
      </FormItem>
    </>
  )
}
const RoleForm: React.FC = (props) => {
  return (
    <PermissionSelectOptionsContextProvider>
      <Form />
    </PermissionSelectOptionsContextProvider>
  )
}
export default RoleForm
