import React from 'react'
import Img from '../../general/img'
import { useTranslation } from 'react-i18next'
import styles from './style.module.scss'
import { PlusCircleOutlined } from '@ant-design/icons'
import { Button, Tooltip } from 'antd'

interface IImageContainerProps {
  title?: string
  imageUrl?: string
  onClick: () => void
}
const AddImage: React.FC<IImageContainerProps> = ({
  title,
  imageUrl,
  onClick,
}) => {
  const { t } = useTranslation()
  return (
    <div onClick={onClick} className={styles.container}>
      <Img
        style={{ height: '12.3rem' }}
        preview={false}
        src={imageUrl ?? '/images/upload-image.png'}
      />

      <Tooltip title={t('add_new_image')} mouseEnterDelay={0.5}>
        <Button
          className={styles.button}
          icon={<PlusCircleOutlined />}
          type='ghost'
          onClick={onClick}
        >
          {title ?? t('add_new_image')}
        </Button>
      </Tooltip>
    </div>
  )
}

export default AddImage
