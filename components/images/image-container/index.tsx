import React from 'react'
import Img from '../../general/img'
import styles from './style.module.scss'
import { useTranslation } from 'react-i18next'
import { Button, Divider, Popconfirm, Tooltip } from 'antd'
import { InfoOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'

interface IImageContainerProps {
  imageId: number
  title: string
  imageUrl: string
  info: {
    setInfoId: (id: number) => void
    setInfoVisible: (value: boolean) => void
  }
  edit: {
    setEditId: (id: number) => void
    setEditVisible: (value: boolean) => void
  }
  onDeleteImage: (imageId: number) => void
}
const ImageContainer: React.FC<IImageContainerProps> = ({
  imageId,
  title,
  imageUrl,
  info,
  edit,
  onDeleteImage,
}) => {
  const { t } = useTranslation()

  return (
    <div className={styles.container}>
      <Img style={{ height: '13rem' }} preview={false} src={imageUrl} />
      <div>
        <Tooltip title={title} placement='topRight'>
          <span className={styles.title}>{title}</span>
        </Tooltip>

        <span className={styles.action}>
          <Tooltip title={t('show_info')} mouseEnterDelay={0.5}>
            <Button
              className={styles.button}
              size='small'
              type='link'
              icon={<InfoOutlined />}
              onClick={() => {
                info?.setInfoId(imageId)
                info?.setInfoVisible(true)
              }}
            />
          </Tooltip>
          <Divider type='vertical' style={{ height: 'auto' }} />
          <Tooltip title={t('edit')} mouseEnterDelay={0.5}>
            <Button
              className={styles.button}
              size='small'
              type='link'
              icon={<EditOutlined />}
              onClick={() => {
                edit?.setEditId(imageId)
                edit?.setEditVisible(true)
              }}
            />
          </Tooltip>
          <Divider type='vertical' style={{ height: 'auto' }} />
          <Tooltip title={t('delete')} mouseEnterDelay={0.5}>
            <Popconfirm
              title={t('delete_confirm')}
              onConfirm={() => {
                onDeleteImage(imageId)
              }}
              onCancel={() => {}}
              okButtonProps={{ danger: true }}
              placement='bottomRight'
            >
              <Button
                className={styles.button}
                danger
                size='small'
                type='link'
                icon={<DeleteOutlined />}
              />
            </Popconfirm>
          </Tooltip>
        </span>
      </div>
    </div>
  )
}

export default ImageContainer
