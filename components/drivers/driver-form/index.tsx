import React, { useContext } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Input, Select, Upload, DatePicker, Row, Col } from 'antd'
import { ErrorMessage } from '@hookform/error-message'
import FormItem from '@/components/general/form-item'
import styles from './style.module.scss'
import { IDriverDetails } from '@/models/driver/response'
import AppContext from '@/contexts/app/context'
import { UserGender } from '@/models/user/enums'

const { Dragger } = Upload

interface IDriverFormProps {
	driver?: IDriverDetails
}

const DriverForm: React.FC<IDriverFormProps> = ({ driver }) => {
	const { control, errors } = useFormContext()
	const { t } = useTranslation()
	const { loading, languages } = useContext(AppContext)

	return (
		<>
			<Row gutter={12}>
				{/* first_name */}
				<Col span={12}>
					<FormItem label={t('first_name')}>
						<Controller as={Input} name='first_name' control={control} rules={{ required: `${t('field_is_required_message')}` }} />

						<ErrorMessage errors={errors} name='first_name' render={({ message }) => <p className={styles.alert}>{message}</p>} />
					</FormItem>
				</Col>
				{/* last_name */}
				<Col span={12}>
					<FormItem label={t('last_name')}>
						<Controller as={Input} name='last_name' control={control} rules={{ required: `${t('field_is_required_message')}` }} />

						<ErrorMessage errors={errors} name='first_name' render={({ message }) => <p className={styles.alert}>{message}</p>} />
					</FormItem>
				</Col>
				{/* Gender */}
				<Col span={12}>
					<FormItem label={t('gender')}>
						<Controller
							rules={{ required: `${t('field_is_required_message')}` }}
							render={({ onChange, onBlur, value }) => {
								return (
									<Select
										style={{ width: '100%' }}
										placeholder={t('please_select')}
										onChange={onChange}
										onBlur={onBlur}
										value={value}
										optionLabelProp='label'
										options={[
											{
												value: UserGender.male,
												label: t(`${UserGender[UserGender.male]}`)
											},
											{
												value: UserGender.female,
												label: t(`${UserGender[UserGender.female]}`)
											}
										]}
									/>
								)
							}}
							name='gender'
							control={control}
						/>

						<ErrorMessage errors={errors} name='gender' render={({ message }) => <p className={styles.alert}>{message}</p>} />
					</FormItem>
				</Col>
				<Col span={12}>
					{' '}
					{/* birth_date */}
					<FormItem label={t('birth_date')}>
						<Controller as={Input} type='date' name='birth_date' control={control} rules={{ required: `${t('field_is_required_message')}` }} />

						<ErrorMessage errors={errors} name='birth_date' render={({ message }) => <p className={styles.alert}>{message}</p>} />
					</FormItem>
				</Col>
				<Col span={24}>
					{' '}
					{/* mobile_number */}
					<FormItem label={t('mobile_number')}>
						<Controller as={Input} name='mobile_number' control={control} rules={{ required: `${t('field_is_required_message')}` }} />

						<ErrorMessage errors={errors} name='mobile_number' render={({ message }) => <p className={styles.alert}>{message}</p>} />
					</FormItem>
				</Col>
				{!driver && <Col span={24}>
					{/* email */}
					<FormItem label={t('email')}>
						<Controller as={Input} name='email' type='email' control={control} rules={{ required: `${t('field_is_required_message')}` }} />

						<ErrorMessage errors={errors} name='email' render={({ message }) => <p className={styles.alert}>{message}</p>} />
					</FormItem>
				</Col>}
				{!driver && <Col span={12}>
					{/* password */}
					<FormItem label={t('password')}>
						<Controller as={Input} name='password' type='password' control={control} rules={{ required: `${t('field_is_required_message')}` }} />

						<ErrorMessage errors={errors} name='password' render={({ message }) => <p className={styles.alert}>{message}</p>} />
					</FormItem>
				</Col>}
				{!driver && <Col span={12}>
					{/* password_confirmation */}
					<FormItem label={t('password_confirmation')}>
						<Controller as={Input} name='password_confirmation' type='password' control={control} rules={{ required: `${t('field_is_required_message')}` }} />

						<ErrorMessage errors={errors} name='password_confirmation' render={({ message }) => <p className={styles.alert}>{message}</p>} />
					</FormItem>
				</Col>} 
			</Row>
		</>
	)
}

export default DriverForm
