import React from 'react'
import { Descriptions } from 'antd'
import { useTranslation } from 'react-i18next'
import { IDriverDetails } from '@/models/driver/response'

import styles from './style.module.scss'
import Img from '@/components/general/img'
import { UserGender } from '@/models/user/enums'

interface IDriverInfoProps {
  driver: IDriverDetails
}

const DriverInfo: React.FC<IDriverInfoProps> = ({ driver }) => {
  const { t } = useTranslation()

  return (
    <>
      <Descriptions
        bordered
        column={{ xxl: 1, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
        size='small'
      >
        {/* id */}
        <Descriptions.Item label={t('id')} span={3}>
          {`#${driver?.id}`}
        </Descriptions.Item>

        {/* role */}
        <Descriptions.Item label={t('role')}>
          {driver?.role}
        </Descriptions.Item>

        {/* email */}
        <Descriptions.Item label={t('email')}>
          {driver?.user?.email}
        </Descriptions.Item>

        {/* first_name */}
        <Descriptions.Item label={t('first_name')}>
          {driver?.user?.firstName}
        </Descriptions.Item>

        {/* last_name */}
        <Descriptions.Item label={t('last_name')}>
          {driver?.user?.lastName}
        </Descriptions.Item>

        {/* gender */}
        <Descriptions.Item label={t('gender')}>
          {UserGender[driver?.user?.gender]}
        </Descriptions.Item>

        {/* birthdate */}
        <Descriptions.Item label={t('birth_date')}>
          {driver?.user?.birthDate}
        </Descriptions.Item>

        {/* mobileNumber */}
        <Descriptions.Item label={t('mobile_number')}>
          {driver?.user?.mobileNumber}
        </Descriptions.Item>

        {/* image */}
        <Descriptions.Item label={t('image')} span={3}>
          <Img
            className={styles.imageIcon}
            src={driver?.user?.profileImg?.url}
          />
        </Descriptions.Item>

        {/* created_at */}
        <Descriptions.Item label={t('created_at')}>
          {driver?.createdAt}
        </Descriptions.Item>
      </Descriptions>
    </>
  )
}

export default DriverInfo
