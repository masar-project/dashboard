import React from 'react'
import { Layer, Source } from 'react-mapbox-gl'

const MapRoute = ({ coordinates, lineColor, lineWidth }) => {
  const options = {
    type: 'geojson',
    data: {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'LineString',
        coordinates,
      },
    },
  }

  return (
    <>
      <Source id='route' tileJsonSource={options} />
      <Layer
        type='line'
        id='route'
        sourceId='route'
        layout={{ 'line-join': 'round', 'line-cap': 'round' }}
        paint={{
          'line-color': lineColor ?? 'dodgerblue',
          'line-width': lineWidth ?? 8,
        }}
      ></Layer>
    </>
  )
}

export default MapRoute
