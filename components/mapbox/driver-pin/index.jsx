import React from 'react'
import { Marker } from 'react-mapbox-gl'

const DriverPin = ({ coordinates, onClick }) => {
  return (
    <Marker onClick={onClick} coordinates={coordinates} anchor='bottom'>
      <img src={'/images/track.png'} alt='track' height={48} />
    </Marker>
  )
}

export default DriverPin
