import React, { useContext, useEffect, useState } from 'react'
import ReactMapboxGl, { Popup, MapContext } from 'react-mapbox-gl'
import {
  GeolocateControl,
  NavigationControl,
  FullscreenControl,
  setRTLTextPlugin,
} from 'mapbox-gl'
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder'
import * as MapboxDirection from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions'
import MapPin from '../map-pin'
import DirectionStyle from './direction-style'
import { PointStatus } from '@/models/pos/enum'
import { useTranslation } from 'react-i18next'
import SocketContext from '@/contexts/socket/context'
import DriverPin from '../driver-pin'
import { orderBy } from 'lodash'

const accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN
setRTLTextPlugin(
  'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.3/mapbox-gl-rtl-text.js',
  null,
  true
)
const Map = ReactMapboxGl({
  accessToken,
})

export interface ICoordinate {
  id: number
  long: number
  lat: number
  label?: string
  onClick?: () => void
  status?: PointStatus
  defaultOrder?: number
}
interface IProps {
  hideNavigation?: boolean
  hideGeolocate?: boolean
  hideFullscreen?: boolean
  coordinates?: ICoordinate[]
}

const App: React.FC<IProps> = (props) => {
  const { t } = useTranslation()
  const { driverLocation } = useContext(SocketContext)

  // Zoom
  const [zoom, setZoom] = useState<[number]>([12])
  // mapCenetr
  const [mapCenter, setMapCenter] = useState<[number, number]>([
    36.2765, 33.5138,
  ])

  const mapLoaded = (map: any) => {
    map.resize()

    const geocoder = new MapboxGeocoder({
      accessToken,
      mapboxgl: map,
    })
    map.addControl(geocoder)

    // direction.setOrigin()
    // map.addControl(geocoder)

    const geoLocate = new GeolocateControl()
    const navigation = new NavigationControl()
    const fullScreem = new FullscreenControl()

    // console.log('map: ', !map.hasControl(geoLocate), map.hasControl(geoLocate))

    // check if "hideGeolocate" prop os not passed and map not have "geoLocate" control yest
    !props.hideGeolocate && map.addControl(geoLocate)

    // check if "hideNavigation" prop os not passed and map not have "navigation" control yest
    !props?.hideNavigation && map.addControl(navigation)

    // check if "hideFullscreen" prop os not passed and map not have "fullScreem" control yest
    !props?.hideFullscreen && map.addControl(fullScreem)
  }

  return (
    <Map
      style='mapbox://styles/kareemdabbeet/ckqja6weq1f6k18mokxw2x8km'
      containerStyle={{
        height: '100%',
      }}
      center={mapCenter}
      zoom={zoom}
      onZoomEnd={(map) => {
        setZoom([map?.getZoom()])
      }}
      onMoveEnd={(map) => {
        setMapCenter([map.getCenter().lng, map.getCenter().lat])
      }}
      onStyleLoad={(map) => {
        return mapLoaded(map)
      }}
      onRender={(map) => {}}
      onClick={(event) => {
        // console.log(
        //   `Location: lat:${event.getCenter().lat}, lng:${event.getCenter().lng}`
        // )
      }}
    >
      {/* Add Markers */}
      <>
        {props?.coordinates?.map((coordinate, _) => {
          return (
            <>
              <MapPin
                onClick={coordinate?.onClick}
                key={coordinate?.id}
                coordinates={[coordinate?.long, coordinate?.lat]}
                status={coordinate?.status}
                defaultOrder={coordinate?.defaultOrder}
              />
              {coordinate?.label && (
                <Popup
                  coordinates={[coordinate?.long, coordinate?.lat]}
                  offset={[0, -30]}
                >
                  <h3>{coordinate?.label}</h3>
                </Popup>
              )}
            </>
          )
        })}
        {driverLocation && (
          <>
            <DriverPin
              coordinates={[driverLocation?.lng, driverLocation?.lat]}
              onClick={() => {}}
            />
            {/* <Popup
              coordinates={[driverLocation?.lng, driverLocation?.lat]}
              offset={[0, -30]}
            >
              <h3>{t('driver')}</h3>
            </Popup> */}
          </>
        )}
      </>

      {/* <MapContext.Consumer>
        {(map) => {
          const direction = new MapboxDirection({
            accessToken,
            styles: DirectionStyle,
            interactive: false,
            controls: {
              inputs: false,
              instructions: false,
              profileSwitcher: false,
            },
          })
          props.coordinates?.forEach((coordinate, index) => {
            if (index == 0) {
              direction.setOrigin([coordinate.long, coordinate.lat])
            } else if (index == props?.coordinates?.length - 1) {
              direction.setDestination([coordinate.long, coordinate.lat])
            } else {
              direction.addWaypoint(index, [coordinate.long, coordinate.lat])
            }
          })
          map.addControl(direction)
          return <></>
        }}
      </MapContext.Consumer> */}
    </Map>
  )
}

export default App
