import React from 'react'
import { PointStatus } from '@/models/pos/enum'
import { Marker } from 'react-mapbox-gl'
import { Badge } from 'antd'

interface IProps {
  coordinates: [number, number]
  onClick: () => void
  status: PointStatus
  defaultOrder?: number
}

const MapPin: React.FC<IProps> = ({
  coordinates,
  onClick,
  status,
  defaultOrder,
}) => {
  return (
    <Marker onClick={onClick} coordinates={coordinates} anchor='bottom'>
      {defaultOrder ? (
        <Badge
          count={defaultOrder}
          size='small'
          style={{
            color: '#fff',
            backgroundColor: '#15191e',
          }}
        >
          <img src={getPng(status)} alt='pin' height={32} />
        </Badge>
      ) : (
        <img src={getPng(status)} alt='pin' height={32} />
      )}
    </Marker>
  )
}

export default MapPin

export const getPng = (status) => {
  let src = '/images/pin/pin-primary.png'
  switch (status) {
    case PointStatus.Primary:
      src = '/images/pin/pin-primary.png'
      break
    case PointStatus.Success:
      src = '/images/pin/pin-success.png'
      break
    case PointStatus.Failure:
      src = '/images/pin/pin-error.png'
      break
    case PointStatus.Disable:
      src = '/images/pin/pin-disable.png'
      break
    case PointStatus.InTransit:
      src = '/images/pin/pin-info.png'
      break
    default:
      src = '/images/pin/pin-primary.png'
      break
  }
  return src
}
