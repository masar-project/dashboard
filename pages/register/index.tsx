import React, { useContext, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Typography, Form, Input, Button, Row, Col, Divider } from 'antd'
import classes from './style.module.scss'
import AuthContext from '../../contexts/auth/context'
import {
  MailOutlined,
  LockOutlined,
  UserOutlined,
  PhoneOutlined,
} from '@ant-design/icons'
import Head from 'next/head'
import AppContext from '../../contexts/app/context'
import { useRouter } from 'next/router'
import { ILoginRequest, IRegisterRequest } from '../../models/auth/request'
const { Title, Text } = Typography

import styles from './style.module.scss'

const RegisterPage: React.FC = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  const { actions, registerLoading, registerResponse, isAuthenticated } =
    useContext(AuthContext)

  useEffect(() => {
    if (isAuthenticated) {
      router.replace('/')
    }
  }, [isAuthenticated])

  const router = useRouter()

  useEffect(() => {
    if (registerResponse) {
      router.replace('/')
    }
  }, [registerResponse])

  const onSubmit = (data: IRegisterRequest) => {
    actions.register(data)
  }

  return (
    <div dir={direction} style={{}} className={styles.container}>
      <Head>
        <title>
          {t('masar')} - {t('register')}
        </title>
      </Head>

      <div className={styles.bg} />
      <div className={styles.card}>
        <div style={{ width: '110%' }}>
          <Title level={3} className={classes.title}>
            <Text className={classes.subtitle}>Apply as a company</Text>

            <img
              src='/images/text-logo.png'
              alt={'logo'}
              className={styles.textLogo}
            />
          </Title>

          <Divider />

          <Form
            hideRequiredMark
            layout='vertical'
            onFinish={onSubmit}
            className={classes.form}
            scrollToFirstError
            validateMessages={{
              required: t('field_is_required_message'),
            }}
          >
            <Row justify='space-around'>
              <Col span={11}>
                <Form.Item
                  name='firstName'
                  rules={[
                    {
                      type: 'string',
                    },
                    {
                      required: true,
                    },
                  ]}
                >
                  <Input
                    className={classes.input}
                    prefix={<UserOutlined className={classes.prefixIcon} />}
                    placeholder={t('first_name')}
                  />
                </Form.Item>
              </Col>
              <Col span={11}>
                <Form.Item
                  name='lastName'
                  rules={[
                    {
                      type: 'string',
                    },
                    {
                      required: true,
                    },
                  ]}
                >
                  <Input
                    className={classes.input}
                    prefix={<UserOutlined className={classes.prefixIcon} />}
                    placeholder={t('last_name')}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row justify='space-around'>
              <Col span={11}>
                <Form.Item
                  name='username'
                  rules={[
                    {
                      type: 'string',
                    },
                    {
                      required: true,
                    },
                  ]}
                >
                  <Input
                    className={classes.input}
                    prefix={<UserOutlined className={classes.prefixIcon} />}
                    placeholder={t('user_name')}
                  />
                </Form.Item>
              </Col>
              <Col span={11}>
                <Form.Item
                  name='phoneNumber'
                  rules={[
                    {
                      type: 'string',
                    },
                    {
                      required: true,
                    },
                  ]}
                >
                  <Input
                    className={classes.input}
                    prefix={<PhoneOutlined className={classes.prefixIcon} />}
                    placeholder={t('phone_number')}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row justify='space-around'>
              <Col span={11}>
                <Form.Item
                  style={{ marginTop: '1rem' }}
                  name='password'
                  rules={[
                    {
                      required: true,
                    },
                    {
                      min: 8,
                      message: t('invalid_password'),
                    },
                    {
                      max: 32,
                      message: t('invalid_password'),
                    },
                  ]}
                >
                  <Input.Password
                    className={classes.input}
                    prefix={<LockOutlined className={classes.prefixIcon} />}
                    placeholder={t('password')}
                  />
                </Form.Item>
              </Col>
              <Col span={11}>
                <Form.Item
                  style={{ marginTop: '1rem' }}
                  name='confirmPassword'
                  rules={[
                    {
                      required: true,
                    },
                    {
                      min: 8,
                      message: t('invalid_password'),
                    },
                    {
                      max: 32,
                      message: t('invalid_password'),
                    },
                  ]}
                >
                  <Input.Password
                    className={classes.input}
                    prefix={<LockOutlined className={classes.prefixIcon} />}
                    placeholder={t('confirm_password')}
                  />
                </Form.Item>
              </Col>
            </Row>

            <Form.Item className={classes.footer}>
              <Button
                type='primary'
                size='large'
                htmlType='submit'
                className={classes.submitButton}
                loading={registerLoading}
              >
                {t('register')}
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  )
}

export default RegisterPage
