import Head from 'next/head'
import { useTranslation } from 'react-i18next'
import React, { useContext } from 'react'
import BaseLayout from '@/components/general/base-layout/index'
import AppContext from '@/contexts/app/context'
import { Typography } from 'antd'
import Footer from '@/components/general/footer'

const { Title } = Typography

const Home: React.FC = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('table')}
        </title>
      </Head>

      <BaseLayout footer={<Footer />}>
        <Title> this is table page </Title>
      </BaseLayout>
    </div>
  )
}

const HomePage: React.FC = () => {
  return <Home />
}
export default HomePage
