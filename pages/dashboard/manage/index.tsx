import React, { useContext, useEffect, useState } from 'react'
import Head from 'next/head'
import { useTranslation } from 'react-i18next'
import BaseLayout from '@/components/general/base-layout/index'
import AppContext from '@/contexts/app/context'
import LoadingPage from '@/components/general/loading-page'
import ManageSideBar from '@/components/manage/side-bar'
import { IDefaultTrip } from '../../../models/default-trip/response'
import { PointStatus } from '@/models/pos/enum'
import ManageFooter from '@/components/manage/footer'
import dynamic from 'next/dynamic'
const OurMap = dynamic(() => import('@/components/mapbox/map/index'), {
  ssr: false,
  loading: () => <LoadingPage height={100} />,
})
import { FILTER_DATE_FORMATE } from '../../../utils/constants'
import DefaultTripContextProvider from '@/contexts/default-trip/provider'
import DefaultTripContext from '@/contexts/default-trip/context'
import { DEFAULT_MAXIMUM_PAGE_SIZE } from '@/contexts/constants'
import IMapFilter from '@/models/base/filter-items'
import moment from 'moment'
import { IPOS } from '@/models/pos/response'
import { ICoordinate } from '@/components/mapbox/map/index'
import SocketContext from '../../../contexts/socket/context'
import TripContext from '@/contexts/trip/context'
import TripContextProvider from '@/contexts/trip/provider'
import { ISortedPOS, ITrip } from '../../../models/trip/response'
import { SortedPOSStatus } from '@/models/trip/enum'

import styles from './style.module.scss'
import { Notification } from '@/utils/helpers/notification'
import PreviewTrip from '@/components/default-trip/preview-trip'

const Home: React.FC = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  const [showCustomizedActions, setShowCustomizedActions] = useState(false)

  // Socket
  const {
    driverLocation,
    selectedTrip,
    actions: socketActions,
    POSChangedStatus,
  } = useContext(SocketContext)

  useEffect(() => {
    if (POSChangedStatus) {
      console.log('POSChangedStatus: ', POSChangedStatus)

      let point = todaySelectedTrip?.pointsOfSaleSorted?.find(
        (p) => p.id === POSChangedStatus?.posId
      )

      let changedPoint = point ?? {
        ...previewTrip?.pointsOfSale?.find(
          (p) => p.id === POSChangedStatus?.posId
        ),
        status: POSChangedStatus?.status,
      }

      changedPoint.status = POSChangedStatus?.status

      console.log('changedPoint: ', changedPoint)
      setTodaySelectedTrip({
        ...todaySelectedTrip,
        pointsOfSaleSorted: [
          ...todaySelectedTrip?.pointsOfSaleSorted?.filter(
            (p) => p?.id != POSChangedStatus?.posId
          ),
          changedPoint,
        ],
      })
    }
  }, [POSChangedStatus])

  // Filters
  const [filters, setFilters] = useState<IMapFilter>({
    date: moment(),
  })

  // Trips
  const {
    data: trips,
    loading: tripsLoading,
    actions: tripsActions,
    actionLoading: actionsTripsLoading,
  } = useContext(TripContext)

  // Trip
  const [todaySelectedTrip, setTodaySelectedTrip] = useState<ITrip>()
  const [todaySelectedPOS, setTodaySelectedPOS] = useState<ISortedPOS>()

  useEffect(() => {
    if (todaySelectedTrip) {
      socketActions?.setSelectedTrip(todaySelectedTrip)
    }

    if (previewTrip) {
      socketActions?.setPreviewTrip(previewTrip)
    }
  }, [todaySelectedTrip])

  // Default Trips
  const [defaultTrip, setDefaultTrip] = useState<IDefaultTrip>()

  const {
    actions: defaultTripsActions,
    data: defaultTrips,
    loading: defaultTripsLoading,
    previewTrip,
    previewTripLoading,
  } = useContext(DefaultTripContext)

  // Preview
  useEffect(() => {
    defaultTrip &&
      defaultTripsActions.previewTrip(defaultTrip?.id, {
        date: filters?.date?.format(FILTER_DATE_FORMATE),
      })
  }, [defaultTrip])

  useEffect(() => {
    // Old Trips
    if (moment().isAfter(filters.date, 'day')) {
      console.log('Old')
      setShowCustomizedActions(false)
      tripsActions.setPageSize(DEFAULT_MAXIMUM_PAGE_SIZE)
      tripsActions.setFilter({
        date: filters?.date?.format(FILTER_DATE_FORMATE),
      })
    }
    // Today Trips
    else if (moment().isSame(filters.date, 'day')) {
      console.log('Today')
      socketActions?.setSelectedTrip(todaySelectedTrip)
      socketActions?.setPreviewTrip(previewTrip)

      setShowCustomizedActions(true)
      tripsActions.setPageSize(DEFAULT_MAXIMUM_PAGE_SIZE)
      tripsActions.setFilter({
        date: filters?.date?.format(FILTER_DATE_FORMATE),
      })
    }
    // New Trips
    else {
      console.log('New')
      setShowCustomizedActions(true)
      // Default trips with preview
      defaultTripsActions.setPageSize(DEFAULT_MAXIMUM_PAGE_SIZE)
      defaultTripsActions.setFilter({
        date: filters?.date?.format(FILTER_DATE_FORMATE),
      })
    }
  }, [filters])

  // Point of sales
  const [defaultSelectedPOS, setDefaultSelectedPOS] = useState<IPOS>()

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('manage')}
        </title>
      </Head>

      <BaseLayout
        sideBar={
          <ManageSideBar
            showCustomizedActions={showCustomizedActions}
            filters={filters}
            setDefaultSelectedPOS={setDefaultSelectedPOS}
            defaultSelectedPOS={defaultSelectedPOS}
            selectedDefaultTrip={defaultTrip}
            onDefaultTripSelected={(defaultTrip) => {
              setDefaultTrip(defaultTrip)
            }}
            todaySelectedPOS={todaySelectedPOS}
            setTodaySelectedPOS={setTodaySelectedPOS}
            selectedTrip={todaySelectedTrip}
            onTripSelected={(trip) => {
              setTodaySelectedTrip(trip)
            }}
          />
        }
        footer={
          <ManageFooter
            onDateChange={(date) => {
              setFilters({
                date: date,
              })
            }}
          />
        }
      >
        <div className={styles.mapContainer}>
          <OurMap
            coordinates={getCoordinates(
              filters,
              previewTrip?.pointsOfSale,
              defaultSelectedPOS,
              setDefaultSelectedPOS,

              // todaySelectedTrip?.pointsOfSaleSorted,
              previewTrip?.pointsOfSale,
              todaySelectedPOS,
              setTodaySelectedPOS,

              selectedTrip
            )}
          />
        </div>
      </BaseLayout>
    </div>
  )
}

const HomePage: React.FC = () => {
  return (
    <DefaultTripContextProvider>
      {/* <TripContextProvider> */}
      <Home />
      {/* </TripContextProvider> */}
    </DefaultTripContextProvider>
  )
}
export default HomePage

// Helpers
export const getTodayPOSsStatus: (sortedPOS: ISortedPOS) => PointStatus = (
  sortedPOS
) => {
  switch (sortedPOS?.status) {
    case SortedPOSStatus.Idle:
      return PointStatus.Primary

    case SortedPOSStatus.Intransit:
      return PointStatus.InTransit

    case SortedPOSStatus.Success:
      return PointStatus.Success

    case SortedPOSStatus.Failure:
      return PointStatus.Failure

    default:
      return PointStatus.Disable
  }
}

const getCoordinates: (
  filters: IMapFilter,
  defaultPointsOfSale: IPOS[],
  defaultSelectedPOS: IPOS,
  setDefaultSelectedPOS: (pos: IPOS) => void,

  todayPointsOfSale: IPOS[],
  todaySelectedPOS: ISortedPOS,
  setTodaySelectedPOS: (sortedPos: ISortedPOS) => void,
  todaySelectedTrip: ITrip
) => ICoordinate[] = (
  filters,
  // New Trips
  defaultPointsOfSale,
  defaultSelectedPOS,
  setDefaultSelectedPOS,

  // Today Trips
  todayPointsOfSale,
  todaySelectedPOS,
  setTodaySelectedPOS,
  todaySelectedTrip
) => {
  // Old Trips
  if (moment().isAfter(filters.date, 'day')) {
  }
  // Today Trips
  else if (moment().isSame(filters.date, 'day')) {
    return (
      todayPointsOfSale &&
      todayPointsOfSale?.map((todayPoint, _) => {
        let todaySortedPos = {
          ...todayPoint,
          status: todayPoint?.status ?? SortedPOSStatus?.Idle,
          defaultOrder: todayPoint?.defaultOrder,
        }
        // let todaySortedPos = todaySelectedTrip?.pointsOfSaleSorted?.find(
        //   (p) => p?.id === todayPoint?.id
        // ) ?? {
        //   ...todayPoint,
        //   status: todayPoint?.status ?? SortedPOSStatus?.Idle,
        //   defaultOrder: todayPoint?.defaultOrder,
        // }

        return {
          id: todayPoint?.id,
          long: todayPoint?.long,
          lat: todayPoint?.lat,

          defaultOrder: todaySortedPos?.defaultOrder, // todayPoint?.defaultOrder,
          status: getTodayPOSsStatus(todaySortedPos),
          onClick: () => {
            setTodaySelectedPOS(todaySortedPos)
          },
          label:
            todaySelectedPOS?.id == todayPoint?.id && todaySelectedPOS?.name,
        }
      })
    )
  }
  // New Trips
  else {
    return (
      defaultPointsOfSale &&
      defaultPointsOfSale?.map((point, _) => {
        return {
          id: point?.id,
          long: point?.long,
          lat: point?.lat,
          status:
            defaultSelectedPOS?.id == point?.id
              ? PointStatus.Primary
              : PointStatus.Disable,
          onClick: () => {
            setDefaultSelectedPOS(point)
          },
          label:
            defaultSelectedPOS?.id == point?.id && defaultSelectedPOS?.name,
        }
      })
    )
  }
}
