import React, { useContext, useEffect, useState } from 'react'

import AppContext from '@/contexts/app/context'
import { IStaticFilter } from '@/models/filter'
import { Operations } from '@/models/filter/enum'
import { UserGender } from '@/models/user/enums'
import BaseLayout from '../../../../components/general/base-layout/index'
import Footer from '@/components/general/footer'
import SettingsLayout from '@/components/general/settings/layout/index'
import SettingsSideBar from '@/components/general/settings/side-bar/index'
import ProductContext from '../../../../contexts/product/context'
import { useTranslation } from 'react-i18next'
import { ColumnProps } from 'antd/lib/table'
import { IUser } from '../../../../models/user/resopnse'
import {
  Button,
  Popconfirm,
  Tag,
  Tooltip,
  Table,
  Divider,
  Space,
  Switch,
} from 'antd'
import { IProduct } from '../../../../models/product/response'
import {
  InfoCircleFilled,
  EditFilled,
  ManOutlined,
  WomanOutlined,
  PlusOutlined,
  SyncOutlined,
} from '@ant-design/icons'
import Head from 'next/head'
import ProductContextProvider from '@/contexts/product/provider'
import PaginationCard from '@/components/general/pagination-card'
import { tableOnChange } from '@/utils/helpers/table-sorts-filters'
import InfoModal from '@/components/general/info-modal'
import ProductInfo from '@/components/products/product-info'
import CreateFormModal from '@/components/general/create-form-modal'
import EditFormModal from '@/components/general/edit-form-modal'
import ProductForm from '@/components/products/product-form'
import { IProductEdit } from '@/models/product/request'
import SettingsHeader from '@/components/general/settings/page-header/index'

import styles from './style.module.scss'
import { IMedium } from '../../../../models/medium/resopnse'

const Products = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  const {
    data,
    loading,
    details,
    error,
    actions,
    actionLoading,
    detailsLoading,
    page,
    pageSize,
    filter,
  } = useContext(ProductContext)

  const [createVisible, setCreateVisible] = useState(false)

  const [infoVisible, setInfoVisible] = useState(false)
  const [infoId, setInfoId] = useState<number>()

  const [editVisible, setEditVisible] = useState(false)
  const [editId, setEditId] = useState<number>()

  useEffect(() => {
    infoId && actions.getDetails(infoId)
  }, [infoId])

  useEffect(() => {
    editId && actions.getDetails(editId)
  }, [editId])

  useEffect(() => {
    if (!infoVisible) setInfoId(undefined)
    if (!editVisible) setEditId(undefined)
  }, [editVisible, infoVisible])

  // Table Filters Props
  let tableFiltersProps = {
    gender: {
      operation: Operations.IN,
      filterProps: [
        {
          text: t(`${UserGender[UserGender.male]}`),
          value: UserGender.male,
        },
        {
          text: t(`${UserGender[UserGender.female]}`),
          value: UserGender.female,
        },
      ],
    },
  }

  // Search
  const [keySearch, setKeySearch] = useState<string>()

  useEffect(() => {
    // Filters
    let filters: IStaticFilter[] = []

    actions.setFilter({
      key_search: keySearch,
    })
  }, [keySearch])

  // Table Columns
  const columns: ColumnProps<IProduct>[] = [
    {
      title: '',
      dataIndex: 'image',
      render: (image: IMedium) => {
        return image?.url
      },
    },
    {
      title: t('name'),
      dataIndex: 'name',
      ellipsis: { showTitle: false },
      render: (name: string) => {
        return (
          <Tooltip title={name} placement='bottomRight'>
            {name}
          </Tooltip>
        )
      },
    },
    {
      title: t('category'),
      dataIndex: 'gender',
      render: (_, product) => {
        return <Tag color='default'>{product?.category?.name}</Tag>
      },
    },
    {
      title: t('actions'),
      width: 125,
      align: 'center',
      render: (product: IProduct) => {
        return (
          <Space>
            <Tooltip title={t('show_info')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<InfoCircleFilled />}
                type='ghost'
                size='small'
                onClick={() => {
                  setInfoId(product?.id)
                  setInfoVisible(true)
                }}
              />
            </Tooltip>

            <Tooltip title={t('edit')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<EditFilled />}
                size='small'
                type='ghost'
                onClick={() => {
                  setEditId(product?.id)
                  setEditVisible(true)
                }}
              />
            </Tooltip>
          </Space>
        )
      },
    },
  ]

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('products')}
        </title>
      </Head>
      <BaseLayout footer={<Footer />} sideBar={<SettingsSideBar />}>
        <SettingsLayout>
          <div>
            <SettingsHeader
              title={t('products')}
              subTitle={t('products_subtitle')}
              extra={[
                <Button
                  type='ghost'
                  onClick={() => {
                    actions.getData()
                  }}
                  icon={<SyncOutlined />}
                >
                  {t('refresh')}
                </Button>,
                <Divider type='vertical' />,

                <Button
                  type='primary'
                  onClick={() => {
                    setCreateVisible(true)
                  }}
                  icon={<PlusOutlined />}
                >
                  {t('add_product')}
                </Button>,
              ]}
            />
            <Table
              columns={columns}
              dataSource={data?.data ?? []}
              rowKey='id'
              pagination={false}
              loading={loading}
              scroll={{ scrollToFirstRowOnChange: true, x: 1000 }}
            />

            {/* Pagination Card */}
            <PaginationCard
              showQuickJumper
              responsive
              total={data?.meta?.total}
              onChange={(page) => {
                actions.setPage(page)
              }}
              onShowSizeChange={(_, size) => {
                actions.setPageSize(size)
              }}
              current={page}
              pageSize={pageSize}
            />
          </div>
        </SettingsLayout>

        {/* Drawers */}
        {/* Products Info Drawer */}
        <InfoModal
          id='product_info'
          title={t('product_info')}
          visible={infoVisible}
          toggleVisible={setInfoVisible}
          loading={detailsLoading}
          confirmButtonText={t('ok')}
        >
          <ProductInfo product={details} />
        </InfoModal>

        {/* Create Product Drawer */}
        <CreateFormModal
          key='create'
          title={t('create_product')}
          visible={createVisible}
          toggleVisible={setCreateVisible}
          onSubmit={async (createData) => {
            return await actions.createProduct(createData)
          }}
          submitLoading={actionLoading}
          submitText={t('add')}
          width={'60vw'}
        >
          <ProductForm />
        </CreateFormModal>

        {/* Edit Product Drawer */}
        <EditFormModal<IProductEdit>
          defaultValues={{
            name: details?.name,
            categoryId: details?.category?.id,
            imageId: details?.image?.id,
          }}
          key='edit'
          title={t('edit_product')}
          visible={editVisible}
          toggleVisible={setEditVisible}
          onSubmit={async (editData) => {
            return await actions.updateProduct(editId, editData)
          }}
          submitLoading={actionLoading}
          loading={detailsLoading}
        >
          <ProductForm product={details} />
        </EditFormModal>
      </BaseLayout>
    </div>
  )
}

const ProductsPage = () => {
  return (
    <ProductContextProvider>
      <Products />
    </ProductContextProvider>
  )
}
export default ProductsPage
