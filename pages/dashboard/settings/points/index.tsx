import React, { useContext, useEffect, useState } from 'react'
import AppContext from '@/contexts/app/context'
import BaseLayout from '@/components/general/base-layout/index'
import Footer from '@/components/general/footer'
import SettingsLayout from '@/components/general/settings/layout/index'
import SettingsSideBar from '@/components/general/settings/side-bar/index'
import POSContext from '@/contexts/pos/context'
import { useTranslation } from 'react-i18next'
import { ColumnProps } from 'antd/lib/table'
import { Button, Tooltip, Table, Divider, Space } from 'antd'
import {
  InfoCircleFilled,
  EditFilled,
  PlusOutlined,
  SyncOutlined,
} from '@ant-design/icons'
import Head from 'next/head'
import PaginationCard from '@/components/general/pagination-card'
import CreateFormModal from '@/components/general/create-form-modal'
import EditFormModal from '@/components/general/edit-form-modal'
import { days, IPOS } from '@/models/pos/response'
import SettingsHeader from '@/components/general/settings/page-header'
import POSInfo from '@/components/pos/pos-info'
import POSForm from '@/components/pos/pos-form'
import { IPOSEdit } from '@/models/pos/request'
import POSContextProvider from '@/contexts/pos/provider'

import styles from './style.module.scss'
import InfoModal from '../../../../components/general/info-modal/index'
import moment from 'moment'
import { IPOSFrom } from '../../../../components/pos/pos-form/index'
import { ICity } from '@/models/city/response'
import { IRegion } from '@/models/region/response'

const POSs = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  const {
    data,
    loading,
    details,
    error,
    actions,
    actionLoading,
    detailsLoading,
    page,
    pageSize,
    filter,
  } = useContext(POSContext)

  const [createVisible, setCreateVisible] = useState(false)

  const [infoVisible, setInfoVisible] = useState(false)
  const [infoId, setInfoId] = useState<number>()

  const [editVisible, setEditVisible] = useState(false)
  const [editId, setEditId] = useState<number>()

  useEffect(() => {
    infoId && actions.getDetails(infoId)
  }, [infoId])

  useEffect(() => {
    editId && actions.getDetails(editId)
  }, [editId])

  useEffect(() => {
    if (!infoVisible) setInfoId(undefined)
    if (!editVisible) setEditId(undefined)
  }, [editVisible, infoVisible])

  // Table Filters Props
  let tableFiltersProps = {}

  // Search
  const [keyword, setKeyword] = useState<string>()

  useEffect(() => {
    // Filters
    actions.setFilter({
      key_search: keyword,
    })
  }, [keyword])

  // Table Columns
  const columns: ColumnProps<IPOS>[] = [
    {
      title: t('name'),
      dataIndex: 'name',
      ellipsis: { showTitle: false },
      render: (name: string) => {
        return (
          <Tooltip title={name} placement='bottomRight'>
            {name}
          </Tooltip>
        )
      },
    },
    {
      title: t('address'),
      dataIndex: 'address',
      ellipsis: { showTitle: false },
      render: (address: string) => {
        return (
          <Tooltip title={address} placement='bottomRight'>
            {address}
          </Tooltip>
        )
      },
    },
    {
      title: t('city'),
      dataIndex: 'city',
      ellipsis: { showTitle: false },
      render: (city: ICity) => {
        return (
          <Tooltip title={city?.nameTranslations?.ar} placement='bottomRight'>
            {city?.nameTranslations?.ar}
          </Tooltip>
        )
      },
    },
    {
      title: t('region'),
      dataIndex: 'region',
      ellipsis: { showTitle: false },
      render: (region: IRegion) => {
        return (
          <Tooltip title={region?.nameTranslations?.ar} placement='bottomRight'>
            {region?.nameTranslations?.ar}
          </Tooltip>
        )
      },
    },
    {
      title: t('actions'),
      width: 125,
      align: 'center',
      render: (pos: IPOS) => {
        return (
          <Space>
            <Tooltip title={t('show_info')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<InfoCircleFilled />}
                type='ghost'
                size='small'
                onClick={() => {
                  setInfoId(pos?.id)
                  setInfoVisible(true)
                }}
              />
            </Tooltip>

            <Tooltip title={t('edit')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<EditFilled />}
                size='small'
                type='ghost'
                onClick={() => {
                  setEditId(pos?.id)
                  setEditVisible(true)
                }}
              />
            </Tooltip>
          </Space>
        )
      },
    },
  ]

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('poss')}
        </title>
      </Head>
      <BaseLayout footer={<Footer />} sideBar={<SettingsSideBar />}>
        <SettingsLayout>
          <div>
            <SettingsHeader
              title={t('poss')}
              subTitle={t('poss_subtitle')}
              extra={[
                // <Search></Search>,
                <Button
                  type='ghost'
                  onClick={() => {
                    actions.getData()
                  }}
                  icon={<SyncOutlined />}
                >
                  {t('refresh')}
                </Button>,
                <Divider type='vertical' />,

                <Button
                  type='primary'
                  onClick={() => {
                    setCreateVisible(true)
                  }}
                  icon={<PlusOutlined />}
                >
                  {t('add_pos')}
                </Button>,
              ]}
            />
            <Table
              columns={columns}
              dataSource={data?.data ?? []}
              rowKey='id'
              pagination={false}
              loading={loading}
              scroll={{ scrollToFirstRowOnChange: true, x: 1000 }}
            />

            {/* Pagination Card */}
            <PaginationCard
              showQuickJumper
              responsive
              total={data?.meta?.total}
              onChange={(page) => {
                actions.setPage(page)
              }}
              onShowSizeChange={(_, size) => {
                actions.setPageSize(size)
              }}
              current={page}
              pageSize={pageSize}
            />
          </div>
        </SettingsLayout>

        {/* Drawers */}
        {/* POSs Info Drawer */}
        <InfoModal
          id='pos-info'
          confirmButtonText={t('ok')}
          title={t('pos_info')}
          visible={infoVisible}
          toggleVisible={setInfoVisible}
          loading={detailsLoading}
          width='50vw'
        >
          <POSInfo pos={details} />
        </InfoModal>

        {/* Create POS Drawer */}
        <CreateFormModal
          key='create'
          title={t('create_pos')}
          visible={createVisible}
          toggleVisible={setCreateVisible}
          onSubmit={async (createData) => {
            // Location
            createData.lat = createData?.location?.lat
            createData.long = createData?.location?.lng
            delete createData?.location

            // Work Times
            let workTimes = {}
            console.log('createData.workTimes: ', createData.workTimes)

            for (const [day, _] of Object.entries(days)) {
              let dates = createData?.workTimes?.[day]?.date
              let vacation = createData?.workTimes?.[day]?.vacation

              workTimes[day] = {
                vacation: vacation ?? false,
                open: dates
                  ? moment(dates[0]).format('hh:mm')
                  : moment().format('hh:mm'),
                close: dates
                  ? moment(dates[1]).format('hh:mm')
                  : moment().format('hh:mm'),
              }
              createData.work_times = workTimes
            }
            delete createData.workTimes

            return await actions.createPOS(createData)
          }}
          submitLoading={actionLoading}
        >
          <POSForm />
        </CreateFormModal>

        {/* Edit POS Drawer */}
        <EditFormModal<IPOSFrom>
          defaultValues={{
            name: details?.name,
            address: details?.address,
            city_id: details?.city?.id,
            region_id: details?.region?.id,
            location: {
              lat: details?.lat,
              lng: details?.long,
            },
            workTimes: {
              Saturday: {
                date: [
                  moment(details?.workTimes?.Saturday?.open, 'h:mm a'),
                  moment(details?.workTimes?.Saturday?.close, 'h:mm a'),
                ],
                vacation: details?.workTimes?.Saturday?.vacation,
              },
              Sunday: {
                date: [
                  moment(details?.workTimes?.Sunday?.open, 'h:mm a'),
                  moment(details?.workTimes?.Sunday?.close, 'h:mm a'),
                ],
                vacation: details?.workTimes?.Sunday?.vacation,
              },
              Monday: {
                date: [
                  moment(details?.workTimes?.Monday?.open, 'h:mm a'),
                  moment(details?.workTimes?.Monday?.close, 'h:mm a'),
                ],
                vacation: details?.workTimes?.Monday?.vacation,
              },
              Tuesday: {
                date: [
                  moment(details?.workTimes?.Tuesday?.open, 'h:mm a'),
                  moment(details?.workTimes?.Tuesday?.close, 'h:mm a'),
                ],
                vacation: details?.workTimes?.Tuesday?.vacation,
              },
              Wednesday: {
                date: [
                  moment(details?.workTimes?.Wednesday?.open, 'h:mm a'),
                  moment(details?.workTimes?.Wednesday?.close, 'h:mm a'),
                ],
                vacation: details?.workTimes?.Wednesday?.vacation,
              },
              Thursday: {
                date: [
                  moment(details?.workTimes?.Thursday?.open, 'h:mm a'),
                  moment(details?.workTimes?.Thursday?.close, 'h:mm a'),
                ],
                vacation: details?.workTimes?.Thursday?.vacation,
              },
              Friday: {
                date: [
                  moment(details?.workTimes?.Friday?.open, 'h:mm a'),
                  moment(details?.workTimes?.Friday?.close, 'h:mm a'),
                ],
                vacation: details?.workTimes?.Friday?.vacation,
              },
            },
          }}
          key='edit'
          title={t('edit_pos')}
          visible={editVisible}
          toggleVisible={setEditVisible}
          onSubmit={async (editData) => {
            // Location
            editData.lat = editData?.location?.lat
            editData.long = editData?.location?.lng
            delete editData?.location

            // Work Times
            let workTimes = {}
            console.log('editData.workTimes: ', editData.workTimes)

            for (const [day, _] of Object.entries(days)) {
              let dates = editData?.workTimes?.[day]?.date
              let vacation = editData?.workTimes?.[day]?.vacation

              workTimes[day] = {
                vacation: vacation ?? false,
                open: dates
                  ? moment(dates[0]).format('hh:mm')
                  : moment().format('hh:mm'),
                close: dates
                  ? moment(dates[1]).format('hh:mm')
                  : moment().format('hh:mm'),
              }
              editData.work_times = workTimes
            }
            delete editData.workTimes

            return await actions.updatePOS(editId, editData)
          }}
          submitLoading={actionLoading}
          loading={detailsLoading}
        >
          <POSForm pos={details} />
        </EditFormModal>
      </BaseLayout>
    </div>
  )
}

const POSsPage = () => {
  return (
    <POSContextProvider>
      <POSs />
    </POSContextProvider>
  )
}
export default POSsPage
