import React, { useContext, useEffect, useState } from 'react'

import AppContext from '@/contexts/app/context'
import { IStaticFilter } from '@/models/filter'
import { Operations } from '@/models/filter/enum'
import { UserGender } from '@/models/user/enums'
import { isEmpty, isNull, isUndefined } from 'lodash'
import BaseLayout from '@/components/general/base-layout/index'
import Footer from '@/components/general/footer'
import SettingsLayout from '@/components/general/settings/layout/index'
import SettingsSideBar from '@/components/general/settings/side-bar/index'
import DriverContext from '@/contexts/driver/context'
import { useTranslation } from 'react-i18next'
import { ColumnProps } from 'antd/lib/table'
import { IUser } from '@/models/user/resopnse'
import { Button, Popconfirm, Tag, Tooltip, Table, Divider, Space } from 'antd'
import { IDriver } from '@/models/driver/response'
import {
  InfoCircleFilled,
  EditFilled,
  ManOutlined,
  WomanOutlined,
  SyncOutlined,
  PlusOutlined,
} from '@ant-design/icons'
import Head from 'next/head'
import DriverContextProvider from '@/contexts/driver/provider'
import PaginationCard from '@/components/general/pagination-card'
import InfoModal from '@/components/general/info-modal'
import DriverInfo from '@/components/drivers/driver-info'
import CreateFormModal from '@/components/general/create-form-modal'
import EditFormModal from '@/components/general/edit-form-modal'
import DriverForm from '@/components/drivers/driver-form'
import { IDriverEdit } from '@/models/driver/request'
import SettingsHeader from '@/components/general/settings/page-header/index'

const Drivers = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  const {
    data,
    loading,
    details,
    error,
    actions,
    actionLoading,
    detailsLoading,
    page,
    pageSize,
    filter,
  } = useContext(DriverContext)

  const [createVisible, setCreateVisible] = useState(false)

  const [infoVisible, setInfoVisible] = useState(false)
  const [infoId, setInfoId] = useState<number>()

  const [editVisible, setEditVisible] = useState(false)
  const [editId, setEditId] = useState<number>()

  useEffect(() => {
    infoId && actions.getDetails(infoId)
  }, [infoId])

  useEffect(() => {
    editId && actions.getDetails(editId)
  }, [editId])

  useEffect(() => {
    if (!infoVisible) setInfoId(undefined)
    if (!editVisible) setEditId(undefined)
  }, [editVisible, infoVisible])

  // Table Filters Props
  let tableFiltersProps = {
    gender: {
      operation: Operations.IN,
      filterProps: [
        {
          text: t(`${UserGender[UserGender.male]}`),
          value: UserGender.male,
        },
        {
          text: t(`${UserGender[UserGender.female]}`),
          value: UserGender.female,
        },
      ],
    },
  }

  // Search
  const [keySearch, setKeySearch] = useState<string>()

  useEffect(() => {
    // Filters
    let filters: IStaticFilter[] = []

    actions.setFilter({
      key_search: keySearch,
    })
  }, [keySearch])

  // Table Columns
  const columns: ColumnProps<IDriver>[] = [
    {
      title: t('name'),
      dataIndex: 'user',
      ellipsis: { showTitle: false },
      render: (user: IUser) => {
        return (
          <Tooltip
            title={`${user?.firstName} ${user?.lastName}`}
            placement='bottomRight'
          >
            {`${user?.firstName} ${user?.lastName}`}
          </Tooltip>
        )
      },
    },
    {
      title: t('email'),
      dataIndex: 'email',
      render: (_, driver) => {
        return driver?.user?.email
      },
    },
    {
      title: t('gender'),
      dataIndex: 'gender',
      filters: tableFiltersProps.gender.filterProps,
      render: (_, driver) => {
        return (
          <Tag
            color={driver?.user?.gender == 'male' ? '#009CFF' : '#FF85C0'}
            icon={
              driver?.user.gender == 'male' ? (
                <ManOutlined />
              ) : (
                <WomanOutlined />
              )
            }
          >
            {driver?.user?.gender}
          </Tag>
        )
      },
    },
    {
      title: t('actions'),
      width: 125,
      align: 'center',
      render: (driver: IDriver) => {
        return (
          <Space>
            <Tooltip title={t('show_info')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<InfoCircleFilled />}
                type='ghost'
                size='small'
                onClick={() => {
                  setInfoId(driver?.id)
                  setInfoVisible(true)
                }}
              />
            </Tooltip>

            <Tooltip title={t('edit')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<EditFilled />}
                size='small'
                type='ghost'
                onClick={() => {
                  setEditId(driver?.id)
                  setEditVisible(true)
                }}
              />
            </Tooltip>
          </Space>
        )
      },
    },
  ]

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('drivers')}
        </title>
      </Head>
      <BaseLayout footer={<Footer />} sideBar={<SettingsSideBar />}>
        <SettingsLayout>
          <div>
            <SettingsHeader
              title={t('drivers')}
              subTitle={t('drivers_subtitle')}
              extra={[
                <Button
                  type='ghost'
                  onClick={() => {
                    actions.getData()
                  }}
                  icon={<SyncOutlined />}
                >
                  {t('refresh')}
                </Button>,
                <Divider type='vertical' />,

                <Button
                  type='primary'
                  onClick={() => {
                    setCreateVisible(true)
                  }}
                  icon={<PlusOutlined />}
                >
                  {t('add_driver')}
                </Button>,
              ]}
            />
            <Table
              columns={columns}
              dataSource={data?.data ?? []}
              rowKey='id'
              pagination={false}
              loading={loading}
              scroll={{ scrollToFirstRowOnChange: true, x: 1000 }}
            />

            {/* Pagination Card */}
            <PaginationCard
              showQuickJumper
              responsive
              total={data?.meta?.total}
              onChange={(page) => {
                actions.setPage(page)
              }}
              onShowSizeChange={(_, size) => {
                actions.setPageSize(size)
              }}
              current={page}
              pageSize={pageSize}
            />
          </div>
        </SettingsLayout>

        {/* Drawers */}
        {/* Drivers Info Drawer */}
        <InfoModal
          id={'driver_info'}
          title={t('driver_info')}
          visible={infoVisible}
          toggleVisible={setInfoVisible}
          loading={detailsLoading}
          confirmButtonText={t('ok')}
        >
          <DriverInfo driver={details} />
        </InfoModal>

        {/* Create Driver Drawer */}
        <CreateFormModal
          key='create'
          title={t('create_driver')}
          visible={createVisible}
          toggleVisible={setCreateVisible}
          onSubmit={async (createData) => {
            createData.role = 'driver' // TODO: Role Enum
            return await actions.createDriver(createData)
          }}
          submitLoading={actionLoading}
        >
          <DriverForm />
        </CreateFormModal>

        {/* Edit Driver Drawer */}
        <EditFormModal<IDriverEdit>
          defaultValues={{
            profile_img_id: details?.user?.profileImg.id,
            gender: details?.user?.gender,
            first_name: details?.user?.firstName,
            last_name: details?.user?.lastName,
            mobile_number: details?.user?.mobileNumber,
            birth_date: details?.user?.birthDate,
          }}
          key='edit'
          title={t('edit_driver')}
          visible={editVisible}
          toggleVisible={setEditVisible}
          onSubmit={async (editData) => {
            editData.role = 'driver' // TODO: Role Enum
            return await actions.updateDriver(editId, editData)
          }}
          submitLoading={actionLoading}
          loading={detailsLoading}
        >
          <DriverForm driver={details} />
        </EditFormModal>
      </BaseLayout>
    </div>
  )
}

const DriversPage = () => {
  return (
    <DriverContextProvider>
      <Drivers />
    </DriverContextProvider>
  )
}
export default DriversPage
