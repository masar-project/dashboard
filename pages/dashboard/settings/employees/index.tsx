import React, { useContext, useEffect, useState } from 'react'

import AppContext from '@/contexts/app/context'
import { IStaticFilter } from '@/models/filter'
import { Operations } from '@/models/filter/enum'
import { UserGender, UserRole } from '@/models/user/enums'
import { isEmpty, isNull, isUndefined } from 'lodash'
import BaseLayout from '../../../../components/general/base-layout/index'
import Footer from '@/components/general/footer'
import SettingsLayout from '@/components/general/settings/layout/index'
import SettingsSideBar from '@/components/general/settings/side-bar/index'
import EmployeeContext from '../../../../contexts/employee/context'
import { useTranslation } from 'react-i18next'
import { ColumnProps } from 'antd/lib/table'
import Img from '@/components/general/img'
import { IMedium } from '@/models/medium/resopnse'
import { IUser } from '../../../../models/user/resopnse'
import {
  Button,
  Popconfirm,
  Tag,
  Tooltip,
  Table,
  Divider,
  Space,
  Switch,
} from 'antd'
import { IEmployee } from '../../../../models/employee/response'
import {
  InfoCircleFilled,
  EditFilled,
  ManOutlined,
  WomanOutlined,
  PlusOutlined,
  SyncOutlined,
} from '@ant-design/icons'
import Head from 'next/head'
import EmployeeContextProvider from '@/contexts/employee/provider'
import PaginationCard from '@/components/general/pagination-card'
import { tableOnChange } from '@/utils/helpers/table-sorts-filters'

import styles from './style.module.scss'
import InfoModal from '@/components/general/info-modal'
import EmployeeInfo from '@/components/employees/employee-info'
import CreateFormModal from '@/components/general/create-form-modal'
import EditFormModal from '@/components/general/edit-form-modal'
import EmployeeForm from '@/components/employees/employee-form'
import { IEmployeeEdit } from '@/models/employee/request'
import SettingsHeader from '@/components/general/settings/page-header/index'

const Employees = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  const {
    data,
    loading,
    details,
    error,
    actions,
    actionLoading,
    detailsLoading,
    page,
    pageSize,
    filter,
  } = useContext(EmployeeContext)

  const [createVisible, setCreateVisible] = useState(false)

  const [infoVisible, setInfoVisible] = useState(false)
  const [infoId, setInfoId] = useState<number>()

  const [editVisible, setEditVisible] = useState(false)
  const [editId, setEditId] = useState<number>()

  useEffect(() => {
    infoId && actions.getDetails(infoId)
  }, [infoId])

  useEffect(() => {
    editId && actions.getDetails(editId)
  }, [editId])

  useEffect(() => {
    if (!infoVisible) setInfoId(undefined)
    if (!editVisible) setEditId(undefined)
  }, [editVisible, infoVisible])

  // Table Filters Props
  let tableFiltersProps = {
    gender: {
      operation: Operations.IN,
      filterProps: [
        {
          text: t(`${UserGender[UserGender.male]}`),
          value: UserGender.male,
        },
        {
          text: t(`${UserGender[UserGender.female]}`),
          value: UserGender.female,
        },
      ],
    },
  }

  // Search
  const [keySearch, setKeySearch] = useState<string>()

  useEffect(() => {
    // Filters
    let filters: IStaticFilter[] = []

    actions.setFilter({
      key_search: keySearch,
    })
  }, [keySearch])

  // Table Columns
  const columns: ColumnProps<IEmployee>[] = [
    {
      title: t('name'),
      dataIndex: 'user',
      ellipsis: { showTitle: false },
      render: (user: IUser) => {
        return (
          <Tooltip
            title={`${user?.firstName} ${user?.lastName}`}
            placement='bottomRight'
          >
            {`${user?.firstName} ${user?.lastName}`}
          </Tooltip>
        )
      },
    },
    {
      title: t('email'),
      dataIndex: 'email',
      render: (_, employee) => {
        return employee?.user?.email
      },
    },
    {
      title: t('gender'),
      dataIndex: 'gender',
      filters: tableFiltersProps.gender.filterProps,
      render: (_, employee) => {
        return (
          <Tag
            color={employee?.user?.gender == 'male' ? '#009CFF' : '#FF85C0'}
            icon={
              employee?.user.gender == 'male' ? (
                <ManOutlined />
              ) : (
                <WomanOutlined />
              )
            }
          >
            {employee?.user?.gender}
          </Tag>
        )
      },
    },
    {
      title: t('status'),
      dataIndex: 'status',
      render: (_, employee: IEmployee) => {
        return (
          <Switch
            checkedChildren={t('enabled')}
            unCheckedChildren={t('disabled')}
            defaultChecked={!employee?.disable}
            checked={!employee?.disable}
            loading={actionLoading}
            onChange={() => {
              if (employee?.disable) {
                actions.enableEmployee(employee.id)
              } else {
                actions.disableEmployee(employee.id)
              }
            }}
          />
        )
      },
    },
    {
      title: t('actions'),
      width: 125,
      align: 'center',
      render: (employee: IEmployee) => {
        return (
          <Space>
            <Tooltip title={t('show_info')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<InfoCircleFilled />}
                type='ghost'
                size='small'
                onClick={() => {
                  setInfoId(employee?.id)
                  setInfoVisible(true)
                }}
              />
            </Tooltip>

            <Tooltip title={t('edit')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<EditFilled />}
                size='small'
                type='ghost'
                onClick={() => {
                  setEditId(employee?.id)
                  setEditVisible(true)
                }}
              />
            </Tooltip>
          </Space>
        )
      },
    },
  ]

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('employees')}
        </title>
      </Head>
      <BaseLayout footer={<Footer />} sideBar={<SettingsSideBar />}>
        <SettingsLayout>
          <div>
            <SettingsHeader
              title={t('employees')}
              subTitle={t('employees_subtitle')}
              extra={[
                <Button
                  type='ghost'
                  onClick={() => {
                    actions.getData()
                  }}
                  icon={<SyncOutlined />}
                >
                  {t('refresh')}
                </Button>,
                <Divider type='vertical' />,

                <Button
                  type='primary'
                  onClick={() => {
                    setCreateVisible(true)
                  }}
                  icon={<PlusOutlined />}
                >
                  {t('add_employee')}
                </Button>,
              ]}
            />
            <Table
              columns={columns}
              dataSource={data?.data ?? []}
              rowKey='id'
              pagination={false}
              loading={loading}
              scroll={{ scrollToFirstRowOnChange: true, x: 1000 }}
            />

            {/* Pagination Card */}
            <PaginationCard
              showQuickJumper
              responsive
              total={data?.meta?.total}
              onChange={(page) => {
                actions.setPage(page)
              }}
              onShowSizeChange={(_, size) => {
                actions.setPageSize(size)
              }}
              current={page}
              pageSize={pageSize}
            />
          </div>
        </SettingsLayout>

        {/* Drawers */}
        {/* Employees Info Drawer */}
        <InfoModal
          id='employee_info'
          title={t('employee_info')}
          visible={infoVisible}
          toggleVisible={setInfoVisible}
          loading={detailsLoading}
          confirmButtonText={t('ok')}
        >
          <EmployeeInfo employee={details} />
        </InfoModal>

        {/* Create Employee Drawer */}
        <CreateFormModal
          key='create'
          title={t('create_employee')}
          visible={createVisible}
          toggleVisible={setCreateVisible}
          onSubmit={async (createData) => {
            return await actions.createEmployee(createData)
          }}
          submitLoading={actionLoading}
        >
          <EmployeeForm />
        </CreateFormModal>

        {/* Edit Employee Drawer */}
        <EditFormModal<IEmployeeEdit>
          defaultValues={{
            profile_img_id: details?.user?.profileImg.id,
            gender: details?.user?.gender,
            first_name: details?.user?.firstName,
            last_name: details?.user?.lastName,
            mobile_number: details?.user?.mobileNumber,
            birth_date: details?.user?.birthDate,
          }}
          key='edit'
          title={t('edit_employee')}
          visible={editVisible}
          toggleVisible={setEditVisible}
          onSubmit={async (editData) => {
            editData.role = 'employee' // TODO: Role Enum
            return await actions.updateEmployee(editId, editData)
          }}
          submitLoading={actionLoading}
          loading={detailsLoading}
        >
          <EmployeeForm employee={details} />
        </EditFormModal>
      </BaseLayout>
    </div>
  )
}

const EmployeesPage = () => {
  return (
    <EmployeeContextProvider>
      <Employees />
    </EmployeeContextProvider>
  )
}
export default EmployeesPage
