import React, { useContext, useEffect, useState } from 'react'
import AppContext from '@/contexts/app/context'
import BaseLayout from '@/components/general/base-layout/index'
import Footer from '@/components/general/footer'
import SettingsLayout from '@/components/general/settings/layout/index'
import SettingsSideBar from '@/components/general/settings/side-bar/index'
import { useTranslation } from 'react-i18next'
import { ColumnProps } from 'antd/lib/table'
import { Button, Tooltip, Table, Divider, Space, Popconfirm, Tag } from 'antd'
import {
  InfoCircleFilled,
  EditFilled,
  PlusOutlined,
  SyncOutlined,
} from '@ant-design/icons'
import Head from 'next/head'
import PaginationCard from '@/components/general/pagination-card'
import CreateFormModal from '@/components/general/create-form-modal'
import EditFormModal from '@/components/general/edit-form-modal'
import SettingsHeader from '@/components/general/settings/page-header'
import InfoModal from '../../../../components/general/info-modal/index'
import DefaultTripContext from '@/contexts/default-trip/context'
import DefaultTripContextProvider from '@/contexts/default-trip/provider'
import { IDefaultTripEdit } from '@/models/default-trip/request'
import { IDefaultTrip } from '@/models/default-trip/response'
import DefaultTripInfo from '@/components/default-trip/default-trip-info'
import DefaultTripForm from '@/components/default-trip/default-trip-form'
import { DeleteOutlined } from '@ant-design/icons'
import POSContextProvider from '@/contexts/pos/provider'
import { IDriver } from '@/models/driver/response'

const DefaultTrips = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  const {
    data,
    loading,
    details,
    error,
    actions,
    actionLoading,
    detailsLoading,
    page,
    pageSize,
    filter,
  } = useContext(DefaultTripContext)

  const [createVisible, setCreateVisible] = useState(false)

  const [infoVisible, setInfoVisible] = useState(false)
  const [infoId, setInfoId] = useState<number>()

  const [editVisible, setEditVisible] = useState(false)
  const [editId, setEditId] = useState<number>()

  useEffect(() => {
    infoId && actions.getDetails(infoId)
  }, [infoId])

  useEffect(() => {
    editId && actions.getDetails(editId)
  }, [editId])

  useEffect(() => {
    if (!infoVisible) setInfoId(undefined)
    if (!editVisible) setEditId(undefined)
  }, [editVisible, infoVisible])

  // Table Filters Props
  let tableFiltersProps = {}

  // Search
  const [keyword, setKeyword] = useState<string>()

  useEffect(() => {
    // Filters
    actions.setFilter({
      key_search: keyword,
    })
  }, [keyword])

  // Table Columns
  const columns: ColumnProps<IDefaultTrip>[] = [
    {
      title: t('name'),
      dataIndex: 'name',
      ellipsis: { showTitle: false },
      render: (name: string) => {
        return (
          <Tooltip title={name} placement='bottomRight'>
            {name}
          </Tooltip>
        )
      },
    },
    {
      title: t('day'),
      dataIndex: 'day',
      ellipsis: { showTitle: false },
      render: (day: string) => {
        return <Tag color='processing'>{t(day)}</Tag>
      },
    },
    {
      title: t('driver'),
      dataIndex: 'driver',
      ellipsis: { showTitle: false },
      render: (driver: IDriver) => {
        return (
          <Tooltip
            title={driver?.user?.firstName + ' ' + driver?.user?.lastName}
            placement='bottomRight'
          >
            {driver?.user?.firstName + ' ' + driver?.user?.lastName}
          </Tooltip>
        )
      },
    },
    {
      title: t('pointsOfSaleCount'),
      dataIndex: 'pointsOfSaleCount',
      ellipsis: { showTitle: false },
      render: (pointsOfSaleCount: number) => {
        return (
          <Tooltip title={pointsOfSaleCount} placement='bottomRight'>
            {pointsOfSaleCount}
          </Tooltip>
        )
      },
    },
    {
      title: t('actions'),
      width: 125,
      align: 'center',
      render: (defaultTrip: IDefaultTrip) => {
        return (
          <Space>
            <Tooltip title={t('show_info')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<InfoCircleFilled />}
                type='ghost'
                size='small'
                onClick={() => {
                  setInfoId(defaultTrip?.id)
                  setInfoVisible(true)
                }}
              />
            </Tooltip>

            <Tooltip title={t('edit')} mouseEnterDelay={0.5}>
              <Button
                shape='circle'
                icon={<EditFilled />}
                size='small'
                type='ghost'
                onClick={() => {
                  setEditId(defaultTrip?.id)
                  setEditVisible(true)
                }}
              />
            </Tooltip>

            <Tooltip title={t('delete')} mouseEnterDelay={0.5}>
              <Popconfirm
                title={t('delete_confirm')}
                onConfirm={() => {
                  actions.deleteDefaultTrip(defaultTrip?.id)
                }}
                onCancel={() => {}}
                okButtonProps={{ danger: true }}
                placement='bottomRight'
              >
                <Button
                  shape='circle'
                  icon={<DeleteOutlined />}
                  type='ghost'
                  size='small'
                  danger
                />
              </Popconfirm>
            </Tooltip>
          </Space>
        )
      },
    },
  ]

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('default_trips')}
        </title>
      </Head>
      <BaseLayout footer={<Footer />} sideBar={<SettingsSideBar />}>
        <SettingsLayout>
          <div>
            <SettingsHeader
              title={t('default_trips')}
              subTitle={t('default_trips_subtitle')}
              extra={[
                // <Search></Search>,
                <Button
                  type='ghost'
                  onClick={() => {
                    actions.getData()
                  }}
                  icon={<SyncOutlined />}
                >
                  {t('refresh')}
                </Button>,
                <Divider type='vertical' />,

                <Button
                  type='primary'
                  onClick={() => {
                    setCreateVisible(true)
                  }}
                  icon={<PlusOutlined />}
                >
                  {t('add_default_trip')}
                </Button>,
              ]}
            />
            <Table
              columns={columns}
              dataSource={data?.data ?? []}
              rowKey='id'
              pagination={false}
              loading={loading}
              scroll={{ scrollToFirstRowOnChange: true, x: 1000 }}
            />

            {/* Pagination Card */}
            <PaginationCard
              showQuickJumper
              responsive
              total={data?.meta?.total}
              onChange={(page) => {
                actions.setPage(page)
              }}
              onShowSizeChange={(_, size) => {
                actions.setPageSize(size)
              }}
              current={page}
              pageSize={pageSize}
            />
          </div>
        </SettingsLayout>

        {/* Modals */}
        {/* DefaultTrips Info Drawer */}
        <InfoModal
          id='default-trip-info'
          confirmButtonText={t('ok')}
          title={t('default_trip_info')}
          visible={infoVisible}
          toggleVisible={setInfoVisible}
          loading={detailsLoading}
        >
          <DefaultTripInfo defaultTrip={details} />
        </InfoModal>

        {/* Create DefaultTrip Drawer */}
        <CreateFormModal
          key='create'
          title={t('create_default_trip')}
          visible={createVisible}
          toggleVisible={setCreateVisible}
          onSubmit={async (createData) => {
            return await actions.createDefaultTrip(createData)
          }}
          submitLoading={actionLoading}
          width='70vw'
        >
          <DefaultTripForm />
        </CreateFormModal>

        {/* Edit DefaultTrip Drawer */}
        <EditFormModal<IDefaultTripEdit>
          defaultValues={{
            name: details?.name,
            day: details?.day,
            driver_id: details?.driver?.id,
            point_of_sale_ids: details?.pointsOfSale?.map((pos) => pos?.id),
          }}
          key='edit'
          title={t('edit_default-trip')}
          visible={editVisible}
          toggleVisible={setEditVisible}
          onSubmit={async (editData) => {
            return await actions.updateDefaultTrip(editId, editData)
          }}
          submitLoading={actionLoading}
          loading={detailsLoading}
          width='70vw'
        >
          <DefaultTripForm defaultTrip={details} />
        </EditFormModal>
      </BaseLayout>
    </div>
  )
}

const DefaultTripsPage = () => {
  return (
    <DefaultTripContextProvider>
      <POSContextProvider>
        <DefaultTrips />
      </POSContextProvider>
    </DefaultTripContextProvider>
  )
}
export default DefaultTripsPage
