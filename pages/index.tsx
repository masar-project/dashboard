import Head from 'next/head'
import { useTranslation } from 'react-i18next'
import React, { useContext, useEffect } from 'react'
import BaseLayout from '../components/general/base-layout/index'
import AppContext from '../contexts/app/context'
import { Typography } from 'antd'
import SideBar from '@/components/general/side-bar'
import Footer from '@/components/general/footer'
import { useRouter } from 'next/router'

import styles from './style.module.scss'
import { Notification } from '@/utils/helpers/notification'

const { Title } = Typography

const Home: React.FC = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)
  const router = useRouter()

  useEffect(() => {
    router.replace('/dashboard/manage')
  }, [])

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('home')}
        </title>
      </Head>

      <BaseLayout sideBar={<SideBar />} footer={<Footer />}>
        <Title>Loading...</Title>
      </BaseLayout>
    </div>
  )
}

const HomePage: React.FC = () => {
  return <Home />
}
export default HomePage
