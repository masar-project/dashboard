import React, { useContext } from 'react'
import Head from 'next/head'
import { Result } from 'antd'
import { useTranslation } from 'react-i18next'
import BaseLayout from '../../components/general/base-layout'
import AppContext from '../../contexts/app/context'

const Custom403: React.FC = () => {
  const { direction } = useContext(AppContext)

  const { t } = useTranslation()

  return (
    <div dir={direction}>
      <Head>
        <title>
          {t('masar')} - {t('403')}
        </title>
      </Head>

      <BaseLayout>
        <Result status='403' title='403' subTitle={t('forbidden_message')} />
      </BaseLayout>
    </div>
  )
}

export default Custom403
