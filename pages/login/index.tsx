import React, { useContext, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Typography, Card, Form, Input, Button, Row, Col, Divider } from 'antd'
import classes from './style.module.scss'
import AuthContext from '../../contexts/auth/context'

import { MailOutlined, LockOutlined } from '@ant-design/icons'
import Head from 'next/head'
import AppContext from '../../contexts/app/context'
import { useRouter } from 'next/router'
import { ILoginRequest } from '../../models/auth/request'

const { Title, Text } = Typography

import styles from './style.module.scss'

const LogInPage: React.FC = () => {
  const { t } = useTranslation()
  const { direction } = useContext(AppContext)

  const { actions, loginLoading, loginResponse, loginError, isAuthenticated } =
    useContext(AuthContext)

  useEffect(() => {
    if (isAuthenticated) {
      router.replace('/')
    }
  }, [isAuthenticated])

  const router = useRouter()

  useEffect(() => {
    if (loginResponse) {
      router.replace('/')
    }
  }, [loginResponse])

  const onSubmit = (data: ILoginRequest) => {
    actions.login(data)
  }

  return (
    <div dir={direction} style={{}} className={styles.container}>
      <Head>
        <title>
          {t('masar')} - {t('login')}
        </title>
      </Head>
      
      <div className={styles.bg} />
      <div className={styles.card}>
        <div style={{ width: '100%' }}>
      <img
              src='/images/text-logo.png'
              alt={'logo'}
              className={styles.textLogo}
            />
          <Title level={3} className={classes.title}>
            <Text className={classes.subtitle}>{t('welcome_masar')}</Text>
          </Title>

          <Title level={5} className={classes.title}>
            <Text>{t('enter_credential')}</Text>
          </Title>

          <Form
            hideRequiredMark
            layout='vertical'
            onFinish={onSubmit}
            className={classes.form}
            scrollToFirstError
            validateMessages={{
              required: t('field_is_required_message'),
            }}
          >
            <Form.Item
              name='email'
              rules={[
                {
                  type: 'string',
                },
                {
                  required: true,
                },
              ]}
            >
              <Input
                className={classes.input}
                type='email'
                prefix={<MailOutlined className={classes.prefixIcon} />}
                placeholder={t('email')}
              />
            </Form.Item>
            <Form.Item
              style={{ marginTop: '1rem' }}
              name='password'
              rules={[
                {
                  required: true,
                },
                {
                  min: 8,
                  message: t('invalid_password'),
                },
                {
                  max: 32,
                  message: t('invalid_password'),
                },
              ]}
            >
              <Input.Password
                className={classes.input}
                prefix={<LockOutlined className={classes.prefixIcon} />}
                placeholder={t('password')}
              />
            </Form.Item>

            <Form.Item className={classes.footer}>
              <Button
                type='primary'
                size='large'
                htmlType='submit'
                className={classes.submitButton}
                loading={loginLoading}
              >
                {t('login')}
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  )
}

export default LogInPage
