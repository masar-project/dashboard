import React, { useState } from 'react'
import { AppProps } from 'next/app'
import AppContextProvider from '../contexts/app/provider'
import AuthContextProvider from '../contexts/auth/provider'
import { useRouter } from 'next/router'
import Loader from '../components/general/loader/index'

// Antd less
import '../styles/antd.less'

// global styles
import '../styles/globals.scss'

// global styles
import '../styles/custom.less'
import SocketContextProvider from '@/contexts/socket/provider'
import TripContextProvider from '@/contexts/trip/provider'

const MyApp: React.FC<AppProps> = (props) => {
  const { Component, pageProps } = props
  const router = useRouter()
  const { pathname } = router

  const [spinning, setSpinning] = useState(false)

  return (
    <Loader spinning={false} pathName={pathname}>
      <AppContextProvider>
        <AuthContextProvider>
          <TripContextProvider>
            <SocketContextProvider>
              <Component {...pageProps} />
            </SocketContextProvider>
          </TripContextProvider>
        </AuthContextProvider>
      </AppContextProvider>
    </Loader>
  )
}

export default MyApp
