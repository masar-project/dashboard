import {
  IChangePassword,
  ILoginRequest,
  IRegisterRequest,
} from '../../models/auth/request'
import {
  IEmployeeLoginResponse,
  IRegisterResponse,
} from '../../models/auth/response'
import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'

class AuthService extends ApiService {
  constructor() {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_API_URL}/auth` })
  }

  public loginEmployee(
    request: ILoginRequest
  ): Promise<ApiResult<IEmployeeLoginResponse>> {
    return this.post('/login/employee', request)
  }

  public registerCompany(
    request: IRegisterRequest
  ): Promise<ApiResult<IRegisterResponse>> {
    return this.post('/register/company', request)
  }

  public signOut(): Promise<ApiResult<any>> {
    return this.post('/sign-out')
  }

  public changePassword(request: IChangePassword): Promise<ApiResult<any>> {
    return this.post(`/change-password`, request)
  }
}

const authService = new AuthService()

export default authService
