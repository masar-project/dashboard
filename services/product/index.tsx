import { IProductListFilter } from '../../models/product/filter'
import { IProductCreate, IProductEdit } from '../../models/product/request'
import { IProduct, IProductDetails } from '../../models/product/response'

import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class ProductService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/company/1/products`,
    })
  }

  public getAllProducts(params: {
    page: number
    perPage: number
    filter?: IProductListFilter
  }): Promise<ApiResult<IBaseListingResponse<IProduct>>> {
    return this.get('', {
      params: {
        page: params.page,
        per_page: params.perPage,
        ...params.filter,
      },
    })
  }

  public getProductDetails(id: number): Promise<ApiResult<IProductDetails>> {
    return this.get(`/${id}`)
  }

  public createProduct(request: IProductCreate): Promise<ApiResult<IProduct>> {
    return this.post('', request)
  }

  public setProductMain(id: number): Promise<ApiResult<IProduct>> {
    return this.post(`/${id}`)
  }

  public updateProduct(
    id: number,
    request: IProductEdit
  ): Promise<ApiResult<IProduct>> {
    return this.put(`/${id}`, request)
  }

  public deleteProduct(id: number): Promise<ApiResult<any>> {
    return this.delete(`/${id}`)
  }
}

const productService = new ProductService()

export default productService
