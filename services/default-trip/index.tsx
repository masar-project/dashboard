import {
  IDefaultTripListFilter,
  IPreviewTripListFilter,
} from '../../models/default-trip/filter'
import {
  IDefaultTripCreate,
  IDefaultTripEdit,
} from '../../models/default-trip/request'
import {
  IDefaultTrip,
  IDefaultTripDetails,
} from '../../models/default-trip/response'

import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class DefaultTripService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/company/1/default-trips`,
    })
  }

  public getAllTrips(params: {
    page: number
    perPage: number
    filter?: IDefaultTripListFilter
  }): Promise<ApiResult<IBaseListingResponse<IDefaultTrip>>> {
    return this.get('', {
      params: {
        page: params.page,
        per_page: params.perPage,
        ...params.filter,
      },
    })
  }

  public getTripDetails(id: number): Promise<ApiResult<IDefaultTripDetails>> {
    return this.get(`/${id}`)
  }

  public previewTrip(
    id: number,
    filter?: IPreviewTripListFilter
  ): Promise<ApiResult<IDefaultTripDetails>> {
    return this.get(`/${id}/preview`, {
      params: {
        ...filter,
      },
    })
  }

  public createTrip(
    request: IDefaultTripCreate
  ): Promise<ApiResult<IDefaultTrip>> {
    return this.post('', request)
  }

  public updateTrip(
    id: number,
    request: IDefaultTripEdit
  ): Promise<ApiResult<IDefaultTrip>> {
    return this.put(`/${id}`, request)
  }

  public deleteTrip(id: number): Promise<ApiResult<any>> {
    return this.delete(`/${id}`)
  }
}

const defaultTripService = new DefaultTripService()

export default defaultTripService
