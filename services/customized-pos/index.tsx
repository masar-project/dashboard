import { ICustomizedPOSListFilter } from '../../models/customized-pos/filter'
import {
  ICustomizedPOSCreate,
  ICustomizedPOSEdit,
} from '../../models/customized-pos/request'
import {
  ICustomizedPOS,
  ICustomizedPOSDetails,
} from '../../models/customized-pos/response'

import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class CustomizedPOSService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/company/1/customized-points`,
    })
  }

  public getAllTrips(params: {
    page: number
    perPage: number
    filter?: ICustomizedPOSListFilter
  }): Promise<ApiResult<IBaseListingResponse<ICustomizedPOS>>> {
    return this.get('', {
      params: {
        page: params.page,
        per_page: params.perPage,
        ...params.filter,
      },
    })
  }

  public getTripDetails(id: number): Promise<ApiResult<ICustomizedPOSDetails>> {
    return this.get(`/${id}`)
  }

  public createTrip(
    request: ICustomizedPOSCreate
  ): Promise<ApiResult<ICustomizedPOS>> {
    return this.post('', request)
  }

  public setTripMain(id: number): Promise<ApiResult<ICustomizedPOS>> {
    return this.post(`/${id}`)
  }

  public updateTrip(
    id: number,
    request: ICustomizedPOSEdit
  ): Promise<ApiResult<ICustomizedPOS>> {
    return this.put(`/${id}`, request)
  }

  public deleteTrip(id: number): Promise<ApiResult<any>> {
    return this.delete(`/${id}`)
  }
}

const customizedPOSService = new CustomizedPOSService()

export default customizedPOSService
