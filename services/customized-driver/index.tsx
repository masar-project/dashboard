import { ICustomizedDriverListFilter } from '../../models/customized-driver/filter'
import {
  ICustomizedDriverCreate,
  ICustomizedDriverEdit,
} from '../../models/customized-driver/request'
import {
  ICustomizedDriver,
  ICustomizedDriverDetails,
} from '../../models/customized-driver/response'
import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class CustomizedDriverService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/company/1/customized-drivers`,
    })
  }

  public getAllTrips(params: {
    page: number
    perPage: number
    filter?: ICustomizedDriverListFilter
  }): Promise<ApiResult<IBaseListingResponse<ICustomizedDriver>>> {
    return this.get('', {
      params: {
        page: params.page,
        per_page: params.perPage,
        ...params.filter,
      },
    })
  }

  public getTripDetails(
    id: number
  ): Promise<ApiResult<ICustomizedDriverDetails>> {
    return this.get(`/${id}`)
  }

  public createTrip(
    request: ICustomizedDriverCreate
  ): Promise<ApiResult<ICustomizedDriver>> {
    return this.post('', request)
  }

  public setTripMain(id: number): Promise<ApiResult<ICustomizedDriver>> {
    return this.post(`/${id}`)
  }

  public updateTrip(
    id: number,
    request: ICustomizedDriverEdit
  ): Promise<ApiResult<ICustomizedDriver>> {
    return this.put(`/${id}`, request)
  }

  public deleteTrip(id: number): Promise<ApiResult<any>> {
    return this.delete(`/${id}`)
  }
}

const customizedDriverService = new CustomizedDriverService()

export default customizedDriverService
