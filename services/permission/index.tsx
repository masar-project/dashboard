import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import IPermissionListFilter from '../../models/permission/filter'
import { IPermission } from '../../models/permission/response'

class PermissionService extends ApiService {
  constructor() {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_API_URL}/permissions` })
  }

  public getAllPermissions(params: {
    page: number
    perPage: number
    filter?: IPermissionListFilter
  }): Promise<ApiResult<IBaseListingResponse<IPermission>>> {
    return this.post('/index', {
      page: params.page,
      perPage: params.perPage,
      ...params.filter,
    })
  }
}

const permissionService = new PermissionService()

export default permissionService
