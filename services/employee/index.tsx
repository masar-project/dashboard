import { IEmployeeListFilter } from '../../models/employee/filter'
import { IEmployeeCreate, IEmployeeEdit } from '../../models/employee/request'
import { IEmployee, IEmployeeDetails } from '../../models/employee/response'

import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class EmployeeService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/company/1/employees`,
    })
  }

  public getAllEmployees(params: {
    page: number
    perPage: number
    filter?: IEmployeeListFilter
  }): Promise<ApiResult<IBaseListingResponse<IEmployee>>> {
    return this.get('', {
      params: {
        page: params.page,
        per_page: params.perPage,
        ...params.filter,
      },
    })
  }

  public getEmployeeDetails(id: number): Promise<ApiResult<IEmployeeDetails>> {
    return this.get(`/${id}`)
  }

  public createEmployee(
    request: IEmployeeCreate
  ): Promise<ApiResult<IEmployee>> {
    return this.post('', request)
  }

  public setEmployeeMain(id: number): Promise<ApiResult<IEmployee>> {
    return this.post(`/${id}`)
  }

  public updateEmployee(
    id: number,
    request: IEmployeeEdit
  ): Promise<ApiResult<IEmployee>> {
    return this.put(`/${id}`, request)
  }

  public disableEmployee(id: number): Promise<ApiResult<any>> {
    return this.post(`/${id}/disable`)
  }

  public enableEmployee(id: number): Promise<ApiResult<any>> {
    return this.post(`/${id}/enable`)
  }
}

const employeeService = new EmployeeService()

export default employeeService
