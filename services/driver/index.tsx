import { IDriverListFilter } from '../../models/driver/filter'
import { IDriverCreate, IDriverEdit } from '../../models/driver/request'
import { IDriver, IDriverDetails } from '../../models/driver/response'

import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class DriverService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/company/1/drivers`,
    })
  }

  public getAllDrivers(params: {
    page: number
    perPage: number
    filter?: IDriverListFilter
  }): Promise<ApiResult<IBaseListingResponse<IDriver>>> {
    return this.get('', {
      params: {
        page: params.page,
        per_page: params.perPage,
        ...params.filter,
      },
    })
  }

  public getDriverDetails(id: number): Promise<ApiResult<IDriverDetails>> {
    return this.get(`/${id}`)
  }

  public createDriver(request: IDriverCreate): Promise<ApiResult<IDriver>> {
    return this.post('', request)
  }

  public setDriverMain(id: number): Promise<ApiResult<IDriver>> {
    return this.post(`/${id}`)
  }

  public updateDriver(
    id: number,
    request: IDriverEdit
  ): Promise<ApiResult<IDriver>> {
    return this.put(`/${id}`, request)
  }

  public disableDriver(id: number): Promise<ApiResult<any>> {
    return this.post(`/${id}/disable`)
  }

  public enableDriver(id: number): Promise<ApiResult<any>> {
    return this.post(`/${id}/enable`)
  }
}

const driverService = new DriverService()

export default driverService
