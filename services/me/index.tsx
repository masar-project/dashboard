import { IUserEdit } from '../../models/user/request'
import { IUser } from '../../models/user/resopnse'
import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import { IEmployeeDetails } from '../../models/employee/response'

class MeService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/my-account`,
    })
  }

  public me(): Promise<ApiResult<IEmployeeDetails>> {
    return this.get('')
  }

  public updateUser(id: number, request: IUserEdit): Promise<ApiResult<IUser>> {
    return this.put('', request)
  }

  public changeLanguage(language: string): Promise<ApiResult<IUser>> {
    return this.put(`/`, { language })
  }
}

const meService = new MeService()

export default meService
