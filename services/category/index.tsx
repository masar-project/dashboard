import { ICategoryListFilter } from '@/models/category/filter'
import { ICategoryCreate, ICategoryEdit } from '@/models/category/request'
import { ICategory, ICategoryDetails } from '../../models/category/response'

import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class CategoryService extends ApiService {
  constructor() {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_API_URL}/categorys` })
  }

  public getAllCategories(params: {
    page: number
    perPage: number
    filter?: ICategoryListFilter
  }): Promise<ApiResult<IBaseListingResponse<ICategory>>> {
    return this.post('/index', {
      page: params.page,
      perPage: params.perPage,
      ...params.filter,
    })
  }

  public getCategoryDetails(id: string): Promise<ApiResult<ICategoryDetails>> {
    return this.get(`/${id}`)
  }

  public createCategory(
    request: ICategoryCreate
  ): Promise<ApiResult<ICategory>> {
    return this.post('', request)
  }

  public setCategoryMain(id: string): Promise<ApiResult<ICategory>> {
    return this.post(`/${id}`)
  }

  public updateCategory(
    id: string,
    request: ICategoryEdit
  ): Promise<ApiResult<ICategory>> {
    return this.put(`/${id}`, request)
  }

  public deleteCategory(id: string): Promise<ApiResult<any>> {
    return this.delete(`/${id}`)
  }
}

const categoryService = new CategoryService()

export default categoryService
