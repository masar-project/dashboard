import { LOGGED_USER } from '@/utils/constants'
import { IPOSListFilter } from '../../models/pos/filter'
import { IPOSCreate, IPOSEdit } from '../../models/pos/request'
import { IPOS, IPOSDetails } from '../../models/pos/response'

import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class POSService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/company/1/point-of-sale`,
    })
  }

  public getAllPOSs(params: {
    page: number
    perPage: number
    filter?: IPOSListFilter
  }): Promise<ApiResult<IBaseListingResponse<IPOS>>> {
    return this.get('', {
      params: {
        page: params.page,
        per_page: params.perPage,
        ...params.filter,
      },
    })
  }

  public getPOSDetails(id: number): Promise<ApiResult<IPOSDetails>> {
    return this.get(`/${id}`)
  }

  public createPOS(request: IPOSCreate): Promise<ApiResult<IPOS>> {
    return this.post('', request)
  }

  public updatePOS(id: number, request: IPOSEdit): Promise<ApiResult<IPOS>> {
    return this.put(`/${id}`, request)
  }

  public disablePOS(id: number): Promise<ApiResult<any>> {
    return this.post(`/${id}/disable`)
  }

  public enablePOS(id: number): Promise<ApiResult<any>> {
    return this.post(`/${id}/enable`)
  }
}

const posService = new POSService()

export default posService
