import { ILanguageListFilter } from '../../models/language/filter'
import { ILanguageCreate, ILanguageEdit } from '../../models/language/request'
import { ILanguage, ILanguageDetails } from '../../models/language/response'

import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class LanguageService extends ApiService {
  constructor() {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_API_URL}/languages` })
  }

  public getAllLanguages(params: {
    page: number
    perPage: number
    filter?: ILanguageListFilter
  }): Promise<ApiResult<IBaseListingResponse<ILanguage>>> {
    return this.post('/index', {
      page: params.page,
      perPage: params.perPage,
      ...params.filter,
    })
  }

  public getLanguageDetails(id: string): Promise<ApiResult<ILanguageDetails>> {
    return this.get(`/${id}`)
  }

  public createLanguage(
    request: ILanguageCreate
  ): Promise<ApiResult<ILanguage>> {
    return this.post('', request)
  }

  public setLanguageMain(id: string): Promise<ApiResult<ILanguage>> {
    return this.post(`/${id}`)
  }

  public updateLanguage(
    id: string,
    request: ILanguageEdit
  ): Promise<ApiResult<ILanguage>> {
    return this.put(`/${id}`, request)
  }

  public deleteLanguage(id: string): Promise<ApiResult<any>> {
    return this.delete(`/${id}`)
  }
}

const languageService = new LanguageService()

export default languageService
