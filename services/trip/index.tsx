import { ITripListFilter } from '../../models/trip/filter'
import { ITrip, ITripDetails } from '../../models/trip/response'
import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'

class TripService extends ApiService {
  constructor() {
    super({
      baseURL: `${process.env.NEXT_PUBLIC_BASE_API_DASHBOARD_URL}/company/1/trips`,
    })
  }

  public getAllTrips(params: {
    page: number
    perPage: number
    filter?: ITripListFilter
  }): Promise<ApiResult<IBaseListingResponse<ITrip>>> {
    return this.get('', {
      params: {
        page: params.page,
        per_page: params.perPage,
        ...params.filter,
      },
    })
  }

  public getTripDetails(id: number): Promise<ApiResult<ITripDetails>> {
    return this.get(`/${id}`)
  }

  public deleteTrip(id: number): Promise<ApiResult<any>> {
    return this.delete(`/${id}`)
  }
}

const tripService = new TripService()

export default tripService
