import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import IRoleListFilter from '../../models/role/filter'
import { IRole, IRoleDetails } from '../../models/role/response'
import { IRoleEdit, IRoleCreate } from '../../models/role/request'

class RoleService extends ApiService {
  constructor() {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_API_URL}/roles` })
  }

  public getAllRoles(params: {
    page: number
    perPage: number
    filter?: IRoleListFilter
  }): Promise<ApiResult<IBaseListingResponse<IRole>>> {
    return this.post('/index', {
      page: params.page,
      perPage: params.perPage,
      ...params.filter,
    })
  }

  public getRoleDetails(id: number): Promise<ApiResult<IRoleDetails>> {
    return this.get(`/${id}`)
  }

  public createRole(request: IRoleCreate): Promise<ApiResult<IRole>> {
    return this.post('', request)
  }

  public updateRole(id: number, request: IRoleEdit): Promise<ApiResult<IRole>> {
    return this.put(`/${id}`, request)
  }

  public deleteRole(id: number): Promise<ApiResult<any>> {
    return this.delete(`/${id}`)
  }
}

const roleService = new RoleService()

export default roleService
