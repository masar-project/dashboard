import ApiResult from '../../utils/api/api-result'
import ApiService from '../../utils/api/api-service'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import ICityListFilter from '../../models/city/filter'
import { ICity } from '../../models/city/response'

class CityService extends ApiService {
  constructor() {
    super({ baseURL: `${process.env.NEXT_PUBLIC_BASE_API_URL}/index/cities` })
  }

  public getAllCities(params: {
    page: number
    perPage: number
    filter?: ICityListFilter
  }): Promise<ApiResult<IBaseListingResponse<ICity>>> {
    return this.get('', {
      params: { page: params.page, perPage: params.perPage, ...params.filter },
    })
  }
}

const cityService = new CityService()

export default cityService
