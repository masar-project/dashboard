export default interface IBaseListingResponse<T> {
  data: T[]
  meta: {
    total: number
    last_page: number
  }
}
