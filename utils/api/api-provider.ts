import axios, { AxiosInstance, AxiosRequestConfig, AxiosError } from 'axios'
import RequestConfig from './request-config'
import ApiError from './api-error'
import ApiErrorType from './api-error-type'
import ApiResult from './api-result'
import { ACCESS_TOKEN, LANGUAGE_CODE } from '../constants'
import eventManager from '../event-manager/index'
import {
  EVENT_FORBIDDEN,
  EVENT_UNAOUTHORIZED,
  EVENT_ERORR_NOTIFICATION,
} from '../event-manager/index'

export default class ApiProvider {
  private api: AxiosInstance

  public constructor(config: RequestConfig) {
    this.api = axios.create(config)
    this.api.interceptors.request.use((param: AxiosRequestConfig) => ({
      ...param,
      headers: {
        ...param.headers,
        Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`,
        'Accept-Language': `${localStorage.getItem(LANGUAGE_CODE) ?? 'ar'}`,
      },
    }))
  }

  public async request<T>(config: RequestConfig): Promise<ApiResult<T>> {
    let result: T | ApiError = {
      errorType: ApiErrorType.UNKNOWN,
    }

    try {
      const response = await this.api.request<T>(config)
      result = response.data
    } catch (error) {
      result = this.handleError(error)
    } finally {
      return result
    }
  }

  private handleError(error: AxiosError): ApiError {
    if (error.response) {
      // The request was made and the server responded with an error status code.
      const message: string = `${error.response.data.message}`
      let type: ApiErrorType
      switch (error.response?.status) {
        case 400:
          type = ApiErrorType.BAD_REQUEST
          break
        case 401:
          type = ApiErrorType.UNAUTHORIZED
          eventManager.emit(EVENT_UNAOUTHORIZED)
          break
        case 403:
          type = ApiErrorType.FORBIDDEN
          eventManager.emit(EVENT_FORBIDDEN)
          break
        case 404:
          type = ApiErrorType.NOT_FOUND
          break
        case 409:
          type = ApiErrorType.CONFLICT
          break
        case 500:
          type = ApiErrorType.INTERNAL_SERVER_ERROR
          break
        default:
          type = ApiErrorType.UNKNOWN
          break
      }
      // Display Error
      eventManager.emit(EVENT_ERORR_NOTIFICATION, [message])
      return { errorType: type, message }
    } else if (error.request) {
      // The request was made but no response was received.
      // Display Error
      eventManager.emit(EVENT_ERORR_NOTIFICATION, ['something_wrong_message'])
      return { errorType: ApiErrorType.CONNECTION }
    } else {
      // Display Error
      eventManager.emit(EVENT_ERORR_NOTIFICATION, ['something_wrong_message'])
      return { errorType: ApiErrorType.UNKNOWN }
    }
  }
}
