import imageCompression from 'browser-image-compression'

export const compressImage: (file: any) => Promise<any> = async (file) => {
  const options = {
    maxSizeMB: 1,
    maxWidthOrHeight: 1920,
    useWebWorker: true,
    onProgress: (p) => {
      // console.log('onProgress: ', p)
    },
  }

  return await imageCompression(file, options)
}
