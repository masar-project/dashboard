import { notification } from 'antd'
export type notificationType = 'success' | 'info' | 'warning' | 'error'

export const Notification = (
  type: notificationType,
  message: string,
  description?: string
) => {
  return notification[type]({
    message: message,
    description: description ?? '',
    placement: 'topLeft',
    duration: 3,
  })
}
