import { notification } from 'antd'

export const errorNotification = (message: string, description?: string) => {
  return notification['error']({
    message: message,
    description: description ?? '',
    placement: 'topRight',
    duration: 2,
  })
}
