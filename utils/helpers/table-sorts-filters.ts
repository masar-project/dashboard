import { FilterValue, SorterResult } from 'antd/lib/table/interface'
import { isArray } from 'lodash'
import { ReactText } from 'react'
import { ISort, IStaticFilter } from '../../models/filter'
import { Orders } from '../../models/filter/enum'

///
// Handle Sorts and Filters in Antd Table
///
export const tableOnChange = (
  tableFilters: Record<string, FilterValue>,
  tableFiltersProps: Object,
  setStaticFilters: (filters: IStaticFilter[]) => void
) => {
  // Filters
  let filters: IStaticFilter[] = []
  Object.values(tableFilters).map((value, index) => {
    let key = `${Object.keys(tableFilters)[index]}`
    filters.push({
      name: key,
      operation: tableFiltersProps[key]?.operation,
      value: value?.map((v) => +v as number),
    })
  })
  setStaticFilters(filters)
}
