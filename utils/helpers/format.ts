import moment from 'moment'
import { DATE_FORMATE } from '../constants'

export const currencyFormat = (price: number, currency?: string) => {
  if (currency) return `${new Intl.NumberFormat().format(price)} ${currency}`
  return `${new Intl.NumberFormat().format(price)}`
}

export const formatDate = (date: Date, format: string = DATE_FORMATE) => {
  return date && moment(date)?.format(format)
}
