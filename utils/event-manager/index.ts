import { EventEmitter } from 'events'

const eventManager = new EventEmitter()

export default eventManager

export const EVENT_UNAOUTHORIZED = 'event-unauthorized'
export const EVENT_FORBIDDEN = 'event-forbidden'
export const EVENT_ERORR_NOTIFICATION = 'error-notification'
export const EVENT_MESSAGES_SCROLL_TO_BOTTOM = 'messages-scroll-to-bottom'