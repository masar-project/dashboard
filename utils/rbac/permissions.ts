///
/// Permissions
///

// Home Page
export const CAN_VIEW_HOME_PAGE = 'can-view-home-page'
export const CAN_VIEW_DASHBOARD = 'can-view-dashboard'
export const CAN_VIEW_MY_DASHBOARD = 'can-view-my-dashboard'

// Language Permissions
export const CAN_VIEW_LANGUAGES = 'can-view-languages'
export const CAN_CREATE_LANGUAGE = 'can-create-language'
export const CAN_UPDATE_LANGUAGE = 'can-update-language'
export const CAN_DELETE_LANGUAGE = 'can-delete-language'

// Medium Permissions
export const CAN_CREATE_MEDIUM = 'can-create-medium'

// Roles Permissions
export const CAN_VIEW_ROLES = 'can-view-roles'
export const CAN_CREATE_ROLE = 'can-create-role'
export const CAN_UPDATE_ROLE = 'can-update-role'
export const CAN_DELETE_ROLE = 'can-delete-role'
export const CAN_VIEW_PERMISSIONS = 'can-view-permissions'

// Users Permissions
export const CAN_VIEW_USERS = 'can-view-users'
export const CAN_CREATE_USER = 'can-create-user'
export const CAN_UPDATE_USER = 'can-update-user'
export const CAN_DELETE_USER = 'can-delete-user'
export const CAN_VIEW_USERS_WITH_TRASHED = 'can-view-users-with-trashed'
export const CAN_RESTORE_USER = 'can-restore-user'
