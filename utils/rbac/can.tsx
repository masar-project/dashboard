import React, { useContext } from 'react'
import AuthContext from '../../contexts/auth/context'
import { isArray } from 'lodash'
import Spinner from '../../components/general/spinner'

export const check = (
  grantedPermissions: string[],
  action: string | string[]
) => {
  if (grantedPermissions.length === 0) return false

  if (isArray(action)) {
    if (grantedPermissions.find((p) => action.includes(p))) return true
  } else {
    if (grantedPermissions.find((p) => p === action)) return true
  }

  return false
}

interface ICanProps {
  perform: string | string[]
  yes?: () => JSX.Element
  no?: () => JSX.Element
}

const Can: React.FC<ICanProps> = (props) => {
  const { grantedPermissions, employeeDetails: userDetails } =
    useContext(AuthContext)

  const defaultFunction = () => null

  const yes = props.yes ?? defaultFunction
  const no = props.no ?? defaultFunction

  if (!grantedPermissions || !userDetails) return <Spinner />

  return check([...grantedPermissions], props.perform) ? yes() : no()
}

export default Can
