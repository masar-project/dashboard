const withSass = require('@zeit/next-sass')
const withLess = require('@zeit/next-less')
const withCSS = require('@zeit/next-css')

const isProd = process.env.NODE_ENV === 'production'

// fix: prevents error when .less files are required by node
if (typeof require !== 'undefined') {
  require.extensions['.less'] = (file) => {}
}

module.exports = {
  trailingSlash: true,
  reactStrictMode: true,
  // basePath: '/admin',
  // assetPrefix: isProd ? 'http://213.239.205.241/admin' : '',
  ...withCSS({
    cssModules: true,
    cssLoaderOptions: {
      importLoaders: 1,
      localIdentName: '[local]___[hash:base64:5]',
    },
    ...withSass({
      cssModules: true,
      ...withLess({
        lessLoaderOptions: {
          javascriptEnabled: true,
        },
      }),
    }),
  }),
}
