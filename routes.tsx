import React from 'react'
import {
  HomeOutlined,
  CompassOutlined,
  TableOutlined,
  UserOutlined,
  PushpinOutlined,
  ShoppingOutlined,
} from '@ant-design/icons'
import { CAN_VIEW_HOME_PAGE } from './utils/rbac/permissions'

export interface ISubRoute {
  labelKey: string
  icon?: JSX.Element
  path: string
  permissions: string[]
}
export interface IRoute {
  labelKey: string
  icon: JSX.Element
  path: string

  hasSubMenus: boolean
  subMenus?: ISubRoute[]

  permissions: string[]
}

export const routes: IRoute[] = [
  {
    labelKey: 'home',
    icon: <HomeOutlined />,
    path: '/',
    hasSubMenus: false,
    permissions: [CAN_VIEW_HOME_PAGE],
  },
]

export const horizontalRoutes: IRoute[] = [
  {
    labelKey: 'map',
    icon: <CompassOutlined />,
    path: '/dashboard/manage',
    hasSubMenus: false,
    permissions: [CAN_VIEW_HOME_PAGE],
  },
  // {
  //   labelKey: 'table',
  //   icon: <TableOutlined />,
  //   path: '/dashboard/table',
  //   hasSubMenus: false,
  //   permissions: [CAN_VIEW_HOME_PAGE],
  // },
  // {
  //   labelKey: 'settings',
  //   icon: <SettingOutlined />,
  //   path: '/dashboard/settings/drivers',
  //   hasSubMenus: false,
  //   permissions: [CAN_VIEW_HOME_PAGE],
  // },
]

export const settingsRoutes: IRoute[] = [
  {
    labelKey: 'employees',
    icon: <UserOutlined />,
    path: '/dashboard/settings/employees',
    hasSubMenus: false,
    permissions: [CAN_VIEW_HOME_PAGE],
  },
  {
    labelKey: 'drivers',
    icon: <CompassOutlined />,
    path: '/dashboard/settings/drivers',
    hasSubMenus: false,
    permissions: [CAN_VIEW_HOME_PAGE],
  },
  {
    labelKey: 'points',
    icon: <PushpinOutlined />,
    path: '/dashboard/settings/points',
    hasSubMenus: false,
    permissions: [CAN_VIEW_HOME_PAGE],
  },
  {
    labelKey: 'default_trips',
    icon: <CompassOutlined />,
    path: '/dashboard/settings/default-trips',
    hasSubMenus: false,
    permissions: [CAN_VIEW_HOME_PAGE],
  },
  {
    labelKey: 'products',
    icon: <ShoppingOutlined />,
    path: '/dashboard/settings/products',
    hasSubMenus: false,
    permissions: [CAN_VIEW_HOME_PAGE],
  },
]
