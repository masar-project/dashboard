import { ILanguage } from './../../models/language/response'
import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import Direction from '../../utils/ui/direction'
import ScreenSize from '../../utils/ui/screen-size'
import { IInternalState } from './context'
import { ICity } from '../../models/city/response'
import { IDriver } from '../../models/driver/response'
import { ICategory } from '../../models/category/response'

type Action =
  | { type: 'SET_DIRECTION'; payload: { direction: Direction } }
  | { type: 'SET_SCREEN_SIZE'; payload: { screenSize: ScreenSize } }
  | { type: 'SET_LOCALE'; payload: { locale: any } }
  | { type: 'LANGUAGES_LOADING' }
  | {
      type: 'LANGUAGES_SUCCESS'
      payload: { languages: IBaseListingResponse<ILanguage> }
    }
  | { type: 'LANGUAGES_ERROR'; payload: { error: ApiError } }
  | { type: 'CITIES_LOADING' }
  | {
      type: 'CITIES_SUCCESS'
      payload: { cities: IBaseListingResponse<ICity> }
    }
  | { type: 'CITIES_ERROR'; payload: { error: ApiError } }
  | { type: 'DRIVERS_LOADING' }
  | {
      type: 'DRIVERS_SUCCESS'
      payload: { drivers: IBaseListingResponse<IDriver> }
    }
  | { type: 'DRIVERS_ERROR'; payload: { error: ApiError } }
  | { type: 'CATEGORIES_LOADING' }
  | {
      type: 'CATEGORIES_SUCCESS'
      payload: { categories: IBaseListingResponse<ICategory> }
    }
  | { type: 'CATEGORIES_ERROR'; payload: { error: ApiError } }

const reducer = (state: IInternalState, action: Action): IInternalState => {
  switch (action.type) {
    case 'SET_DIRECTION':
      return { ...state, direction: action.payload.direction }
    case 'SET_SCREEN_SIZE':
      return { ...state, screenSize: action.payload.screenSize }
    case 'SET_LOCALE':
      return { ...state, locale: action.payload.locale }
    case 'LANGUAGES_LOADING':
      return { ...state, loading: true, error: undefined }
    case 'LANGUAGES_SUCCESS':
      return {
        ...state,
        loading: false,
        languages: action.payload.languages,
      }
    case 'LANGUAGES_ERROR':
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      }
    case 'CITIES_LOADING':
      return { ...state, loading: true, error: undefined }
    case 'CITIES_SUCCESS':
      return {
        ...state,
        loading: false,
        cities: action.payload.cities,
      }
    case 'CITIES_ERROR':
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      }
    case 'DRIVERS_LOADING':
      return { ...state, loading: true, error: undefined }
    case 'DRIVERS_SUCCESS':
      return {
        ...state,
        loading: false,
        drivers: action.payload.drivers,
      }
    case 'DRIVERS_ERROR':
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      }
    case 'CATEGORIES_LOADING':
      return { ...state, loading: true, error: undefined }
    case 'CATEGORIES_SUCCESS':
      return {
        ...state,
        loading: false,
        categories: action.payload.categories,
      }
    case 'CATEGORIES_ERROR':
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      }
    default:
      return state
  }
}

export default reducer
