/* eslint-disable react-hooks/exhaustive-deps */
import { ConfigProvider } from 'antd'
import en from '../../utils/antd/en-locale'
import ar from '../../utils/antd/ar-locale'
import React, { useEffect, useReducer } from 'react'
import AppContext, { internalState } from './context'
import reducer from './reducer'
import { useMediaQuery } from 'react-responsive'
import ScreenSize from '../../utils/ui/screen-size'
import { I18nextProvider, useTranslation } from 'react-i18next'
import i18n from '../../utils/localization/i18n'
import { DEFAULT_MAXIMUM_PAGE_SIZE } from '../constants'
import { isError } from '../../utils/api/api-result'
import eventManager, {
  EVENT_ERORR_NOTIFICATION,
} from '../../utils/event-manager'
import { errorNotification } from '../../utils/helpers/error-notification'
import languageService from '../../services/language/index'
import { ACCESS_TOKEN } from '../../utils/constants'
import { useRouter } from 'next/router'
import cityService from '@/services/city'
import driverService from '../../services/driver/index'
import categoryService from '../../services/category/index'

const AppContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const router = useRouter()

  const {
    t,
    i18n: { language },
  } = useTranslation()

  const isLaptopOrDesktop = useMediaQuery({
    minWidth: 992,
  })

  const isMobileOrTablet = useMediaQuery({
    maxWidth: 992,
  })

  // Catch Errors
  useEffect(() => {
    eventManager.on(EVENT_ERORR_NOTIFICATION, (message) => {
      errorNotification(t(message))
    })

    if (!localStorage.getItem(ACCESS_TOKEN)) {
      router.replace('/login')
    }
  }, [])

  useEffect(() => {
    const direction = language === 'ar' ? 'rtl' : 'ltr'
    dispatch({ type: 'SET_DIRECTION', payload: { direction } })

    const locale = language === 'ar' ? ar : en
    dispatch({ type: 'SET_LOCALE', payload: { locale } })
  }, [language])

  useEffect(() => {
    let screenSize: ScreenSize

    if (isLaptopOrDesktop) screenSize = 'laptopOrDesktop'
    else if (isMobileOrTablet) screenSize = 'mobileOrTablet'

    dispatch({
      type: 'SET_SCREEN_SIZE',
      payload: { screenSize },
    })
  }, [isLaptopOrDesktop, isMobileOrTablet])

  // Get all languages
  const getLanguages = async () => {
    dispatch({ type: 'LANGUAGES_LOADING' })

    const result = await languageService.getAllLanguages({
      page: 1,
      perPage: DEFAULT_MAXIMUM_PAGE_SIZE,
    })

    if (isError(result)) {
      dispatch({ type: 'LANGUAGES_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'LANGUAGES_SUCCESS', payload: { languages: result } })
  }

  // Get all cities
  const getCities = async () => {
    dispatch({ type: 'CITIES_LOADING' })

    const result = await cityService.getAllCities({
      page: 1,
      perPage: DEFAULT_MAXIMUM_PAGE_SIZE,
    })

    if (isError(result)) {
      dispatch({ type: 'CITIES_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'CITIES_SUCCESS', payload: { cities: result } })
  }

  // Getall drivers
  const getDrivers = async () => {
    dispatch({ type: 'DRIVERS_LOADING' })

    const result = await driverService.getAllDrivers({
      page: 1,
      perPage: DEFAULT_MAXIMUM_PAGE_SIZE,
    })

    if (isError(result)) {
      dispatch({ type: 'DRIVERS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DRIVERS_SUCCESS', payload: { drivers: result } })
  }

  // Getall drivers
  const getCategories = async () => {
    dispatch({ type: 'CATEGORIES_LOADING' })

    const result = await categoryService.getAllCategories({
      page: 1,
      perPage: DEFAULT_MAXIMUM_PAGE_SIZE,
    })

    if (isError(result)) {
      dispatch({ type: 'CATEGORIES_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'CATEGORIES_SUCCESS', payload: { categories: result } })
  }

  return (
    <I18nextProvider i18n={i18n}>
      <ConfigProvider {...state}>
        <AppContext.Provider
          value={{
            ...state,
            actions: {
              getLanguages,
              getCities,
              getDrivers,
              getCategories,
            },
          }}
        >
          {props.children}
        </AppContext.Provider>
      </ConfigProvider>
    </I18nextProvider>
  )
}

export default AppContextProvider
