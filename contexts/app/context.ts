import { createContext } from 'react'
import ar from '../../utils/antd/ar-locale'
import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import Direction from '../../utils/ui/direction'
import ScreenSize from '../../utils/ui/screen-size'
import { DEFAULT_FUNCTION } from '../constants'
import { ILanguage } from '../../models/language/response'
import { ICity } from '../../models/city/response'
import { IDriver } from '../../models/driver/response'
import { ICategory } from '../../models/category/response'

export interface IInternalState {
  direction: Direction
  screenSize: ScreenSize
  locale: any
  loading?: boolean
  error?: ApiError

  languages?: IBaseListingResponse<ILanguage>
  cities?: IBaseListingResponse<ICity>
  drivers?: IBaseListingResponse<IDriver>
  categories?: IBaseListingResponse<ICategory>
}

export const internalState: IInternalState = {
  direction: 'rtl',
  screenSize: 'laptopOrDesktop',
  locale: ar,
}

export interface IExternalState extends IInternalState {
  actions: {
    getLanguages: () => void
    getCities: () => void
    getDrivers: () => void
    getCategories: () => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getLanguages: DEFAULT_FUNCTION,
    getCities: DEFAULT_FUNCTION,
    getDrivers: DEFAULT_FUNCTION,
    getCategories: DEFAULT_FUNCTION,
  },
}

const AppContext = createContext(externalState)

export default AppContext
