import { createContext } from 'react'
import ApiError from '../../../utils/api/api-error'
import IBaseListingResponse from '../../../utils/api/base-listing-response'
import {
  DEFAULT_FUNCTION,
  INITIAL_PAGE,
  DEFAULT_MAXIMUM_PAGE_SIZE,
} from '../../constants'
import { IPermission } from '../../../models/permission/response'
import IPermissionListFilter from '../../../models/permission/filter'

export interface IInternalState {
  data?: IBaseListingResponse<IPermission>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number

  filter?: IPermissionListFilter
  error?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_MAXIMUM_PAGE_SIZE,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: IPermissionListFilter) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
  },
}

const PermissionSelectOptionsContext = createContext(externalState)

export default PermissionSelectOptionsContext
