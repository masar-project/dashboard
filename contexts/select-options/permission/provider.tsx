/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useReducer } from 'react'

import { isError } from '../../../utils/api/api-result'
import reducer from './reducer'
import PermissionSelectOptionsContext, { internalState } from './context'
import IPermissionListFilter from '../../../models/permission/filter'
import permissionService from '../../../services/permission'

const PermissionSelectOptionsContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)

  useEffect(() => {
    getData()
  }, [state.pageSize, state.page])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await permissionService.getAllPermissions({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: IPermissionListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  return (
    <PermissionSelectOptionsContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
        },
      }}
    >
      {props.children}
    </PermissionSelectOptionsContext.Provider>
  )
}

export default PermissionSelectOptionsContextProvider
