import IBaseListingResponse from '../../../utils/api/base-listing-response'
import { IInternalState } from './context'
import { INITIAL_PAGE } from '../../constants'
import ApiError from '../../../utils/api/api-error'
import { IPermission } from '../../../models/permission/response'
import IPermissionListFilter from '../../../models/permission/filter'

type Action =
  | { type: 'LOADING' }
  | { type: 'ACTION_LOADING' }
  | { type: 'SUCCESS'; payload: { data: IBaseListingResponse<IPermission> } }
  | { type: 'ERROR'; payload: { error: ApiError } }
  | { type: 'SET_PAGE'; payload: { page: number } }
  | { type: 'SET_PAGE_SIZE'; payload: { pageSize: number } }
  | { type: 'SET_FILTER'; payload: { filter?: IPermissionListFilter } }

const reducer = (state: IInternalState, action: Action): IInternalState => {
  switch (action.type) {
    case 'LOADING':
      return { ...state, loading: true, error: undefined }
    case 'ACTION_LOADING':
      return { ...state, actionLoading: !state.actionLoading }
    case 'SUCCESS':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        data: action.payload.data,
      }
    case 'ERROR':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        error: action.payload.error,
      }
    case 'SET_PAGE':
      return { ...state, page: action.payload.page }
    case 'SET_PAGE_SIZE':
      return { ...state, pageSize: action.payload.pageSize }
    case 'SET_FILTER':
      return { ...state, filter: action.payload.filter, page: INITIAL_PAGE }
    default:
      return state
  }
}

export default reducer
