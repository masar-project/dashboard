import React, { useEffect, useReducer } from 'react'
import { isError } from '../../utils/api/api-result'
import reducer from './reducer'
import EmployeeContext, { internalState } from './context'
import { IEmployeeListFilter } from '../../models/employee/filter'
import { IEmployeeCreate, IEmployeeEdit } from '../../models/employee/request'
import employeeService from '../../services/employee'
import { useIsMount } from '../../utils/hooks/is-mount'

const EmployeeContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const isMount = useIsMount()

  useEffect(() => {
    !isMount && getData()
  }, [state.page, state.pageSize, state.filter])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await employeeService.getAllEmployees({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: IEmployeeListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  const getDetails = async (id: number) => {
    dispatch({ type: 'DETAILS_LOADING' })

    const result = await employeeService.getEmployeeDetails(id)

    if (isError(result)) {
      dispatch({ type: 'DETAILS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DETAILS_SUCCESS', payload: { data: result } })
  }

  const createEmployee = async (request: IEmployeeCreate) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await employeeService.createEmployee(request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const updateEmployee = async (id: number, request: IEmployeeEdit) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await employeeService.updateEmployee(id, request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const disableEmployee = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await employeeService.disableEmployee(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }

    getData()
  }

  const enableEmployee = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await employeeService.enableEmployee(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }
    getData()
  }

  return (
    <EmployeeContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
          getDetails,
          createEmployee,
          updateEmployee,
          disableEmployee,
          enableEmployee,
        },
      }}
    >
      {props.children}
    </EmployeeContext.Provider>
  )
}

export default EmployeeContextProvider
