import { createContext } from 'react'

import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import {
  DEFAULT_FUNCTION,
  DEFAULT_PAGE_SIZE,
  INITIAL_PAGE,
  DEFAULT_FILTER,
} from '../constants'

import { IEmployeeListFilter } from '../../models/employee/filter'
import { IEmployeeCreate, IEmployeeEdit } from '../../models/employee/request'
import { IEmployee, IEmployeeDetails } from '../../models/employee/response'

export interface IInternalState {
  data?: IBaseListingResponse<IEmployee>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: IEmployeeListFilter
  error?: ApiError

  details?: IEmployeeDetails
  detailsLoading?: boolean
  detailsError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
  filter: DEFAULT_FILTER,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: IEmployeeListFilter) => void
    getDetails: (id: number) => void
    createEmployee: (request: IEmployeeCreate) => Promise<Boolean>
    updateEmployee: (id: number, request: IEmployeeEdit) => Promise<Boolean>
    disableEmployee: (id: number) => void
    enableEmployee: (id: number) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    createEmployee: DEFAULT_FUNCTION,
    updateEmployee: DEFAULT_FUNCTION,
    disableEmployee: DEFAULT_FUNCTION,
    enableEmployee: DEFAULT_FUNCTION,
  },
}

const EmployeeContext = createContext(externalState)

export default EmployeeContext
