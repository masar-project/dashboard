import { createContext } from 'react'

import ApiError from '@/utils/api/api-error'
import IBaseListingResponse from '@/utils/api/base-listing-response'
import {
  DEFAULT_FUNCTION,
  DEFAULT_PAGE_SIZE,
  INITIAL_PAGE,
  DEFAULT_FILTER,
} from '../constants'

import { IDriverListFilter } from '@/models/driver/filter'
import { IDriverCreate, IDriverEdit } from '@/models/driver/request'
import { IDriver, IDriverDetails } from '@/models/driver/response'

export interface IInternalState {
  data?: IBaseListingResponse<IDriver>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: IDriverListFilter
  error?: ApiError

  details?: IDriverDetails
  detailsLoading?: boolean
  detailsError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
  filter: DEFAULT_FILTER,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: IDriverListFilter) => void
    getDetails: (id: number) => void
    createDriver: (request: IDriverCreate) => Promise<Boolean>
    updateDriver: (id: number, request: IDriverEdit) => Promise<Boolean>
    disableDriver: (id: number) => void
    enableDriver: (id: number) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    createDriver: DEFAULT_FUNCTION,
    updateDriver: DEFAULT_FUNCTION,
    disableDriver: DEFAULT_FUNCTION,
    enableDriver: DEFAULT_FUNCTION,
  },
}

const DriverContext = createContext(externalState)

export default DriverContext
