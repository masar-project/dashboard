import React, { useContext, useEffect, useReducer } from 'react'
import { isError } from '../../utils/api/api-result'
import reducer from './reducer'
import DriverContext, { internalState } from './context'
import { IDriverListFilter } from '../../models/driver/filter'
import { IDriverCreate, IDriverEdit } from '../../models/driver/request'
import driverService from '../../services/driver'
import { useIsMount } from '../../utils/hooks/is-mount'
import AppContext from '../app/context'

const DriverContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const isMount = useIsMount()
  const { actions: appActions } = useContext(AppContext)

  useEffect(() => {
    !isMount && getData()
  }, [state.page, state.pageSize, state.filter])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await driverService.getAllDrivers({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: IDriverListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  const getDetails = async (id: number) => {
    dispatch({ type: 'DETAILS_LOADING' })

    const result = await driverService.getDriverDetails(id)

    if (isError(result)) {
      dispatch({ type: 'DETAILS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DETAILS_SUCCESS', payload: { data: result } })
  }

  const createDriver = async (request: IDriverCreate) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await driverService.createDriver(request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    appActions?.getDrivers()
    return true
  }

  const updateDriver = async (id: number, request: IDriverEdit) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await driverService.updateDriver(id, request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    appActions?.getDrivers()
    return true
  }

  const disableDriver = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await driverService.disableDriver(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }

    getData()
    appActions?.getDrivers()
  }

  const enableDriver = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await driverService.enableDriver(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }
    getData()
    appActions?.getDrivers()
  }

  return (
    <DriverContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
          getDetails,
          createDriver,
          updateDriver,
          disableDriver,
          enableDriver,
        },
      }}
    >
      {props.children}
    </DriverContext.Provider>
  )
}

export default DriverContextProvider
