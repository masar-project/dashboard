import React, { useEffect, useReducer } from 'react'
import { isError } from '../../utils/api/api-result'
import reducer from './reducer'
import DefaultTripContext, { internalState } from './context'
import {
  IDefaultTripListFilter,
  IPreviewTripListFilter,
} from '../../models/default-trip/filter'
import {
  IDefaultTripCreate,
  IDefaultTripEdit,
} from '../../models/default-trip/request'
import { useIsMount } from '../../utils/hooks/is-mount'
import defaultTripService from '@/services/default-trip'

const DefaultTripContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const isMount = useIsMount()

  useEffect(() => {
    !isMount && getData()
  }, [state.page, state.pageSize, state.filter])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await defaultTripService.getAllTrips({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: IDefaultTripListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  const getDetails = async (id: number) => {
    dispatch({ type: 'DETAILS_LOADING' })

    const result = await defaultTripService.getTripDetails(id)

    if (isError(result)) {
      dispatch({ type: 'DETAILS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DETAILS_SUCCESS', payload: { data: result } })
  }

  const previewTrip = async (id: number, filter: IPreviewTripListFilter) => {
    dispatch({ type: 'PREVIEW_TRIP_LOADING' })

    const result = await defaultTripService.previewTrip(id, filter)

    if (isError(result)) {
      dispatch({ type: 'PREVIEW_TRIP_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'PREVIEW_TRIP_SUCCESS', payload: { data: result } })
  }

  const createDefaultTrip = async (request: IDefaultTripCreate) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await defaultTripService.createTrip(request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const updateDefaultTrip = async (id: number, request: IDefaultTripEdit) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await defaultTripService.updateTrip(id, request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const deleteDefaultTrip = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await defaultTripService.deleteTrip(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }

    getData()
  }

  return (
    <DefaultTripContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
          getDetails,
          previewTrip,
          createDefaultTrip,
          updateDefaultTrip,
          deleteDefaultTrip,
        },
      }}
    >
      {props.children}
    </DefaultTripContext.Provider>
  )
}

export default DefaultTripContextProvider
