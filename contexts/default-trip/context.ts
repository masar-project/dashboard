import { IDefaultTripListFilter } from '@/models/default-trip/filter'
import {
  IDefaultTripCreate,
  IDefaultTripEdit,
} from '@/models/default-trip/request'
import {
  IDefaultTrip,
  IDefaultTripDetails,
  IPreviewTrip,
} from '@/models/default-trip/response'
import { createContext } from 'react'

import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import { IPreviewTripListFilter } from '../../models/default-trip/filter'
import {
  DEFAULT_FUNCTION,
  DEFAULT_PAGE_SIZE,
  INITIAL_PAGE,
  DEFAULT_FILTER,
} from '../constants'

export interface IInternalState {
  data?: IBaseListingResponse<IDefaultTrip>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: IDefaultTripListFilter
  error?: ApiError

  details?: IDefaultTripDetails
  detailsLoading?: boolean
  detailsError?: ApiError

  previewTrip?: IPreviewTrip
  previewTripLoading?: boolean
  previewTripError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
  filter: DEFAULT_FILTER,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: IDefaultTripListFilter) => void
    getDetails: (id: number) => void
    previewTrip: (id: number, filter: IPreviewTripListFilter) => void
    createDefaultTrip: (request: IDefaultTripCreate) => Promise<Boolean>
    updateDefaultTrip: (
      id: number,
      request: IDefaultTripEdit
    ) => Promise<Boolean>
    deleteDefaultTrip: (id: number) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    previewTrip: DEFAULT_FUNCTION,
    createDefaultTrip: DEFAULT_FUNCTION,
    updateDefaultTrip: DEFAULT_FUNCTION,
    deleteDefaultTrip: DEFAULT_FUNCTION,
  },
}

const DefaultTripContext = createContext(externalState)

export default DefaultTripContext
