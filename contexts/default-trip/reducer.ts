import IBaseListingResponse from '../../utils/api/base-listing-response'
import { IInternalState } from './context'
import { INITIAL_PAGE } from '../constants'
import ApiError from '../../utils/api/api-error'
import {
  IDefaultTrip,
  IDefaultTripDetails,
  IPreviewTrip,
} from '@/models/default-trip/response'
import { IDefaultTripListFilter } from '@/models/default-trip/filter'

type Action =
  | { type: 'LOADING' }
  | { type: 'ACTION_LOADING' }
  | { type: 'SUCCESS'; payload: { data: IBaseListingResponse<IDefaultTrip> } }
  | { type: 'ERROR'; payload: { error: ApiError } }
  | { type: 'SET_PAGE'; payload: { page: number } }
  | { type: 'SET_PAGE_SIZE'; payload: { pageSize: number } }
  | { type: 'SET_FILTER'; payload: { filter?: IDefaultTripListFilter } }
  | { type: 'DETAILS_LOADING' }
  | { type: 'DETAILS_SUCCESS'; payload: { data: IDefaultTripDetails } }
  | { type: 'DETAILS_ERROR'; payload: { error: ApiError } }
  | { type: 'PREVIEW_TRIP_LOADING' }
  | { type: 'PREVIEW_TRIP_SUCCESS'; payload: { data: IPreviewTrip } }
  | { type: 'PREVIEW_TRIP_ERROR'; payload: { error: ApiError } }

const reducer = (state: IInternalState, action: Action): IInternalState => {
  switch (action.type) {
    case 'LOADING':
      return { ...state, loading: true, error: undefined }
    case 'ACTION_LOADING':
      return { ...state, actionLoading: !state.actionLoading }
    case 'SUCCESS':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        data: action.payload.data,
      }
    case 'ERROR':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        error: action.payload.error,
      }
    case 'SET_PAGE':
      return { ...state, page: action.payload.page }
    case 'SET_PAGE_SIZE':
      return { ...state, pageSize: action.payload.pageSize }
    case 'SET_FILTER':
      return { ...state, filter: action.payload.filter, page: INITIAL_PAGE }
    case 'DETAILS_LOADING':
      return { ...state, detailsLoading: true, detailsError: undefined }
    case 'DETAILS_SUCCESS':
      return {
        ...state,
        detailsLoading: false,
        actionLoading: false,
        details: action.payload.data,
      }
    case 'DETAILS_ERROR':
      return {
        ...state,
        detailsLoading: false,
        actionLoading: false,
        detailsError: action.payload.error,
      }
    case 'PREVIEW_TRIP_LOADING':
      return { ...state, previewTripLoading: true, previewTripError: undefined }
    case 'PREVIEW_TRIP_SUCCESS':
      return {
        ...state,
        previewTripLoading: false,
        actionLoading: false,
        previewTrip: action.payload.data,
      }
    case 'PREVIEW_TRIP_ERROR':
      return {
        ...state,
        previewTripLoading: false,
        actionLoading: false,
        previewTripError: action.payload.error,
      }
    default:
      return state
  }
}

export default reducer
