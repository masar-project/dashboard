import { ITripListFilter } from '@/models/trip/filter'

import { ITrip, ITripDetails } from '@/models/trip/response'
import { createContext } from 'react'

import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import {
  DEFAULT_FUNCTION,
  DEFAULT_PAGE_SIZE,
  INITIAL_PAGE,
  DEFAULT_FILTER,
} from '../constants'

export interface IInternalState {
  data?: IBaseListingResponse<ITrip>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: ITripListFilter
  error?: ApiError

  details?: ITripDetails
  detailsLoading?: boolean
  detailsError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
  filter: DEFAULT_FILTER,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => Promise<Boolean>
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: ITripListFilter) => void
    getDetails: (id: number) => void
    deleteTrip: (id: number) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    deleteTrip: DEFAULT_FUNCTION,
  },
}

const TripContext = createContext(externalState)

export default TripContext
