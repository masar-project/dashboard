import { ICustomizedDriverListFilter } from '@/models/customized-driver/filter'
import {
  ICustomizedDriverCreate,
  ICustomizedDriverEdit,
} from '@/models/customized-driver/request'
import {
  ICustomizedDriver,
  ICustomizedDriverDetails,
} from '@/models/customized-driver/response'
import { createContext } from 'react'

import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import {
  DEFAULT_FUNCTION,
  DEFAULT_PAGE_SIZE,
  INITIAL_PAGE,
  DEFAULT_FILTER,
} from '../constants'

export interface IInternalState {
  data?: IBaseListingResponse<ICustomizedDriver>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: ICustomizedDriverListFilter
  error?: ApiError

  details?: ICustomizedDriverDetails
  detailsLoading?: boolean
  detailsError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
  filter: DEFAULT_FILTER,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: ICustomizedDriverListFilter) => void
    getDetails: (id: number) => void
    createCustomizedDriver: (
      request: ICustomizedDriverCreate
    ) => Promise<Boolean>
    updateCustomizedDriver: (
      id: number,
      request: ICustomizedDriverEdit
    ) => Promise<Boolean>
    deleteCustomizedDriver: (id: number) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    createCustomizedDriver: DEFAULT_FUNCTION,
    updateCustomizedDriver: DEFAULT_FUNCTION,
    deleteCustomizedDriver: DEFAULT_FUNCTION,
  },
}

const CustomizedDriverContext = createContext(externalState)

export default CustomizedDriverContext
