import React, { useEffect, useReducer } from 'react'
import { isError } from '../../utils/api/api-result'
import reducer from './reducer'
import CustomizedDriverContext, { internalState } from './context'
import { ICustomizedDriverListFilter } from '../../models/customized-driver/filter'
import {
  ICustomizedDriverCreate,
  ICustomizedDriverEdit,
} from '../../models/customized-driver/request'
import { useIsMount } from '../../utils/hooks/is-mount'
import customizedDriverService from '@/services/customized-driver'

const CustomizedDriverContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const isMount = useIsMount()

  useEffect(() => {
    !isMount && getData()
  }, [state.page, state.pageSize, state.filter])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await customizedDriverService.getAllTrips({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: ICustomizedDriverListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  const getDetails = async (id: number) => {
    dispatch({ type: 'DETAILS_LOADING' })

    const result = await customizedDriverService.getTripDetails(id)

    if (isError(result)) {
      dispatch({ type: 'DETAILS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DETAILS_SUCCESS', payload: { data: result } })
  }

  const createCustomizedDriver = async (request: ICustomizedDriverCreate) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await customizedDriverService.createTrip(request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const updateCustomizedDriver = async (
    id: number,
    request: ICustomizedDriverEdit
  ) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await customizedDriverService.updateTrip(id, request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const deleteCustomizedDriver = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await customizedDriverService.deleteTrip(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }

    getData()
  }

  return (
    <CustomizedDriverContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
          getDetails,
          createCustomizedDriver,
          updateCustomizedDriver,
          deleteCustomizedDriver,
        },
      }}
    >
      {props.children}
    </CustomizedDriverContext.Provider>
  )
}

export default CustomizedDriverContextProvider
