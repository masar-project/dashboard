import React, { useEffect, useReducer } from 'react'
import { isError } from '../../utils/api/api-result'
import reducer from './reducer'
import POSContext, { internalState } from './context'
import { IPOSListFilter } from '../../models/pos/filter'
import { IPOSCreate, IPOSEdit } from '../../models/pos/request'
import posService from '../../services/pos'
import { useIsMount } from '../../utils/hooks/is-mount'

const POSContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const isMount = useIsMount()

  useEffect(() => {
    !isMount && getData()
  }, [state.page, state.pageSize, state.filter])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await posService.getAllPOSs({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: IPOSListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  const getDetails = async (id: number) => {
    dispatch({ type: 'DETAILS_LOADING' })

    const result = await posService.getPOSDetails(id)

    if (isError(result)) {
      dispatch({ type: 'DETAILS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DETAILS_SUCCESS', payload: { data: result } })
  }

  const createPOS = async (request: IPOSCreate) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await posService.createPOS(request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const updatePOS = async (id: number, request: IPOSEdit) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await posService.updatePOS(id, request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const disablePOS = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await posService.disablePOS(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }

    getData()
  }

  const enablePOS = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await posService.enablePOS(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }
    getData()
  }

  return (
    <POSContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
          getDetails,
          createPOS,
          updatePOS,
          disablePOS,
          enablePOS,
        },
      }}
    >
      {props.children}
    </POSContext.Provider>
  )
}

export default POSContextProvider
