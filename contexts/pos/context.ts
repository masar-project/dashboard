import { createContext } from 'react'

import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import {
  DEFAULT_FUNCTION,
  DEFAULT_PAGE_SIZE,
  INITIAL_PAGE,
  DEFAULT_FILTER,
} from '../constants'

import { IPOSListFilter } from '../../models/pos/filter'
import { IPOSCreate, IPOSEdit } from '../../models/pos/request'
import { IPOS, IPOSDetails } from '../../models/pos/response'

export interface IInternalState {
  data?: IBaseListingResponse<IPOS>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: IPOSListFilter
  error?: ApiError

  details?: IPOSDetails
  detailsLoading?: boolean
  detailsError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
  filter: DEFAULT_FILTER,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: IPOSListFilter) => void
    getDetails: (id: number) => void
    createPOS: (request: IPOSCreate) => Promise<Boolean>
    updatePOS: (id: number, request: IPOSEdit) => Promise<Boolean>
    disablePOS: (id: number) => void
    enablePOS: (id: number) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    createPOS: DEFAULT_FUNCTION,
    updatePOS: DEFAULT_FUNCTION,
    disablePOS: DEFAULT_FUNCTION,
    enablePOS: DEFAULT_FUNCTION,
  },
}

const POSContext = createContext(externalState)

export default POSContext
