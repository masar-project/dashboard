import { createContext } from 'react'

import ApiError from '@/utils/api/api-error'
import {
  ILocation,
  IPOSChangedStatus,
  ITripData,
} from '../../models/socket/index'
import { DEFAULT_FUNCTION } from '../constants'
import { ITrip } from '../../models/trip/response'
import { IPreviewTrip } from '../../models/default-trip/response'

export interface IInternalState {
  refreshTrip?: string
  completeTrip?: ITrip
  activeTripId?: number
  pausedTripId?: number
  canceledTripId?: number
  POSChangedStatus?: IPOSChangedStatus
  selectedTrip?: ITrip
  previewTrip?: IPreviewTrip
  driverLocation?: ILocation
  loading?: boolean
  actionLoading?: boolean
  error?: ApiError
}

export const internalState: IInternalState = {}

export interface IExternalState extends IInternalState {
  actions: {
    stopTrip: (tripData: ITripData) => void
    setSelectedTrip: (selectedTrip: ITrip) => void
    setPreviewTrip: (previewTrip: IPreviewTrip) => void
    refreshTrip: () => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    stopTrip: DEFAULT_FUNCTION,
    setSelectedTrip: DEFAULT_FUNCTION,
    setPreviewTrip: DEFAULT_FUNCTION,
    refreshTrip: DEFAULT_FUNCTION,
  },
}

const SocketContext = createContext(externalState)

export default SocketContext
