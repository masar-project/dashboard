import React, { useContext, useEffect, useReducer, useState } from 'react'
import reducer from './reducer'
import SocketContext, { internalState } from './context'
import { IFailure, IInTransit, ISuccess, ITripData } from '@/models/socket'
import AuthContext from '../auth/context'
import {
  EVENT_GET_CANCEL_TRIP,
  EVENT_GET_COMPLETE_TRIP,
  EVENT_GET_FAILURE_POS,
  EVENT_GET_IN_TRANSIT_POS,
  EVENT_GET_LOCATION,
  EVENT_GET_PAUSE_TRIP,
  EVENT_GET_REFRESH_DASHBOARD,
  EVENT_GET_START_TRIP,
  EVENT_GET_SUCCESS_POS,
  EVENT_POST_REFRESH,
} from '@/models/socket/events/events'
import { ILocation } from '../../models/socket/index'
import io from 'socket.io-client'
import { ITrip } from '../../models/trip/response'
import TripContext from '../trip/context'
import { Notification } from '@/utils/helpers/notification'
import { useTranslation } from 'react-i18next'
import { IPreviewTrip } from '@/models/default-trip/response'
import PreviewTrip from '../../components/default-trip/preview-trip/index'

const SocketContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const { employeeDetails } = useContext(AuthContext)
  const { data: trips, actions: tripsAction } = useContext(TripContext)
  const { t } = useTranslation()

  const [socket, setSocket] = useState<SocketIOClient.Socket>()

  useEffect(() => {
    // if (!employeeDetails) {
    //   socket?.disconnect()
    //   setSocket(undefined)
    //   console.log('NO employeeDetails')
    //   return
    // }
    const ioSocket = io(`${process.env.NEXT_PUBLIC_TRACKING_SERVER_URL}`, {
      transports: ['websocket'],
    })
    setSocket(ioSocket)
  }, [])

  // useEffect(() => {
  //   if (!employeeDetails) {
  //     socket?.disconnect()
  //     setSocket(undefined)
  //     return
  //   }
  //   const ioSocket = io(`${process.env.NEXT_PUBLIC_TRACKING_SERVER_URL}`, {
  //     transports: ['websocket'],
  //   })

  //   setSocket(ioSocket)
  // }, [employeeDetails])

  // // Listners
  // useEffect(() => {
  //   console.log('trips', trips)

  //   // Trip Status
  //   trips?.data?.map((trip, _) => {
  //     // Started
  //     socket.on(`${EVENT_GET_START_TRIP}-${trip?.id}`, () => {
  //       onTripStarted(trip?.id)
  //     })

  //     // Paused
  //     socket.on(`${EVENT_GET_PAUSE_TRIP}-${trip?.id}`, () => {
  //       // onTripStarted(trip?.id)
  //       onTripPaused(trip?.id)
  //     })

  //     // Canceled
  //     socket.on(`${EVENT_GET_CANCEL_TRIP}-${trip?.id}`, () => {
  //       // onTripStarted(trip?.id)
  //       onTripCanceled(trip?.id)
  //     })
  //   })
  // }, [trips])

  useEffect(() => {
    if (!socket) return
    socket.removeAllListeners()
    dispatch({ type: 'GET_LOCATION_SUCCESS', payload: { location: null } })

    socket.on('connect', onConnect)
    socket.on('disconnect', onDisconnect)

    // Trips Status
    if (trips) {
      console.log('trips: ', trips)

      trips?.data?.map((trip, _) => {
        // Started
        socket.on(`${EVENT_GET_START_TRIP}-${trip?.id}`, () => {
          onTripStarted(trip?.id)
        })

        // Paused
        socket.on(`${EVENT_GET_PAUSE_TRIP}-${trip?.id}`, () => {
          onTripPaused(trip?.id)
        })

        // Canceled
        socket.on(`${EVENT_GET_CANCEL_TRIP}-${trip?.id}`, () => {
          onTripCanceled(trip?.id)
        })

        // Completed
        socket.on(`${EVENT_GET_COMPLETE_TRIP}-${trip?.id}`, async () => {
          await onTripComplete(trip?.id)
        })
      })
    }

    // Selected Trip Listeners
    if (state?.selectedTrip) {
      // Events
      socket.on(EVENT_GET_REFRESH_DASHBOARD, onGetRefresh)

      socket.on(
        `${EVENT_GET_LOCATION}-${state?.selectedTrip?.id}`,
        onGetLocation
      )

      // Success
      socket.on(
        `${EVENT_GET_SUCCESS_POS}-${state?.selectedTrip?.id}`,
        onPOSSuccess
      )
      // In Transit
      socket.on(
        `${EVENT_GET_IN_TRANSIT_POS}-${state?.selectedTrip?.id}`,
        onPOSInTransit
      )
      // Failure
      socket.on(
        `${EVENT_GET_FAILURE_POS}-${state?.selectedTrip?.id}`,
        onPOSFailure
      )
    }
  }, [socket, state?.selectedTrip, trips])

  const stopTrip = async (tripData: ITripData) => {
    return true
  }

  // Change selected Trip
  const setSelectedTrip = (trip: ITrip) => {
    dispatch({ type: 'SELECTED_TRIP', payload: { selectedTrip: trip } })

    return true
  }

  const setPreviewTrip = (previewTrip: IPreviewTrip) => {
    dispatch({ type: 'PREVIEW_TRIP', payload: { PreviewTrip: previewTrip } })

    return true
  }

  // Socket
  const onConnect = () => {
    console.log('Socket connected successfully')
  }

  const onDisconnect = () => {
    console.log('Socket disconnected')
  }

  const getPosName = (id: number) => {
    return (
      state?.selectedTrip?.pointsOfSaleSorted?.find((p) => p?.id == id)?.name ??
      state?.previewTrip?.pointsOfSale?.find((p) => p?.id == id)?.name
    )
  }

  const onGetLocation = (location: ILocation) => {
    console.log('onGetLocation: ', location)

    dispatch({ type: 'GET_LOCATION_SUCCESS', payload: { location: location } })
  }

  const onPOSSuccess = (data: ISuccess) => {
    console.log('IPostSuccessPOSData: ', data)
    // POSChangedStatus
    dispatch({ type: 'GET_POS_SUCCESS', payload: { data: data } })

    Notification(
      'success',
      `${t('pos_visited')} ${getPosName(data?.posId)} ${t('successfuly')}`
    )
  }

  const onPOSInTransit = (data: IInTransit) => {
    console.log('onPOSTransit: ', data)

    dispatch({ type: 'GET_POS_INTRANSIT', payload: { data: data } })

    Notification('info', `${t('pos_in_transit')} ${getPosName(data?.posId)}`)
  }

  const onPOSFailure = (data: IFailure) => {
    console.log('onPOSFailure: ', data)

    dispatch({ type: 'GET_POS_FAILURE', payload: { data: data } })

    Notification('error', `${t('pos_failure')} ${getPosName(data?.posId)}`)
  }

  const getTripName = (tripId: number) => {
    return trips?.data?.find((t) => t?.id == tripId)?.name
  }

  // Refresh

  const onGetRefresh = () => {
    console.log('onGetRefresh')
    dispatch({
      type: 'GET_REFRESH',
      payload: { refreshTrip: `${Date.now()}` },
    })
  }

  const onTripStarted = (tripId: number) => {
    dispatch({ type: 'GET_TRIP_STARTED', payload: { tripId: tripId } })

    console.log('GET_TRIP_STARTED', tripId)
    Notification(
      'success',
      `${t('trip')} ${getTripName(tripId)} ${t('started')}`
    )
  }

  const onTripPaused = (tripId: number) => {
    dispatch({ type: 'GET_TRIP_PAUSED', payload: { tripId: tripId } })

    console.log('GET_TRIP_PAUSED', tripId)

    Notification(
      'warning',
      `${t('trip')} ${getTripName(tripId)} ${t('paused')}`
    )
  }

  const onTripCanceled = (tripId: number) => {
    dispatch({ type: 'GET_TRIP_CANCELED', payload: { tripId: tripId } })

    Notification(
      'error',
      `${t('trip')} ${getTripName(tripId)} ${t('canceled')}`
    )
  }

  const onTripComplete = async (tripId: number) => {
    const result = await tripsAction?.getData()
    if (result) {
      dispatch({
        type: 'GET_TRIP_COMPLETED',
        payload: { completeTrip: trips?.data?.find((t) => t?.id === tripId) },
      })

      Notification(
        'success',
        `${t('trip')} ${getTripName(tripId)} ${t('completed')}`
      )
    }
  }

  const refreshTrip = () => {
    socket.emit(`${EVENT_POST_REFRESH}`, {})
  }

  return (
    <SocketContext.Provider
      value={{
        ...state,
        actions: {
          stopTrip,
          setSelectedTrip,
          setPreviewTrip,
          refreshTrip,
        },
      }}
    >
      {props.children}
    </SocketContext.Provider>
  )
}

export default SocketContextProvider
