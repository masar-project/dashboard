import { IInternalState } from './context'
import ApiError from '@/utils/api/api-error'
import {
  ILocation,
  ISuccess,
  IInTransit,
  IFailure,
} from '../../models/socket/index'
import { ITrip } from '@/models/trip/response'
import { PointStatus } from '@/models/pos/enum'
import { SortedPOSStatus } from '@/models/trip/enum'
import { IPreviewTrip } from '../../models/default-trip/response'
import PreviewTrip from '@/components/default-trip/preview-trip'

type Action =
  | { type: 'LOADING' }
  | { type: 'ACTION_LOADING' }
  | { type: 'SUCCESS' }
  | { type: 'ERROR'; payload: { error: ApiError } }
  | { type: 'GET_LOCATION_SUCCESS'; payload: { location: ILocation } }
  | { type: 'GET_LOCATION_ERROR'; payload: { error: ApiError } }
  | { type: 'SELECTED_TRIP'; payload: { selectedTrip: ITrip } }
  | { type: 'PREVIEW_TRIP'; payload: { PreviewTrip: IPreviewTrip } }
  | { type: 'GET_POS_SUCCESS'; payload: { data: ISuccess } }
  | { type: 'GET_POS_INTRANSIT'; payload: { data: IInTransit } }
  | { type: 'GET_POS_FAILURE'; payload: { data: IFailure } }
  | { type: 'GET_TRIP_STARTED'; payload: { tripId: number } }
  | { type: 'GET_TRIP_PAUSED'; payload: { tripId: number } }
  | { type: 'GET_TRIP_CANCELED'; payload: { tripId: number } }
  | { type: 'GET_TRIP_COMPLETED'; payload: { completeTrip: ITrip } }
  | { type: 'GET_REFRESH'; payload: { refreshTrip: string } }

const reducer = (state: IInternalState, action: Action): IInternalState => {
  switch (action.type) {
    case 'LOADING':
      return { ...state, loading: true, error: undefined }
    case 'ACTION_LOADING':
      return { ...state, actionLoading: !state.actionLoading }
    case 'SUCCESS':
      return {
        ...state,
        loading: false,
        actionLoading: false,
      }
    case 'ERROR':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        error: action.payload.error,
      }

    case 'GET_LOCATION_SUCCESS':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        driverLocation: action.payload.location,
      }
    case 'SELECTED_TRIP':
      return {
        ...state,
        selectedTrip: action?.payload?.selectedTrip,
      }
    case 'PREVIEW_TRIP':
      return {
        ...state,
        previewTrip: action?.payload?.PreviewTrip,
      }
    case 'GET_LOCATION_ERROR':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        error: action.payload.error,
      }
    case 'GET_POS_SUCCESS':
      return {
        ...state,
        POSChangedStatus: {
          status: SortedPOSStatus.Success,
          tripId: state?.selectedTrip?.id,
          posId: action?.payload?.data?.posId,
        },
      }
    case 'GET_POS_INTRANSIT':
      return {
        ...state,
        POSChangedStatus: {
          status: SortedPOSStatus.Intransit,
          tripId: state?.selectedTrip?.id,
          posId: action?.payload?.data?.posId,
        },
      }
    case 'GET_POS_FAILURE':
      return {
        ...state,
        POSChangedStatus: {
          status: SortedPOSStatus.Failure,
          tripId: state?.selectedTrip?.id,
          posId: action?.payload?.data?.posId,
        },
      }
    case 'GET_TRIP_STARTED':
      return {
        ...state,
        activeTripId: action?.payload?.tripId,
      }

    case 'GET_TRIP_PAUSED':
      return {
        ...state,
        pausedTripId: action?.payload?.tripId,
      }
    case 'GET_TRIP_CANCELED':
      return {
        ...state,
        canceledTripId: action?.payload?.tripId,
      }
    case 'GET_TRIP_COMPLETED':
      return {
        ...state,
        completeTrip: action?.payload?.completeTrip,
      }
    case 'GET_REFRESH':
      return {
        ...state,
        refreshTrip: action?.payload?.refreshTrip,
      }
    default:
      return state
  }
}

export default reducer
