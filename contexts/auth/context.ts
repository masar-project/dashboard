import { createContext } from 'react'
import { DEFAULT_FUNCTION } from '../constants'
import {
  IChangePassword,
  ILoginRequest,
  IRegisterRequest,
} from '../../models/auth/request'
import {
  IEmployeeLoginResponse,
  IRegisterResponse,
} from '../../models/auth/response'
import ApiError from '../../utils/api/api-error'
import { CAN_VIEW_HOME_PAGE } from '../../utils/rbac/permissions'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import { IRole } from '../../models/role/response'
import { IEmployeeDetails } from '../../models/employee/response'

export interface IInternalState {
  grantedPermissions: string[]
  loginLoading?: boolean
  loginResponse?: IEmployeeLoginResponse
  loginError?: ApiError

  registerLoading?: boolean
  registerResponse?: IRegisterResponse
  registerError?: ApiError

  logoutError?: ApiError
  logoutLoading?: boolean

  changePasswordError?: ApiError
  changePasswordLoading?: boolean

  isAuthenticated?: boolean
  employeeDetails?: IEmployeeDetails
  roles?: IBaseListingResponse<IRole>
  rolesLoading?: boolean
  rolesError?: ApiError
}

export const internalState: IInternalState = {
  grantedPermissions: [CAN_VIEW_HOME_PAGE],
  changePasswordLoading: false,
  isAuthenticated: false,
}

export interface IExternalState extends IInternalState {
  actions: {
    login: (request: ILoginRequest) => void
    register: (request: IRegisterRequest) => void
    logout: () => void
    changePassword: (request: IChangePassword) => Promise<boolean>
    getRoles: () => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    login: DEFAULT_FUNCTION,
    register: DEFAULT_FUNCTION,
    logout: DEFAULT_FUNCTION,
    changePassword: DEFAULT_FUNCTION,
    getRoles: DEFAULT_FUNCTION,
  },
}

const AuthContext = createContext(externalState)

export default AuthContext
