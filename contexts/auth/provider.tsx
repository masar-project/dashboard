import React, { useContext, useEffect, useReducer } from 'react'
import AuthContext, { internalState } from './context'
import reducer from './reducer'
import {
  ILoginRequest,
  IChangePassword,
  IRegisterRequest,
} from '../../models/auth/request'
import authService from '../../services/auth/index'
import { isError } from '../../utils/api/api-result'
import { ACCESS_TOKEN, LOGGED_USER } from '../../utils/constants'
import eventManager from '../../utils/event-manager/index'
import {
  EVENT_UNAOUTHORIZED,
  EVENT_FORBIDDEN,
} from '../../utils/event-manager/index'
import { useRouter } from 'next/router'
import {
  CAN_VIEW_HOME_PAGE,
  CAN_VIEW_ROLES,
} from '../../utils/rbac/permissions'
import { DEFAULT_MAXIMUM_PAGE_SIZE } from '../constants'
import roleService from '../../services/role'
import meService from '../../services/me'
import AppContext from '../app/context'

const AuthContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const router = useRouter()

  const { actions: appActions } = useContext(AppContext)

  eventManager.on(EVENT_UNAOUTHORIZED, () => {
    router.replace('/login')
  })

  eventManager.on(EVENT_FORBIDDEN, () => {
    router.replace('/403')
  })

  useEffect(() => {
    router.pathname !== '/login' && router.pathname !== '/register' && me()
  }, [])

  // Get Roles
  useEffect(() => {
    if (state.isAuthenticated) {
      state.grantedPermissions.includes(CAN_VIEW_ROLES) && getRoles()
      appActions?.getCities()
      appActions?.getDrivers()
    }
  }, [state.isAuthenticated])

  // Me
  const me = async () => {
    const result = await meService.me()
    console.log('result: ', result)
    if (isError(result)) {
      dispatch({ type: 'IS_AUTHENTICATED', payload: { data: false } })
    } else {
      dispatch({ type: 'IS_AUTHENTICATED', payload: { data: true } })

      dispatch({ type: 'USER_DETAILS', payload: { data: result } })

      localStorage.setItem(LOGGED_USER, JSON.stringify(result))
    }
  }

  // Login
  const login = async (request: ILoginRequest) => {
    dispatch({ type: 'LOGIN_LOADING' })

    const result = await authService.loginEmployee(request)

    if (isError(result)) {
      dispatch({ type: 'LOGIN_ERROR', payload: { error: result } })
      return
    }
    dispatch({ type: 'IS_AUTHENTICATED', payload: { data: true } })

    dispatch({ type: 'USER_DETAILS', payload: { data: result.employee } })

    dispatch({ type: 'LOGIN_SUCCESS', payload: { data: result } })

    localStorage.setItem(ACCESS_TOKEN, result.accessToken)
    localStorage.setItem(LOGGED_USER, JSON.stringify(result.employee))
  }

  // Register
  const register = async (request: IRegisterRequest) => {
    dispatch({ type: 'REGISTER_LOADING' })

    const result = await authService.registerCompany(request)

    if (isError(result)) {
      dispatch({ type: 'REGISTER_ERROR', payload: { error: result } })
      return
    }

    // TODO: Register
    // dispatch({ type: 'IS_AUTHENTICATED', payload: { data: true } })

    // dispatch({ type: 'USER_DETAILS', payload: { data: result.user } })
    // dispatch({
    //   type: 'SET_GRANTED_PERMISSIONS',
    //   payload: {
    //     permissions: [
    //       CAN_VIEW_HOME_PAGE,
    //       ...result?.user?.roles?.flatMap((role) =>
    //         role?.permissions?.map((permission) => permission?.name)
    //       ),
    //     ],
    //   },
    // })
    // dispatch({ type: 'REGISTER_SUCCESS', payload: { data: result } })

    // localStorage.setItem(ACCESS_TOKEN, result.token)
    // localStorage.setItem(LOGGED_USER, JSON.stringify(result.user))
  }

  // Logout
  const logout = async () => {
    dispatch({ type: 'LOGOUT_LOADING' })

    const result = await authService.signOut()

    dispatch({ type: 'LOGOUT_LOADING' })

    if (isError(result)) {
      dispatch({ type: 'LOGOUT_ERROR', payload: { error: result } })
      return
    }
    dispatch({ type: 'LOGOUT_SUCCESS' })

    localStorage.removeItem(ACCESS_TOKEN)
    localStorage.removeItem(LOGGED_USER)
    router.replace('/login')
  }

  // Change Password
  const changePassword = async (request: IChangePassword) => {
    dispatch({ type: 'CHANGE_PASSWORD_LOADING' })

    const result = await authService.changePassword(request)

    dispatch({ type: 'CHANGE_PASSWORD_LOADING' })

    if (isError(result)) {
      dispatch({ type: 'CHANGE_PASSWORD_ERROR', payload: { error: result } })
      return false
    }
    dispatch({ type: 'CHANGE_PASSWORD_SUCCESS' })

    logout()
    return true
  }

  // Get all Roles
  const getRoles = async () => {
    dispatch({ type: 'ROLES_LOADING' })

    const result = await roleService.getAllRoles({
      page: 1,
      perPage: DEFAULT_MAXIMUM_PAGE_SIZE,
    })

    if (isError(result)) {
      dispatch({ type: 'ROLES_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'ROLES_SUCCESS', payload: { roles: result } })
  }

  return (
    <AuthContext.Provider
      value={{
        ...state,
        actions: {
          login,
          register,
          logout,
          changePassword,
          getRoles: getRoles,
        },
      }}
    >
      {props.children}
    </AuthContext.Provider>
  )
}

export default AuthContextProvider
