import {
  IEmployeeLoginResponse,
  IRegisterResponse,
} from '../../models/auth/response'
import { IInternalState } from './context'
import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import { IRole } from '../../models/role/response'
import { IEmployeeDetails } from '../../models/employee/response'

type Action =
  | {
      type: 'SET_GRANTED_PERMISSIONS'
      payload: { permissions: string[] }
    }
  | { type: 'USER_DETAILS'; payload: { data: IEmployeeDetails } }
  | { type: 'IS_AUTHENTICATED'; payload: { data: boolean } }
  | { type: 'LOGOUT_LOADING' }
  | { type: 'LOGOUT_SUCCESS' }
  | { type: 'LOGOUT_ERROR'; payload: { error: ApiError } }
  | { type: 'CHANGE_PASSWORD_LOADING' }
  | { type: 'CHANGE_PASSWORD_SUCCESS' }
  | { type: 'CHANGE_PASSWORD_ERROR'; payload: { error: ApiError } }
  | { type: 'LOGIN_LOADING' }
  | { type: 'LOGIN_SUCCESS'; payload: { data: IEmployeeLoginResponse } }
  | { type: 'LOGIN_ERROR'; payload: { error: ApiError } }
  | { type: 'REGISTER_LOADING' }
  | { type: 'REGISTER_SUCCESS'; payload: { data: IRegisterResponse } }
  | { type: 'REGISTER_ERROR'; payload: { error: ApiError } }
  | { type: 'ROLES_LOADING' }
  | {
      type: 'ROLES_SUCCESS'
      payload: { roles: IBaseListingResponse<IRole> }
    }
  | { type: 'ROLES_ERROR'; payload: { error: ApiError } }

const reducer = (state: IInternalState, action: Action): IInternalState => {
  switch (action.type) {
    case 'SET_GRANTED_PERMISSIONS':
      return { ...state, grantedPermissions: action.payload.permissions }
    case 'IS_AUTHENTICATED':
      return { ...state, isAuthenticated: action.payload.data }
    case 'LOGOUT_LOADING':
      return { ...state, logoutLoading: !state.logoutLoading }
    case 'LOGOUT_ERROR':
      return {
        ...state,
        logoutLoading: false,
        loginResponse: null,
        isAuthenticated: false,
        logoutError: action.payload.error,
      }
    case 'LOGOUT_SUCCESS':
      return {
        ...state,
        logoutLoading: false,
        loginResponse: null,
        isAuthenticated: false,
      }
    case 'CHANGE_PASSWORD_LOADING':
      return { ...state, changePasswordLoading: !state.changePasswordLoading }
    case 'CHANGE_PASSWORD_ERROR':
      return {
        ...state,
        changePasswordLoading: false,
        loginResponse: null,
        changePasswordError: action.payload.error,
      }
    case 'CHANGE_PASSWORD_SUCCESS':
      return {
        ...state,
        changePasswordLoading: false,
        loginResponse: null,
      }
    case 'USER_DETAILS':
      return {
        ...state,
        employeeDetails: action.payload.data,
      }
    case 'LOGIN_LOADING':
      return { ...state, loginLoading: !state.loginLoading }
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        loginLoading: false,
        loginResponse: action.payload.data,
      }
    case 'LOGIN_ERROR':
      return {
        ...state,
        loginLoading: false,
        loginError: action.payload.error,
      }

    case 'REGISTER_LOADING':
      return { ...state, registerLoading: !state.registerLoading }
    case 'REGISTER_SUCCESS':
      return {
        ...state,
        registerLoading: false,
        registerResponse: action.payload.data,
      }
    case 'REGISTER_ERROR':
      return {
        ...state,
        registerLoading: false,
        registerError: action.payload.error,
      }

    case 'ROLES_LOADING':
      return { ...state, rolesLoading: true, rolesError: undefined }
    case 'ROLES_SUCCESS':
      return {
        ...state,
        rolesLoading: false,
        roles: action.payload.roles,
      }
    case 'ROLES_ERROR':
      return {
        ...state,
        rolesLoading: false,
        rolesError: action.payload.error,
      }
    default:
      return state
  }
}

export default reducer
