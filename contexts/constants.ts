export const DEFAULT_FILTER = {
  key_search: undefined,
}
export const DEFAULT_PAGE_SIZE = 10
export const DEFAULT_MAXIMUM_PAGE_SIZE = 9999999
export const INITIAL_PAGE = 1
export const INITIAL_PAGES_COUNT = 1
export const DEFAULT_FUNCTION = () => null
