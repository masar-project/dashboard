import React, { useEffect, useReducer } from 'react'
import { isError } from '../../utils/api/api-result'
import reducer from './reducer'
import ProductContext, { internalState } from './context'
import { IProductListFilter } from '../../models/product/filter'
import { IProductCreate, IProductEdit } from '../../models/product/request'
import productService from '../../services/product'
import { useIsMount } from '../../utils/hooks/is-mount'

const ProductContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const isMount = useIsMount()

  useEffect(() => {
    !isMount && getData()
  }, [state.page, state.pageSize, state.filter])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await productService.getAllProducts({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: IProductListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  const getDetails = async (id: number) => {
    dispatch({ type: 'DETAILS_LOADING' })

    const result = await productService.getProductDetails(id)

    if (isError(result)) {
      dispatch({ type: 'DETAILS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DETAILS_SUCCESS', payload: { data: result } })
  }

  const createProduct = async (request: IProductCreate) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await productService.createProduct(request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const updateProduct = async (id: number, request: IProductEdit) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await productService.updateProduct(id, request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const deleteProduct = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await productService.deleteProduct(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }

    getData()
  }

  return (
    <ProductContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
          getDetails,
          createProduct,
          updateProduct,
          deleteProduct,
        },
      }}
    >
      {props.children}
    </ProductContext.Provider>
  )
}

export default ProductContextProvider
