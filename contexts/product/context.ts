import { createContext } from 'react'

import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import {
  DEFAULT_FUNCTION,
  DEFAULT_PAGE_SIZE,
  INITIAL_PAGE,
  DEFAULT_FILTER,
} from '../constants'

import { IProductListFilter } from '../../models/product/filter'
import { IProductCreate, IProductEdit } from '../../models/product/request'
import { IProduct, IProductDetails } from '../../models/product/response'

export interface IInternalState {
  data?: IBaseListingResponse<IProduct>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: IProductListFilter
  error?: ApiError

  details?: IProductDetails
  detailsLoading?: boolean
  detailsError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
  filter: DEFAULT_FILTER,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: IProductListFilter) => void
    getDetails: (id: number) => void
    createProduct: (request: IProductCreate) => Promise<Boolean>
    updateProduct: (id: number, request: IProductEdit) => Promise<Boolean>
    deleteProduct: (id: number) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    createProduct: DEFAULT_FUNCTION,
    updateProduct: DEFAULT_FUNCTION,
    deleteProduct: DEFAULT_FUNCTION,
  },
}

const ProductContext = createContext(externalState)

export default ProductContext
