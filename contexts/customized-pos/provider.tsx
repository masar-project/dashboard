import React, { useEffect, useReducer } from 'react'
import { isError } from '../../utils/api/api-result'
import reducer from './reducer'
import CustomizedPOSContext, { internalState } from './context'
import { ICustomizedPOSListFilter } from '../../models/customized-pos/filter'
import {
  ICustomizedPOSCreate,
  ICustomizedPOSEdit,
} from '../../models/customized-pos/request'
import { useIsMount } from '../../utils/hooks/is-mount'
import customizedPOSService from '@/services/customized-pos'

const CustomizedPOSContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)
  const isMount = useIsMount()

  useEffect(() => {
    !isMount && getData()
  }, [state.page, state.pageSize, state.filter])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await customizedPOSService.getAllTrips({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: ICustomizedPOSListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  const getDetails = async (id: number) => {
    dispatch({ type: 'DETAILS_LOADING' })

    const result = await customizedPOSService.getTripDetails(id)

    if (isError(result)) {
      dispatch({ type: 'DETAILS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DETAILS_SUCCESS', payload: { data: result } })
  }

  const createCustomizedPOS = async (request: ICustomizedPOSCreate) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await customizedPOSService.createTrip(request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const updateCustomizedPOS = async (
    id: number,
    request: ICustomizedPOSEdit
  ) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await customizedPOSService.updateTrip(id, request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    getData()
    return true
  }

  const deleteCustomizedPOS = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await customizedPOSService.deleteTrip(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }

    getData()
    return true
  }

  return (
    <CustomizedPOSContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
          getDetails,
          createCustomizedPOS,
          updateCustomizedPOS,
          deleteCustomizedPOS,
        },
      }}
    >
      {props.children}
    </CustomizedPOSContext.Provider>
  )
}

export default CustomizedPOSContextProvider
