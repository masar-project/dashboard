import IBaseListingResponse from '../../utils/api/base-listing-response'
import { IInternalState } from './context'
import { INITIAL_PAGE } from '../constants'
import ApiError from '../../utils/api/api-error'
import {
  ICustomizedPOS,
  ICustomizedPOSDetails,
} from '@/models/customized-pos/response'
import { ICustomizedPOSListFilter } from '@/models/customized-pos/filter'

type Action =
  | { type: 'LOADING' }
  | { type: 'ACTION_LOADING' }
  | { type: 'SUCCESS'; payload: { data: IBaseListingResponse<ICustomizedPOS> } }
  | { type: 'ERROR'; payload: { error: ApiError } }
  | { type: 'SET_PAGE'; payload: { page: number } }
  | { type: 'SET_PAGE_SIZE'; payload: { pageSize: number } }
  | { type: 'SET_FILTER'; payload: { filter?: ICustomizedPOSListFilter } }
  | { type: 'DETAILS_LOADING' }
  | { type: 'DETAILS_SUCCESS'; payload: { data: ICustomizedPOSDetails } }
  | { type: 'DETAILS_ERROR'; payload: { error: ApiError } }

const reducer = (state: IInternalState, action: Action): IInternalState => {
  switch (action.type) {
    case 'LOADING':
      return { ...state, loading: true, error: undefined }
    case 'ACTION_LOADING':
      return { ...state, actionLoading: !state.actionLoading }
    case 'SUCCESS':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        data: action.payload.data,
      }
    case 'ERROR':
      return {
        ...state,
        loading: false,
        actionLoading: false,
        error: action.payload.error,
      }
    case 'SET_PAGE':
      return { ...state, page: action.payload.page }
    case 'SET_PAGE_SIZE':
      return { ...state, pageSize: action.payload.pageSize }
    case 'SET_FILTER':
      return { ...state, filter: action.payload.filter, page: INITIAL_PAGE }
    case 'DETAILS_LOADING':
      return { ...state, detailsLoading: true, detailsError: undefined }
    case 'DETAILS_SUCCESS':
      return {
        ...state,
        detailsLoading: false,
        actionLoading: false,
        details: action.payload.data,
      }
    case 'DETAILS_ERROR':
      return {
        ...state,
        detailsLoading: false,
        actionLoading: false,
        detailsError: action.payload.error,
      }
    default:
      return state
  }
}

export default reducer
