import { ICustomizedPOSListFilter } from '@/models/customized-pos/filter'
import {
  ICustomizedPOSCreate,
  ICustomizedPOSEdit,
} from '@/models/customized-pos/request'
import {
  ICustomizedPOS,
  ICustomizedPOSDetails,
} from '@/models/customized-pos/response'
import { createContext } from 'react'

import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import {
  DEFAULT_FUNCTION,
  DEFAULT_PAGE_SIZE,
  INITIAL_PAGE,
  DEFAULT_FILTER,
} from '../constants'

export interface IInternalState {
  data?: IBaseListingResponse<ICustomizedPOS>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: ICustomizedPOSListFilter
  error?: ApiError

  details?: ICustomizedPOSDetails
  detailsLoading?: boolean
  detailsError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
  filter: DEFAULT_FILTER,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: ICustomizedPOSListFilter) => void
    getDetails: (id: number) => void
    createCustomizedPOS: (request: ICustomizedPOSCreate) => Promise<Boolean>
    updateCustomizedPOS: (
      id: number,
      request: ICustomizedPOSEdit
    ) => Promise<Boolean>
    deleteCustomizedPOS: (id: number) => Promise<Boolean>
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    createCustomizedPOS: DEFAULT_FUNCTION,
    updateCustomizedPOS: DEFAULT_FUNCTION,
    deleteCustomizedPOS: DEFAULT_FUNCTION,
  },
}

const CustomizedPOSContext = createContext(externalState)

export default CustomizedPOSContext
