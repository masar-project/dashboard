import { createContext } from 'react'
import IRoleListFilter from '../../models/role/filter'
import { IRoleCreate, IRoleEdit } from '../../models/role/request'
import { IRole, IRoleDetails } from '../../models/role/response'

import ApiError from '../../utils/api/api-error'
import IBaseListingResponse from '../../utils/api/base-listing-response'
import { DEFAULT_FUNCTION, DEFAULT_PAGE_SIZE, INITIAL_PAGE } from '../constants'

export interface IInternalState {
  data?: IBaseListingResponse<IRole>
  loading?: boolean
  actionLoading?: boolean
  page: number
  pageSize: number
  filter?: IRoleListFilter
  error?: ApiError

  details?: IRoleDetails
  detailsLoading?: boolean
  detailsError?: ApiError
}

export const internalState: IInternalState = {
  page: INITIAL_PAGE,
  pageSize: DEFAULT_PAGE_SIZE,
}

export interface IExternalState extends IInternalState {
  actions: {
    getData: () => void
    setPage: (page: number) => void
    setPageSize: (pageSize: number) => void
    setFilter: (filter: IRoleListFilter) => void
    getDetails: (id: number) => void
    createRole: (request: IRoleCreate) => Promise<Boolean>
    updateRole: (id: number, request: IRoleEdit) => Promise<Boolean>
    deleteRole: (id: number) => void
  }
}

export const externalState: IExternalState = {
  ...internalState,
  actions: {
    getData: DEFAULT_FUNCTION,
    setPage: DEFAULT_FUNCTION,
    setPageSize: DEFAULT_FUNCTION,
    setFilter: DEFAULT_FUNCTION,
    getDetails: DEFAULT_FUNCTION,
    createRole: DEFAULT_FUNCTION,
    updateRole: DEFAULT_FUNCTION,
    deleteRole: DEFAULT_FUNCTION,
  },
}

const RoleContext = createContext(externalState)

export default RoleContext
