/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useReducer } from 'react'
import { isError } from '../../utils/api/api-result'
import reducer from './reducer'
import { IRoleCreate, IRoleEdit } from '../../models/role/request'
import roleService from '../../services/role'
import RoleContext, { internalState } from './context'
import IRoleListFilter from '../../models/role/filter'
import AuthContext from '../auth/context'

const RoleContextProvider: React.FC = (props) => {
  const [state, dispatch] = useReducer(reducer, internalState)

  const { actions: authActions } = useContext(AuthContext)

  useEffect(() => {
    getData()
  }, [state.page, state.pageSize, state.filter])

  const getData = async () => {
    dispatch({ type: 'LOADING' })

    const result = await roleService.getAllRoles({
      page: state.page,
      perPage: state.pageSize,
      filter: state.filter,
    })

    if (isError(result)) {
      dispatch({ type: 'ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'SUCCESS', payload: { data: result } })
  }

  const setPage = (page: number) => {
    dispatch({ type: 'SET_PAGE', payload: { page } })
  }

  const setPageSize = (pageSize: number) => {
    dispatch({ type: 'SET_PAGE_SIZE', payload: { pageSize } })
  }

  const setFilter = (filter: IRoleListFilter) => {
    dispatch({ type: 'SET_FILTER', payload: { filter } })
  }

  const getDetails = async (id: number) => {
    dispatch({ type: 'DETAILS_LOADING' })

    const result = await roleService.getRoleDetails(id)

    if (isError(result)) {
      dispatch({ type: 'DETAILS_ERROR', payload: { error: result } })
      return
    }

    dispatch({ type: 'DETAILS_SUCCESS', payload: { data: result } })
  }

  const createRole = async (request: IRoleCreate) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await roleService.createRole(request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    // Refresh Roles
    authActions.getRoles()
    getData()
    return true
  }

  const updateRole = async (id: number, request: IRoleEdit) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await roleService.updateRole(id, request)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return false
    }

    // Refresh Roles
    authActions.getRoles()
    getData()
    return true
  }

  const deleteRole = async (id: number) => {
    dispatch({ type: 'ACTION_LOADING' })

    const result = await roleService.deleteRole(id)

    dispatch({ type: 'ACTION_LOADING' })

    if (isError(result)) {
      return
    }

    // Refresh Roles
    authActions.getRoles()
    getData()
  }

  return (
    <RoleContext.Provider
      value={{
        ...state,
        actions: {
          getData,
          setPage,
          setPageSize,
          setFilter,
          getDetails,
          createRole,
          updateRole,
          deleteRole,
        },
      }}
    >
      {props.children}
    </RoleContext.Provider>
  )
}

export default RoleContextProvider
